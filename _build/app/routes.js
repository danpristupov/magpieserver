var express  = require('express');

module.exports = function(app, passport) {

    app.get('/', function(req, res) {
        res.render('index.ejs');
    });

    app.get('/login', function(req, res) {
        res.render('login.ejs', { message: req.flash('loginMessage') }); 
    });

    app.get('/signup', function(req, res) {
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    app.get('/main', isLoggedIn, function(req, res) {
        res.render('../client/index.ejs', {
            user : req.user 
        });
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/main',
        failureRedirect : '/signup', 
        failureFlash : true 
    }));
    
    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/main',
        failureRedirect : '/login',
        failureFlash : true
    }));
    
    
    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    app.get('/auth/google/callback',
            passport.authenticate('google', {
                    successRedirect : '/main',
                    failureRedirect : '/'
            }));
    
    app.use(express.static(__dirname + '/../client'));
};


function isLoggedIn(req, res, next) {

    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}