
function HierarchyNodePainter(node) {
	this.node = node;
	
	this.visual = {
		contentFont: '14px Arial',
		headersFont: '14px Arial',
		contentColor: '#bbb',
		headersColor: '#666',
		contentLabelTextHeight: 22,
		contentLabelHeight: 28,
		contentLabelUnderlineMargin: 10,
		contentLabelUnderlineWidth: 1.2,
		contentLabelUnderlineColor: '#d1d1d1',
		contentHeaderLabelTextHeight: 20,
		groupRectBorderRadius: 4,
		groupRectBorderColor: '#d1d1d1',
		groupRectBorderWidth: 1.2,
		groupRectMargins: [8, 8],
		groupRectHilightedBorderColor: '#60A0FF',
		groupNameLabelHeight: 22,
		groupNameLabelTextHeight: 18,
		groupNameLabelFont: 'bold 14px Arial',
		groupNameLabelColor: '#333',
		selectedLinkColor: '#60A0FF'
	};
	this.linkPoints = [];
	this.linkRects = [];
}

HierarchyNodePainter.prototype.createGroup = function (pstep) {
	return {linksToDraw: [], linkIds: [], 
				headerLabels: [], labels: [], 
				pstep: pstep,
				highlighted: false}
};

HierarchyNodePainter.prototype.calculateSize = function (ctx, nodes, links) {
	var tw = 0;
	var th = 0;
	var padding = 12;
	
	var headerLabels = [];
	var labels = [];
	var columnWidths = [0, 0];
	
	var groups = [];
	
	for (var ps in this.node.content.part.productionSteps.items)
	{
		var pstep = this.node.content.part.productionSteps.items[ps];
		var grId = pstep.step + ". " + pstep.operationName;
		groups[grId] = this.createGroup(pstep);
	}
	
	var linksToDraw = [];
	
	for (var l in links)
	{
		var link = links[l];
		
		if (link.dstNode != this.node.id || link.srcNode < 0)
		{
			continue;
		}
		var grId = link.props.pstep ? (link.props.pstep.step + ". " + 
							link.props.pstep.operationName) : 0;
		if (!(grId in groups))
		{
			groups[grId] = this.createGroup(link.props.pstep);
		}
		groups[grId].linksToDraw.push(link);
	}
	
	for (var grId in groups)
	{
		ctx.font = this.visual.groupNameLabelFont;
		var m = ctx.measureText(grId).width;
		tw = Math.max(padding + m, tw);
		th += this.visual.groupRectBorderWidth;
		th += this.visual.groupRectMargins[1];
		th += this.visual.groupNameLabelHeight;
		for (var k in groups[grId].linksToDraw) 
		{
			var link = groups[grId].linksToDraw[k];
			var srcNode = nodes[link.srcNode];
			var v = link.props.rqpart ? link.props.rqpart.quantity : "";
			ctx.font = this.visual.contentFont;
			var w = ctx.measureText(srcNode.content.part.name).width;
			ctx.font = this.visual.headersFont;
			var vw = ctx.measureText(v).width;
			th += this.visual.contentLabelHeight;
			tw = Math.max(w + padding + vw, tw);
		}
	}
	th += this.visual.groupRectMargins[1];
	th += this.visual.contentLabelUnderlineWidth;
	return {w: tw, h: th};
};

HierarchyNodePainter.prototype.linkDestPoint = function(nodes, links, linkId) {
	return this.linkPoints[linkId];
};

HierarchyNodePainter.prototype.modifyLink = function(link, x, y) {
	if (!this.groupRects)
	{
		if (this.node.content.part.productionSteps.count() > 0)
		{
			link.props.pstep = this.node.content.part.productionSteps.items[0];
		}
		return;
	}
	if (this.groupRects.length != 0)
	{
		link.props.pstep = this.groupRects[0].pstep;
	}
	for (var g in this.groupRects)
	{
		var gr = this.groupRects[g];
		if (x >= gr.x && y >= gr.y &&
			x < gr.x + gr.w && y < gr.y + gr.h)
		{
			link.props.pstep = gr.pstep;
			return;
		}
	}
};


HierarchyNodePainter.prototype.draw = function (graphView, ctx, rect, nodes, 
												links, simplified) {
	if (!this.node.expanded || !this.node.content)
	{
		return;
	}
	var tw = 0;
	var th = 0;
	var padding = 12;
	
	var headerLabels = [];
	var labels = [];
	var columnWidths = [0, 0];
	
	var groups = [];
	
	for (var ps in this.node.content.part.productionSteps.items)
	{
		var pstep = this.node.content.part.productionSteps.items[ps];
		var grId = pstep ? (pstep.step + ". " + pstep.operationName) : 0;
		groups[grId] = this.createGroup(pstep);
	}
	
	var linksToDraw = [];
	for (var l in links)
	{
		var link = links[l];
		if (link.dstNode != this.node.id || link.srcNode < 0)
		{
			continue;
		}
		var grId = link.props.pstep ? (link.props.pstep.step + ". " + 
							link.props.pstep.operationName) : 0;
		if (!(grId in groups))
		{
			groups[grId] = this.createGroup(link.props.pstep);
		}
		groups[grId].linksToDraw.push(link);
		groups[grId].linkIds.push(l);
		groups[grId].highlighted = groups[grId].highlighted || link.beingCreated;
	}
	
	for (var grId in groups)
	{
		ctx.font = this.visual.groupNameLabelFont;
		tw = Math.max(padding + ctx.measureText(grId).width, tw);
		th += this.visual.groupRectBorderWidth;
		th += this.visual.groupRectMargins[1];
		th += this.visual.groupNameLabelHeight;
		for (var k in groups[grId].linksToDraw) 
		{
			var link = groups[grId].linksToDraw[k];
			var srcNode = nodes[link.srcNode];
			var v = link.props.rqpart ? link.props.rqpart.quantity : "";
			ctx.font = this.visual.contentFont;
			var w = ctx.measureText(srcNode.content.part.name).width;
			groups[grId].headerLabels.push({t: srcNode.content.part.name, w: w});
			ctx.font = this.visual.headersFont;
			var vw = ctx.measureText(v).width;
			groups[grId].labels.push({t: v, w: vw, 
				li: groups[grId].linkIds[k]});
			
			th += this.visual.contentLabelHeight;
			tw = Math.max(w + padding + vw, tw);
			columnWidths[0] = Math.max(w, columnWidths[0]);
			columnWidths[1] = Math.max(vw, columnWidths[1]);
		}
	}
	th += this.visual.contentLabelUnderlineWidth;
	
	this.linkPoints = [];
	this.groupRects = [];
	this.linkRects = [];
	
	ctx.fillStyle = '#fff';
	ctx.beginPath();
	ctx.rect(0, 0, rect.w, rect.h);
	ctx.fill();
	
	ctx.fillStyle = this.visual.contentLabelUnderlineColor;
	ctx.fillRect(0, 0, rect.w, this.visual.contentLabelUnderlineWidth);
	
	var groupY = this.visual.groupRectMargins[1];
	for (var grId in groups)
	{
		var numLinks = groups[grId].linksToDraw.length;
		groupHeight = (this.visual.contentLabelHeight)  * numLinks + 
						this.visual.groupNameLabelHeight;
		var groupRect = {x: this.visual.groupRectMargins[0],
						 y: groupY, 
						 w: rect.w - 2 * this.visual.groupRectMargins[0],
						 h: groupHeight, 
						 pstep: groups[grId].pstep, 
						 id: grId};
		this.groupRects.push(groupRect);
		var groupSelected = (graphView.selectedObject instanceof 
							PartsModel.ProductionStep) && 
							graphView.selectedObject == groups[grId].pstep;
		
		ctx.strokeStyle = (groups[grId].highlighted || groupSelected) ? 
				this.visual.groupRectHilightedBorderColor : 
				this.visual.groupRectBorderColor;
		ctx.lineWidth = this.visual.groupRectBorderWidth;
		drawRoundRect(ctx, groupRect.x, groupRect.y, 
						groupRect.w, groupRect.h, 
						this.visual.groupRectBorderRadius, 
						false, true);
		if (!simplified)
		{
			ctx.fillStyle = this.visual.groupNameLabelColor;
			ctx.font = this.visual.groupNameLabelFont;
			ctx.fillText(grId, groupRect.x + padding, groupY + 
							this.visual.groupNameLabelTextHeight);
		}
		var textY = groupY + this.visual.groupNameLabelHeight;
		ctx.fillStyle =  this.visual.contentLabelUnderlineColor;
		ctx.fillRect(groupRect.x, textY, groupRect.w, 
							this.visual.contentLabelUnderlineWidth);
		for (var l in groups[grId].labels)
		{
			if (!simplified)
			{
				ctx.font = this.visual.contentFont;
				ctx.fillStyle = this.visual.contentColor;
				ctx.fillText(groups[grId].labels[l].t,
							groupRect.w + groupRect.x - padding - 
							ctx.measureText(groups[grId].labels[l].t).width,
							textY + this.visual.contentLabelTextHeight);
				ctx.font = this.visual.headersFont;
				ctx.fillStyle = this.visual.headersColor;
				ctx.fillText(groups[grId].headerLabels[l].t,
							groupRect.x + padding, 
							textY + this.visual.contentHeaderLabelTextHeight);
			}
			this.linkPoints[groups[grId].labels[l].li] = {x: groupRect.x + groupRect.w, 
								  y: textY + 0.5 * this.visual.contentLabelHeight};
			var linkRect = {x: groupRect.x, y: textY,
				w: groupRect.w, h: this.visual.contentLabelHeight};
			this.linkRects[groups[grId].labels[l].li] = linkRect;
				
			textY += this.visual.contentLabelHeight;
			ctx.fillStyle = this.visual.contentLabelUnderlineColor;
			if (l != groups[grId].labels.length - 1)
			{
				ctx.fillRect(groupRect.x, textY, groupRect.w, 
							this.visual.contentLabelUnderlineWidth);
			}
			var linkSelected = graphView.selectedObject == 
								graphView.links[groups[grId].labels[l].li].props.rqpart;
			
			if (linkSelected && !simplified)
			{
				ctx.strokeStyle = this.visual.selectedLinkColor;
				drawRoundRect(ctx, linkRect.x, linkRect.y, linkRect.w, linkRect.h, 
					  this.visual.groupRectBorderRadius, false, true);
			}
		}
		groupY += groupHeight + this.visual.groupRectMargins[1];
	}
	ctx.fillStyle =  this.visual.contentLabelUnderlineColor;
	ctx.fillRect(0, 
				 rect.h - this.visual.contentLabelUnderlineWidth, 
				 rect.w, 
				 this.visual.contentLabelUnderlineWidth);
		
};

HierarchyNodePainter.prototype.clicked = function (graphView, coords) {
	for (var gr in this.groupRects)
	{
		var groupRect = this.groupRects[gr];
		if (coords.x >= groupRect.x && coords.y >= groupRect.y &&
			coords.x < groupRect.x + groupRect.w && 
			coords.y < groupRect.y + groupRect.h)
		{
			//TODO
			graphView.setSelectedObject(groupRect.pstep);
			return;
		}
	}
};


function BasicGraphLayoutEngine() {
	this.nodeMargins = {x: 96, y: 32};
};

BasicGraphLayoutEngine.prototype.layoutNeighbourNodes = function (nodes, links, nodeId) {
	var node = nodes[nodeId];
	
	for (var l in links)
	{
		var s = links[l].srcNode;
		var d = links[l].dstNode;
		var nid = 0;
		var delta = 0;
		if (s == nodeId)
		{
			delta = 1;
			nid = d;
		} 
		else if (d == nodeId)
		{
			delta = -1;
			nid = s;
		}
		if (delta != 0 && !("level" in nodes[nid]))
		{
			nodes[nid].level = node.level + delta;
			this.layoutNeighbourNodes(nodes, links, nid);
		}	
	}
};

BasicGraphLayoutEngine.prototype.layout = function (nodes, links, rects) {
	if (nodes.length == 0 || links.length == 0)
	{
		return;
	}
	for (var n in nodes)
	{
		delete nodes[n].level;
	}
	
	nodes[0].level = 0;
	this.layoutNeighbourNodes(nodes, links, 0);
	
	for (var n in nodes)
	{
		if (!("level" in nodes[n]))
		{
			nodes[n].level = 0;
			this.layoutNeighbourNodes(nodes, links, n);
		}
	}
	var maxHeight = 0;
	var levels = [];
	for (var n in nodes)
	{
		var node = nodes[n];
		var l = node.level;
		if (!(l in levels))
		{
			levels[l] = { w: 0, h: 0, nodes: [], nids: []};
		}
		levels[l].w = Math.max(levels[l].w, rects[n].aw);
		levels[l].h += rects[n].ah + this.nodeMargins.y;
		maxHeight = Math.max(levels[l].h, maxHeight);
		levels[l].nodes.push(node);
		levels[l].nids.push(n);
	}
	var x = 0;
	for (var l in levels)
	{
		var level = levels[l];
		var y = maxHeight / 2 - level.h / 2;
		for (var n in level.nodes)
		{
			level.nodes[n].position.x = x + level.w - rects[level.nids[n]].aw;
			level.nodes[n].position.y = y;
			y += rects[level.nids[n]].ah + this.nodeMargins.y;
		}
		x += level.w + this.nodeMargins.x;
	}
};


function GraphViewNode(content) {
	this.position = {x: 0, y: 0};
	this.content = content;
	this.painter = null;
	this.expanded = false;
	this.linkAreaHilighted = false;
	this.id = -1;
	
};

function GraphViewLink(properties, sourceNodeId, destNodeId) {
	this.srcNode = sourceNodeId; 
	this.dstNode = destNodeId;
	this.props = properties;
};

function GraphViewLinkGroup(name, properties) { 
	this.links = [];
	this.name = name;
	this.props = properties;
};


	
function GraphView(rootElement, container, canvas) {
	ScrollableViewport.call(this, rootElement, container, canvas, 0, 32);
	
	this.visual = {
		backgroundColor: "#fff",
		nodeColor: ['#ececec', '#bcbcec', '#bcecbc'], //normal, root, leaf 
		nodeBorderColor: ['#d1d1d1', '#a1a1d1', '#a1d1a1'], //normal, root, leaf 
		nodeBorderRadius: 4,
		nodeBorderWidth: 1.5,
		nodeShadowBlur: 3,
		nodeShadowOffset: {x: 3, y: 3},
		nodeShadowColor: "#d1d1d1",
		nodeCaptionMinWidth: 64,
		nodeCaptionHeight: 28,
		nodeCaptionFont: 'bold 14px Arial',
		nodeCaptionExpanderPadding: 9,
		nodeCaptionExpanderFont: '12px Arial',
		nodeCaptionExpanderHeight: 17,
		nodeCaptionExpanderColor: "#bbb",
		nodeCaptionTextHeight: 19,
		nodeCaptionPadding: 22,
		nodeCaptionColor: ['#222', '#000', '#444'],
		nodeContentPadding: {x: 1.5, y: 8},
		linkLineWidth: 1.5,
		linkLineColor: '#d1d1d1',
		linkDeniedColor: '#f00',
		newLinkLineColor: '#60A0FF',
		highlightedLinkColor: '#60A0FF',
		linkAreaHighlightColor: '#60A0FF',
		linkAreaHighlightRadius: 4,
		linkAreaHighlightLineWidth: 2,
		linkAreaHighlightPadding: 8,
		minimapSize: 256,
		minimapBgColor: 'rgba(240, 240, 240, 0.5)',
		minimapViewAreaColor: 'rgba(255, 255, 255, 0.8)',
		selectedNodeBorderColor: '#60A0FF'
	};
	this.minimapVisual = JSON.parse(JSON.stringify(this.visual));
	this.minimapVisual.nodeColor = ['#ccc', '#ccc', '#ccc'];
	
	this.model = null;
	this.nodes = [];
	this.links = [];
	this.contentRect = {x: 0, y: 0, w: 0, h: 0};
	this.nodeRects = [];
    this.rootNodesCache = [];
	this.layoutEngine = new BasicGraphLayoutEngine();
	this.selectedObject = null;
	this.selectionChanged = new PartsModel.Event(this);
	this.nodeDoubleClicked = new PartsModel.Event(this);
	this.avgDrawTime = 0.0;
}

GraphView.prototype = Object.create(ScrollableViewport.prototype);
GraphView.prototype.superclassMouseMove = GraphView.prototype.mouseMove;
GraphView.prototype.superclassMouseDown = GraphView.prototype.mouseDown;
GraphView.prototype.superclassMouseUp = GraphView.prototype.mouseUp;

GraphView.prototype.setSelectedObject = function (so) {
	this.selectedObject = so;
	this.selectionChanged.notify(this.selectedObject);
};

GraphView.prototype.setModel = function (model) {
	if (model == this.model)
	{
		return;
	}
	console.log(model);
	this.nodes = [];
	this.links = [];
	this.contentRect = {x: 0, y: 0, w: 0, h: 0};
	this.nodeRects = [];
	this.selectedObject = null;
	var partNodeIds = [];
	
	this.model = model;
	
	var parts = model.getLinearizedParts();
	var self = this;
	// add a node for each part
	for (var p in parts)
	{
		var part = parts[p];
		
		part.productionSteps.removed.attach(function (list, arg) {
			self.productionStepRemoved(part, arg.item);
		});
		var nodeId = this.addNode({part: part});
		partNodeIds[part.name] = nodeId;
	}
	
	// connect nodes
	for (var p in parts)
	{
		var part = parts[p];
		var destNodeId = partNodeIds[part.name];
		this.createPartLinks(part, destNodeId, partNodeIds);
	}
	model.partRemoved.attach(function (sender, part) { self.partRemoved(part); });
    model.parts.added.attach(function (sender, p) { self.partAdded(p.item); });
    model.partCloned.attach(function (sender, args) { 
        self.partCloned(args.sourcePart, args.clonedPart); });
    this.layoutNodes();
	this.expandAllNodes(true);
};

GraphView.prototype.partRemoved = function (part) {
	var n = -1;
	for (var nn in this.nodes)
	{
		if (this.nodes[nn].content.part == part)
		{
			n = nn;
		}
	}
	if (n == -1)
	{
		return;
	}
	for (var l in this.links)
	{
		if (this.links[l].srcNode == n || this.links.dstNode == n)
		{
			delete this.links[l];
		}
		else 
		{
			if (this.links[l].srcNode > n)
			{
				this.links[l].srcNode--;
			}
			if (this.links[l].dstNode > n)
			{
				this.links[l].dstNode--;
			}
		}
	}
	
	this.nodes.splice(n, 1);
	for (var n in this.nodes)
	{
		this.nodes[n].id = n;
	}
	
	this.nodeRects = [];
	if (part == this.selectedObject)
	{
		this.setSelectedObject(null);
	}
	this.requestViewportUpdate();
    this.invalidateRootNodesCache();
};

GraphView.prototype.partAdded = function (part) {
    var nodeId = this.addNode({part: part});
    this.createPartLinks(part, nodeId);
    var node = this.nodes[nodeId];
    node.expanded = this.isNodeExpandable(node);
    this.requestViewportUpdate();
};

GraphView.prototype.partCloned = function(sourcePart, clonedPart) {
    var sourceNode = null;
    var clonedNode = null;
    var sourceNodeId = null;
    var clonedNodeId = null;
    for (var n in this.nodes)
	{
		if (this.nodes[n].content.part == sourcePart)
        {
            sourceNode = this.nodes[n];
            sourceNodeId = n;
        }
        else if (this.nodes[n].content.part == clonedPart)
        {
            clonedNode = this.nodes[n];
            clonedNodeId = n;
        }
	}
	if (sourceNode == null || clonedNode == null)
    {
        return;
    }   
    var margin = 32;
    clonedNode.position.x = sourceNode.position.x;
    clonedNode.position.y = sourceNode.position.y + 
            this.nodeRects[sourceNodeId].ah + margin;
};

GraphView.prototype.createPartLinks = function (part, nodeId, partNodeIds) {
    var self = this;
    if (!partNodeIds)
    {
        partNodeIds = [];
        for (var n in this.nodes)
        {
            partNodeIds[this.nodes[n].content.part.name] = n;
        }
    }
    
    for (var ps in part.productionSteps.items)
    {
        var pstep = part.productionSteps.items[ps];
        
        pstep.requiredParts.added.attach(function (sender, args) {
            self.rqPartAdded(args.item); 
        });
        
        pstep.requiredParts.removed.attach(function (sender, args) {
            self.rqPartRemoved(args.item);
        });
        
        for (var rp in pstep.requiredParts.items)
        {
            var rqpart = pstep.requiredParts.items[rp];
            if (rqpart.part == null)
            {
                continue;
            }
            var linkProps = {pstep: pstep, rqpart: rqpart};
            var linkIdx = this.links.length;
            rqpart.changed.attach(function (sender, args) {
                self.rqPartChanged(sender);
            });
            
            this.addLink(partNodeIds[rqpart.part.name], 
                            nodeId, linkProps);
        }
    }
};

GraphView.prototype.productionStepRemoved = function (part, pstep) {
	for (var l in this.links)
	{
		if (this.links[l].props.pstep == pstep)
		{
			delete this.links[l];
		}
	}
	this.invalidateRootNodesCache();
};

GraphView.prototype.findRqpartParents = function (rqpart) {
	var parts = this.model.getLinearizedParts();
	var part = null;
	var pstep = null;
	for (var p in parts)
	{
		var pp = parts[p];
		for (ps in pp.productionSteps.items)
		{
			var pst = pp.productionSteps.items[ps];
			if (pst.requiredParts.items.indexOf(rqpart) >= 0)
			{
				part = pp;
				pstep = pst;
				break;
			}
		}
	}
	var srcNode = -1;
	var dstNode = -1;
	for (var n in this.nodes)
	{
		if (this.nodes[n].content.part == part)
		{
			dstNode = n;
		}
		else if (this.nodes[n].content.part == rqpart.part)
		{
			srcNode = n;
		}
	}
	
	return {part: part, pstep: pstep, srcNode: srcNode, dstNode: dstNode};
};

GraphView.prototype.rqPartAdded = function (rqpart) {
	if (this.mouse.creatingLink)
	{
		return;
	}
	var parents = this.findRqpartParents(rqpart);
	var part = parents.part,
		pstep = parents.pstep,
		srcNode = parents.srcNode,
		dstNode = parents.dstNode;
	
	if (rqpart == null || pstep == null)
	{
		return;
	}
	
	
	var self = this;
	rqpart.changed.attach(function (sender, args) {
		self.rqPartChanged(sender);
	});
	this.addLink(srcNode, dstNode, 
						{pstep: pstep, rqpart: rqpart});
};

GraphView.prototype.rqPartRemoved = function (rqpart) {
	for (var l in this.links)
	{
		var link = this.links[l];
		if (link.props.rqpart == rqpart)
		{
			delete this.links[l];
			return;
		}
	}	
	this.invalidateRootNodesCache();
};

GraphView.prototype.rqPartChanged = function (rqpart) {
	for (var l in this.links)
	{
		if (this.links[l].props.rqpart == rqpart)
		{
			var dstNode = this.links[l].dstNode;
			var pstep = this.links[l].props.pstep;
			
			if (rqpart.part == null)
			{
				return;
			}
			
			delete this.links[l];
			
			var srcNode = 0;
			for (var n in this.nodes)
			{
				if (this.nodes[n].content.part == rqpart.part)
				{
					srcNode = n;
					break;
				}
			}
			this.addLink(srcNode, dstNode, 
						 {pstep: pstep, rqpart: rqpart});
			
			return;
		}
	}
	this.invalidateRootNodesCache();
};



GraphView.prototype.doubleClick = function (x, y) {
	var w = this.container.offsetWidth - this.scrollBarThickness;
	var h = this.container.offsetHeight - this.scrollBarThickness;
	var vpSize = {	x: this.filtered.right - this.filtered.left, 
						y: this.filtered.bottom - this.filtered.top };
	for (var r in this.nodeRects)
	{
		var rect = this.nodeRects[r];
		if (x >= rect.x && x < rect.x + rect.w &&
			y >= rect.y && y < rect.y + rect.h)
		{
			this.nodeDoubleClicked.notify(this.nodes[r]);
			return;
		}
	}
	var name = "New Part " + this.nodes.length;
	var part = new PartsModel.Part(name);
	
	this.model.parts.add(part);

	var pos = {
		x: this.contentRect.x + this.contentRect.w * 
			(this.filtered.left + (vpSize.x) * x / w),
		y: this.contentRect.y + this.contentRect.h * 
			(this.filtered.top + (vpSize.x) * y / h)
	};
	
	var searchWidth = 200, searchHeight = 400;
	var searchRect = {x: pos.x, y: pos.y - searchHeight, 
						w: searchWidth, h: searchHeight};
	for (var r in this.nodeRects)
	{
		var rect = this.nodeRects[r];
		var a = searchRect,
			b = {x: rect.ax, y: rect.ay, w: rect.aw, h: rect.ah};
		if (a.x < (b.x + b.w) && (a.x + a.w) > b.x &&
			a.y < (b.y + b.h) && (a.y + a.h) > b.y) 
		{
			pos.x = b.x;
			break;
		}
	}
	
	this.nodes[this.nodes.length - 1].position = pos;
	this.requestViewportUpdate();
	this.setSelectedObject(part);
	this.nodeDoubleClicked.notify(this.nodes[this.nodes.length - 1]);
};

GraphView.prototype.addNode = function (node) {
	var newNode = new GraphViewNode(node);
	newNode.id = this.nodes.length;
	this.nodes.push(newNode);
	return newNode.id;
};

GraphView.prototype.addLink = function (sourceNodeId, destNodeId, properties) {
	if (this.linkExists(sourceNodeId, destNodeId, properties.pstep, properties.rqpart) || 
		sourceNodeId >= this.nodes.length || 
		destNodeId >= this.nodes.length || sourceNodeId == destNodeId)
	{
		console.log("Link exists.");
		return;
	}
	var link = new GraphViewLink(properties, sourceNodeId, destNodeId);
	this.links.push(link);
    this.invalidateRootNodesCache();
};

GraphView.prototype.removeAllNodes = function () {
	this.links = [];
	this.nodes = [];
	this.nodeRects = [];
    this.invalidateRootNodesCache();
};


GraphView.prototype.layoutNodes = function () {
	this.draw();
	this.layoutEngine.layout(this.nodes, this.links, this.nodeRects);
};


GraphView.prototype.linkExists = function (src, dst, pstep, rqpart) {
	for (var l in this.links)
	{
		if (l != this.links.length - 1 && 
			this.links[l].srcNode == src &&
			this.links[l].dstNode == dst && 
			this.links[l].props.pstep == pstep)
		{
			return true;
		}
	}
	return false;
}

GraphView.prototype.hasCyclicLinks = function () {
	var nodes = [];
	for (var n in this.nodes)
	{
		nodes[n] = n;
	}
	var links = this.links;
	var isLeaf = function(nodeId) {
		for (var l in links)
		{
			if ((nodes[links[l].srcNode]) && 
				links[l].dstNode == nodeId)
			{
				return false;
			}
		}
		return true;
	};
	var nodesLeft = nodes.length;
	while (nodesLeft > 0)
	{
		var nodesRemoved = 0;
		for (var n in nodes)
		{
			if (nodes[n] == null)
			{
				continue;
			}
			if (isLeaf(n))
			{
				nodes[n] = null;
				nodesLeft--;
				nodesRemoved++;
			}
		}
		if (nodesRemoved == 0 && nodesLeft > 0)
		{
			return true;
		}
	}
	return false;
}

GraphView.prototype.getReferencableNodes = function (srcNodeId) {
    var srcPart = this.nodes[srcNodeId].content.part;
    var parts = this.model.getReferencableParts(srcPart);
    var nodes = [];
    for (var p in parts)
    {
        var part = parts[p];
        for (var n in this.nodes)
        {
            var node = this.nodes[n];
            if (node.content.part == part)
            {
                nodes.push(node);
            }
        }
    }
    return nodes;
}

GraphView.prototype.mouseDown = function (event) {
	var c = getRelativeCoordinates(event, this.canvas);
	this.mouse.movingMinimap = false;
	
	var w = this.container.offsetWidth - this.scrollBarThickness;
	var h = this.container.offsetHeight - this.scrollBarThickness;
	var minimapArea = {x: w - this.visual.minimapSize, 
		y: h - this.visual.minimapSize * h / w, 
		w: this.visual.minimapSize,
		h: this.visual.minimapSize * h / w};
	if (c.x >= minimapArea.x && c.x < minimapArea.x + minimapArea.w &&
		c.y >= minimapArea.y && c.y < minimapArea.y + minimapArea.h)
	{
		this.mouse.movingMinimap = true;
		return;
	}
	
	this.mouse.draggingNode = null;
	var linkAreaWidth = 12;
	for (var r in this.nodeRects)
	{
		var rect = this.nodeRects[r];
		
		if (c.x >= rect.x && c.x < rect.x + rect.w &&
			c.y >= rect.y && c.y < rect.y + rect.h)
		{
			if (c.x < rect.x + linkAreaWidth)
			{
				this.mouse.creatingLink = true;
				var link = {srcNode: r, dstNode: -1, 
					dragPoint: {x: c.x, y: c.y}, props: {pstep: null}, 
					beingCreated: true};
				this.links.push(link);
                this.mouse.validNodes = this.getReferencableNodes(r);
			}
			else
			{
				this.mouse.draggingNode = r;
			}
		}
	}
	
	this.superclassMouseDown(event);	
	if (this.mouse.draggingNode !== null || 
		this.mouse.creatingLink)
	{
		this.mouse.isDown = false;
	}
};

GraphView.prototype.mouseUp = function (event) {
	var c = getRelativeCoordinates(event, this.canvas);
	
	var w = this.container.offsetWidth - this.scrollBarThickness;
	var h = this.container.offsetHeight - this.scrollBarThickness;
	var vpSize = {	x: this.filtered.right - this.filtered.left, 
						y: this.filtered.bottom - this.filtered.top };
	var scaleFactor = (w / vpSize.x) / this.contentRect.w;
	
	var clickDistThreshold = 16;
	
	if (Math.abs(this.mouse.downPos.x - event.pageX) < clickDistThreshold && 
		Math.abs(this.mouse.downPos.y - event.pageY) < clickDistThreshold)
	{
		//click
		if (this.mouse.draggingNode !== null)
		{
			var nr = this.nodeRects[this.mouse.draggingNode];
			c.x -= nr.x;
			c.y -= nr.y;
			c.x /= scaleFactor;
			c.y /= scaleFactor;
			c.y -= this.visual.nodeCaptionHeight;
			this.nodeClicked(this.nodes[this.mouse.draggingNode], c);
		}
		else
		{
			this.setSelectedObject(null);
			this.requestViewportUpdate();
		}
	}
	
	this.mouse.draggingNode = null;
	if (this.mouse.creatingLink)
	{
		var link = this.links[this.links.length - 1];
		if (link.dstNode < 0 || 
			this.linkExists(link.srcNode, link.dstNode, 
							link.props.pstep, link.props.rqpart) || 
			link.addsCycle)
		{
			this.links.pop();
		}
		else
		{
			delete link.beingCreated;
			var rqpart = new PartsModel.RequiredPart(
				this.nodes[link.srcNode].content.part, 1);
			
			if (link.dstNode > 0 && 
				this.nodes[link.dstNode].content.part
				.productionSteps.count() == 0)
			{
				this.nodes[link.dstNode].content.part
					.productionSteps.add(link.props.pstep);
			}
			link.props.rqpart = rqpart;
			link.props.pstep.requiredParts.add(rqpart);
		}
		this.invalidateRootNodesCache();
	}
	this.mouse.creatingLink = false;
	this.mouse.movingMinimap = false;
	this.superclassMouseUp(event);
};


GraphView.prototype.mouseMove = function (event) {
	
	this.superclassMouseMove(event);
	var c = getRelativeCoordinates(event, this.canvas);
	var delta = {x: 0, y: 0};
	delta.x = this.mouse.movePos.x - this.mouse.lastMovePos.x;
	delta.y = this.mouse.movePos.y - this.mouse.lastMovePos.y;
	
	var w = this.container.offsetWidth - this.scrollBarThickness;
	var h = this.container.offsetHeight - this.scrollBarThickness;
	var vpSize = {	x: this.filtered.right - this.filtered.left, 
						y: this.filtered.bottom - this.filtered.top };
	var scaleFactor = (w / vpSize.x) / this.contentRect.w;
	
	if (this.mouse.movingMinimap)
	{	
		var dx = delta.x / this.visual.minimapSize;
		var dy = delta.y / (this.visual.minimapSize * h / w);
		this.left += dx;
		this.right += dx;
		this.top += dy;
		this.bottom += dy;
		
		var s = this.bottom - this.top;
		this.top = Math.max(0, Math.min(1.0 - s, this.top));
		this.bottom = this.top + s;
		s = this.right - this.left;
		this.left = Math.max(0, Math.min(1.0 - s, this.left));
		this.right = this.left + s;
	} 
	else if (this.mouse.draggingNode && this.mouse.draggingNode !== null)
	{
		var dn = this.nodes[this.mouse.draggingNode];
		dn.position.x += delta.x / scaleFactor;
		dn.position.y += delta.y / scaleFactor;
		
		if (this.selectedObject != this.nodes[this.mouse.draggingNode].content.part)
		{
			this.setSelectedObject(this.nodes[
				this.mouse.draggingNode].content.part);
		}
		
		this.requestViewportUpdate();
		return;
	}
	else if (this.mouse.creatingLink)
	{
		var link = this.links[this.links.length - 1];
		link.dragPoint = {x: c.x, y: c.y};
		var lastDstNode = link.dstNode;
		link.dstNode = -1;
		var pstep = new PartsModel.ProductionStep(1, "Operation");
		link.props.pstep = pstep;
		var self = this;
		pstep.requiredParts.removed.attach(function (sender, args) {
			self.rqPartRemoved(args.item);
		});
		for (var r in this.nodeRects)
		{
			var rect = this.nodeRects[r];
			if (r == link.srcNode)
			{
				continue;
			}
			if (c.x >= rect.x && c.x < rect.x + rect.w &&
				c.y >= rect.y && c.y < rect.y + rect.h)
			{
				link.dstNode = r;
				break;
			}
		}
		if (lastDstNode != link.dstNode)
		{
			link.addsCycle = this.hasCyclicLinks();
		}
		
		if (link.dstNode >= 0 && this.nodes[r].painter.modifyLink != null)
		{
			this.nodes[r].painter.modifyLink(link, 
				(c.x - rect.x - rect.cr.x) / scaleFactor, 
				(c.y - rect.y - rect.cr.y) / scaleFactor);
		}
		if (lastDstNode != link.dstNode)
		{
			var expandTimeout = 500;
			if (this.mouse.timeoutHandle)
			{
				clearTimeout(this.mouse.timeoutHandle);
			}
			if (link.dstNode >= 0)
			{
				var node = this.nodes[r];
				this.mouse.timeoutHandle = setTimeout(
					function () { node.expanded = true; }, expandTimeout);
			}
		}
		if (link.dstNode < 0 && lastDstNode >= 0 && 
			!this.isNodeExpandable(this.nodes[lastDstNode]))
		{
			this.nodes[lastDstNode].expanded = false;
		}
		this.requestViewportUpdate();
		return;
	}
	var linkAreaWidth = 12;
	for (var r in this.nodeRects)
	{
		var rect = this.nodeRects[r];
		var lastVal = this.nodes[r].linkAreaHilighted;
		this.nodes[r].linkAreaHilighted = 
				(c.x >= rect.x && c.x < rect.x + rect.w &&
				c.y >= rect.y && c.y < rect.y + rect.h) && 
				(c.x < rect.x + linkAreaWidth);
		if (this.nodes[r].linkAreaHilighted != lastVal)
		{
			this.requestViewportUpdate();
		}
	}
};

GraphView.prototype.nodeClicked = function (node, coords) {
	this.setSelectedObject(node.content.part);
	if (coords.y < 0)
	{
		if ((coords.x > this.nodeRects[this.nodes.indexOf(node)].aw - 
			3 * this.visual.nodeCaptionExpanderPadding) && 
			(node.expanded || this.isNodeExpandable(node)))
		{
			node.expanded = !node.expanded;
		}
	}
	else
	{
		node.painter.clicked(this, coords);
	}
	
	this.requestViewportUpdate();
};

GraphView.prototype.isRootNode = function (node) {
    if (node.id in this.rootNodesCache)
    {
        return this.rootNodesCache[node.id];
    }
    else
    {
        var root = true;
        for (var l in this.links)
        {
            if (this.links[l].srcNode == node.id)
            {
                root = false;
                break;
            }
        }
        console.log(node.content.part.name, ' (', node.id, ')', root ? 'is root' : 'is not root');
        this.rootNodesCache[node.id] = root;
        return root;
    }
};

GraphView.prototype.invalidateRootNodesCache = function () {
    console.log("Root nodes cache invalidated.");
    this.rootNodesCache = [];
};

GraphView.prototype.isLeafNode = function (node) {
    return node.content.part.productionSteps.count() == 0;
};

GraphView.prototype.isNodeExpandable = function (node) {
	return !this.isLeafNode(node);
};

GraphView.prototype.expandAllNodes = function(expand) {
	for (var n in this.nodes)
	{
		var node = this.nodes[n];
		if (this.isNodeExpandable(node))
		{
			node.expanded = expand;
		}
	}
	this.requestViewportUpdate();
};

GraphView.prototype.adjustContentRect = function () {
	var minBounds = {};
	var maxBounds = {};
	var roundUnit = 200;
	var hMargin = 50;
	var vMargin = 50;
	
	if (this.nodeRects.length > 0)
	{
		var rect = this.nodeRects[0];
		minBounds.x = rect.ax;
		minBounds.y = rect.ay;
		maxBounds.x = rect.ax + rect.aw;
		maxBounds.y = rect.ay + rect.ah;
	}
	
	for (var r in this.nodeRects)
	{
		var rect = this.nodeRects[r];
		minBounds.x = Math.min(minBounds.x, rect.ax);
		minBounds.y = Math.min(minBounds.y, rect.ay);
		maxBounds.x = Math.max(maxBounds.x, rect.ax + rect.aw);
		maxBounds.y = Math.max(maxBounds.y, rect.ay + rect.ah);
	}
	minBounds.x -= hMargin;
	minBounds.y -= vMargin;
	maxBounds.x += hMargin;
	maxBounds.y += vMargin;
	this.contentRect = {x: minBounds.x, y: minBounds.y, 
		w: maxBounds.x - minBounds.x, 
		h: maxBounds.y - minBounds.y};
		
	//rounding
	var rw = Math.max(1, Math.ceil(this.contentRect.w / roundUnit)) * roundUnit;
	var rh = Math.max(1, Math.ceil(this.contentRect.h / roundUnit)) * roundUnit;
	this.contentRect.x -= (rw - this.contentRect.w) / 2;
	this.contentRect.y -= (rh - this.contentRect.h) / 2;
	this.contentRect.w = rw;
	this.contentRect.h = rh;
	
	var w = this.container.offsetWidth - this.scrollBarThickness;
	var h = this.container.offsetHeight - this.scrollBarThickness;
	
	var vAsp = w / h;
	var cAsp = this.contentRect.w / this.contentRect.h;
				
	var adjusted = {x: 0, y: 0, w: 0, h: 0};
	
	adjusted.w = Math.max(this.contentRect.w, w);
	adjusted.h = Math.max(this.contentRect.h, h);
	
	if (cAsp < vAsp)
	{
		adjusted.w = adjusted.h * vAsp;
	}
	else
	{
		adjusted.h = adjusted.w / vAsp;
	}
	
	adjusted.x = minBounds.x - Math.abs(0.5 * (this.contentRect.w - adjusted.w));
	adjusted.y = minBounds.y - Math.abs(0.5 * (this.contentRect.h - adjusted.h));
	
	this.contentRect = adjusted;
};

GraphView.prototype.adjustScaleToDPI = function(ctx, w, h) {
	devicePixelRatio = window.devicePixelRatio || 1,
	backingStoreRatio = ctx.webkitBackingStorePixelRatio ||
						ctx.mozBackingStorePixelRatio ||
						ctx.msBackingStorePixelRatio ||
						ctx.oBackingStorePixelRatio ||
						ctx.backingStorePixelRatio || 1,

	ratio = devicePixelRatio / backingStoreRatio;

    if (devicePixelRatio !== backingStoreRatio) 
	{
		this.canvas.style.width = w + 'px';
		this.canvas.style.height = h + 'px';
		this.canvas.width = w * ratio;
		this.canvas.height = h * ratio;
        ctx.setTransform(1, 0, 0, 1, 0, 0);
		ctx.scale(ratio, ratio);
    }
};

GraphView.prototype.calculateNodeBounds = function(ctx, node, vp, simplified) {
	if (!node.painter)
	{
		node.painter = new HierarchyNodePainter(node);
	}
	
	var w = vp.w;
	var h = vp.h;
	var vpSize = vp.vpSize;
	var scaleFactor = vp.scaleFactor;
	
	var expSize = simplified ? {w: 32, h: 32} : 
		node.painter.calculateSize(ctx, this.nodes, this.links);
	
	var caption = node.content.part.name;
	if (!node.content.cache || caption != node.content.cache.captionText)
	{
		ctx.font = this.visual.nodeCaptionFont;
		node.content.cache = {
			captionText: caption,
			captionWidth: ctx.measureText(caption).width
		};
	}
	var captionWidth = node.content.cache.captionWidth;
	
	var nX = w * ((node.position.x - this.contentRect.x) / 
				this.contentRect.w - this.filtered.left) / vpSize.x;
	var nY = h * ((node.position.y - this.contentRect.y) / 
				this.contentRect.h - this.filtered.top) / vpSize.y;
	var nW = Math.max(node.expanded ? (expSize.w + 
				2 * this.visual.nodeContentPadding.x) : 0, 
				captionWidth) + 
				2 * this.visual.nodeCaptionPadding + 
				this.visual.nodeCaptionExpanderPadding;
	var nH = this.visual.nodeCaptionHeight + (node.expanded ? 
			(expSize.h + this.visual.nodeContentPadding.y) : 0);
	var rect = { x: nX, y: nY, w: nW * scaleFactor, 
		h: nH * scaleFactor, ax: node.position.x, 
		ay: node.position.y, aw: nW, ah: nH, 
		expW: expSize.w, expH: expSize.h, 
		fullW: Math.max(expSize.w + 
				2 * this.visual.nodeContentPadding.x, captionWidth + 
				2 * this.visual.nodeCaptionPadding + 
				this.visual.nodeCaptionExpanderPadding),
		fullH: this.visual.nodeCaptionHeight + expSize.h + 
			this.visual.nodeContentPadding.y,
		cr: {}
	};
	
	return rect;
}

GraphView.prototype.drawNode = function (ctx, node, simplified, visual) {
	var w = this.container.offsetWidth - this.scrollBarThickness;
	var h = this.container.offsetHeight - this.scrollBarThickness;
	var vpSize = {	x: this.filtered.right - this.filtered.left, 
					y: this.filtered.bottom - this.filtered.top };
	var scaleFactor = (w / vpSize.x) / this.contentRect.w;
	var caption = node.content.part.name;
	var nodeRect = this.nodeRects[node.id];
	if (!nodeRect)
	{
		return;
	}
	var selected = (node.content.part == this.selectedObject);
	var disabled = this.mouse.creatingLink && (this.mouse.validNodes.indexOf(node) < 0);
    var ierIdx = this.isLeafNode(node) ? 2 : (this.isRootNode(node) ? 1 : 0);
    
	var nX = nodeRect.x;
	var nY = nodeRect.y;
	var nW = nodeRect.aw;
	var nH = nodeRect.ah;
	
	if (!node.expanded && ((nX > w) || (nY > h) || 
		(nX + nodeRect.w < 0) || (nY + nodeRect.h < 0)))
	{
		return false;
	}
	
	ctx.save();
    ctx.globalAlpha = disabled ? 0.6 : 1.0;
	ctx.translate(nX, nY);
	ctx.scale(scaleFactor, scaleFactor);
	
	ctx.fillStyle = visual.nodeColor[ierIdx];
	if (simplified)
	{
		ctx.fillRect(0, 0, nW, nH);
	}
	else
	{
		ctx.strokeStyle = selected ? visual.selectedNodeBorderColor : 
						visual.nodeBorderColor[ierIdx];
		ctx.lineWidth = visual.nodeBorderWidth;
		
		ctx.shadowBlur = visual.nodeShadowBlur * scaleFactor;
		ctx.shadowOffsetX = visual.nodeShadowOffset.x * scaleFactor;
		ctx.shadowOffsetY = visual.nodeShadowOffset.y * scaleFactor;
		ctx.shadowColor = visual.nodeShadowColor;
		
		drawRoundRect(ctx, 0, 0, nW, nH, visual.nodeBorderRadius, 
						true, false);
		ctx.shadowBlur = 0;
		ctx.shadowOffsetX = 0;
		ctx.shadowOffsetY = 0;
		ctx.stroke();
	}
	
	
	if (!simplified)
	{
		ctx.font = visual.nodeCaptionFont;
		ctx.fillStyle = visual.nodeCaptionColor[ierIdx];
		ctx.fillText(caption, visual.nodeCaptionPadding,
						visual.nodeCaptionTextHeight);
		if (this.isNodeExpandable(node))
		{
			ctx.fillStyle = visual.nodeCaptionExpanderColor;
			ctx.font = visual.nodeCaptionExpanderFont;
			var expSymbol = node.expanded ? "\u25bc" : "\u25c0";
			ctx.fillText(expSymbol, nW - 2 * visual.nodeCaptionExpanderPadding,
							visual.nodeCaptionExpanderHeight);
		}
		if (node.linkAreaHilighted)
		{
			ctx.lineWidth = visual.linkLineWidth;
			ctx.strokeStyle = visual.linkAreaHighlightColor;
			ctx.beginPath();
			ctx.arc(visual.linkAreaHighlightPadding, 
					visual.nodeCaptionHeight / 2, 
					visual.linkAreaHighlightRadius, 
					0, Math.PI * 2, false);
			ctx.stroke();
		}
	}
	if (node.expanded)
	{
		var cr = {
			x: visual.nodeContentPadding.x,
			y: visual.nodeCaptionHeight,
			w: nW - 2 * visual.nodeContentPadding.x,
			h: nodeRect.expH
		}
		this.nodeRects[node.id].cr = cr;
		if (!simplified)
		{
			ctx.save();
			ctx.beginPath();
			ctx.rect(cr.x, cr.y, cr.w, cr.h);
			ctx.clip();
			ctx.translate(cr.x, cr.y);
			node.painter.draw(this, ctx, cr, this.nodes, 
							this.links, simplified);
			ctx.translate(-cr.x, -cr.y);
			ctx.restore();
		}
	}
	ctx.restore();
	return true;
};

GraphView.prototype.drawLinks = function (ctx, visual) {
	var w = this.container.offsetWidth - this.scrollBarThickness;
	var h = this.container.offsetHeight - this.scrollBarThickness;
	var vpSize = {	x: this.filtered.right - this.filtered.left, 
					y: this.filtered.bottom - this.filtered.top };
	var scaleFactor = (w / vpSize.x) / this.contentRect.w;
	
	var minExtend = 220 * scaleFactor;
	var linksDrawn = 0;
	for (var l in this.links)
	{
		var link = this.links[l];
		var srcNode = this.nodes[link.srcNode];
		var dstNode = (link.dstNode >= 0) ? this.nodes[link.dstNode] : null;
		
		var nr = this.nodeRects[link.srcNode];
		if (!nr)
		{
			continue;
		}
		var srcPnt = {x: nr.x, y: nr.y + 
			0.5 * visual.nodeCaptionHeight * scaleFactor};
		var dstPnt;
		if (dstNode)
		{
			nr = this.nodeRects[link.dstNode];
			var capDstPnt = {x: nr.x + nr.w, y: nr.y + 
					0.5 * visual.nodeCaptionHeight * scaleFactor};
			if (dstNode.expanded)
			{
				dstPnt = dstNode.painter.linkDestPoint(this.nodes, this.links, l);
				if (dstPnt != null)
				{
					dstPnt.x = nr.x + dstPnt.x * scaleFactor;
					dstPnt.y = nr.y + (visual.nodeCaptionHeight + dstPnt.y) * scaleFactor;
				}
				else
				{
					dstPnt = capDstPnt;
				}
			}
			else
			{
				dstPnt = capDstPnt;
			}
		}
		else
		{
			dstPnt = link.dragPoint;
		}
		var bnd = {x: Math.min(srcPnt.x, dstPnt.x), y: Math.min(srcPnt.y, dstPnt.y),
				   r: Math.max(srcPnt.x, dstPnt.x), b: Math.max(srcPnt.y, dstPnt.y)};
		if (bnd.x > w || bnd.r < 0 || bnd.y > h || bnd.b < 0)
		{
			continue;
		}
		linksDrawn++;
		var highlighted = (srcNode.content.part == this.selectedObject) || 
							link.props.pstep == this.selectedObject || 
							(dstNode && dstNode.content.part == this.selectedObject);
		
		var dist = Math.max(minExtend, Math.abs(dstPnt.x - srcPnt.x));
		ctx.lineWidth = Math.max(1.0, visual.linkLineWidth * scaleFactor);
		ctx.strokeStyle = (this.mouse.creatingLink && l == this.links.length - 1) ?
			(link.addsCycle ? visual.linkDeniedColor : visual.newLinkLineColor) : 
			(highlighted ? visual.highlightedLinkColor : visual.linkLineColor);
		ctx.beginPath();
		ctx.moveTo(srcPnt.x, srcPnt.y);
		ctx.bezierCurveTo(srcPnt.x - 0.25 * dist, srcPnt.y, 
						  dstPnt.x + 0.25 * dist, dstPnt.y,
						  dstPnt.x, dstPnt.y);
		ctx.stroke();
	}
};

GraphView.prototype.drawContent = function (ctx, simplified, visual) {
	ctx.save();
	
	this.nodeRects = [];
	
	var w = this.container.offsetWidth - this.scrollBarThickness;
	var h = this.container.offsetHeight - this.scrollBarThickness;
	var vpSize = {	x: this.filtered.right - this.filtered.left, 
					y: this.filtered.bottom - this.filtered.top };
	var scaleFactor = (w / vpSize.x) / this.contentRect.w;
	
	var vpParams = {
		w: w, h: h, 
		vpSize: vpSize, 
		scaleFactor: scaleFactor};
	
	for (var nid in this.nodes)
	{
		var node = this.nodes[nid];
		this.nodeRects[nid] = this.calculateNodeBounds(
			ctx, node, vpParams, simplified);
	}
	if (!simplified)
	{
		this.adjustContentRect();
		this.drawLinks(ctx, visual);
	}
	
	nodesDrawn = 0;
	for (var nid in this.nodes)
	{
		var node = this.nodes[nid];
		
		if (this.drawNode(ctx, node, simplified, visual))
		{
			nodesDrawn++;
		}
	}
	
	
	ctx.restore();
};



GraphView.prototype.draw = function () {
	if (this.container.style.display == 'none') 
	{
		return;
	}
	var w = this.container.offsetWidth;
	var h = this.container.offsetHeight;	
	if (this.canvas.offsetWidth != w || this.canvas.offsetHeight != h)
	{
		this.canvas.width = w;
		this.canvas.height = h;
	}
	var ctx = this.canvas.getContext('2d');
	this.adjustScaleToDPI(ctx, w, h);
	
	var drawTime = new Date().getTime();
	
	ctx.clearRect(0, 0, w, h);
	this.fillStyle = this.visual.backgroundColor;

	w -= this.scrollBarThickness;
	h -= this.scrollBarThickness;
	
	this.drawContent(ctx, false, this.visual);
	
	// minimap
	
	ctx.save();
	ctx.translate(w - this.visual.minimapSize, h - this.visual.minimapSize * h / w);
	var mmScale = this.visual.minimapSize / w;
	ctx.scale(mmScale, mmScale);
	ctx.fillStyle = this.visual.minimapBgColor;
	ctx.fillRect(0, 0, w, h);
	ctx.fillStyle = this.visual.minimapViewAreaColor;
	ctx.fillRect(w * this.filtered.left, h * this.filtered.top, 
				 w * (this.filtered.right - this.filtered.left), 
				 h * (this.filtered.bottom - this.filtered.top));
	var filteredBackup = this.filtered;
	var rectsBackup = this.nodeRects;
	this.filtered = {left: 0, right: 1, top: 0, bottom: 1};
	this.drawContent(ctx, true, this.minimapVisual);
	this.filtered = filteredBackup;
	this.nodeRects = rectsBackup;
	
	ctx.restore();
	
	var epsilon = 0.001;
	if (Math.abs(0.0 - this.top) > epsilon || Math.abs(1.0 - this.bottom) > epsilon)
	{
		this.drawScrollBar(ctx, true, this.top, this.bottom, 0, h);
	}
	if (Math.abs(0.0 - this.left) > epsilon || Math.abs(1.0 - this.right) > epsilon)
	{
		this.drawScrollBar(ctx, false, this.filtered.left, this.filtered.right, 0, w);
	}
	
	this.scrollAreaMargins.left = 0;
	this.scrollAreaMargins.right = this.scrollBarThickness;
	this.scrollAreaMargins.top = 0;
	this.scrollAreaMargins.bottom = this.scrollBarThickness;
	this.maxZoom = 1 + Math.log(this.contentRect.w / w) / Math.log(this.zoomFactor);
	
	
	
	drawTime = new Date().getTime() - drawTime;
	
	this.avgDrawTime = (this.avgDrawTime == 0.0) ? drawTime : 
					(drawTime * 0.05 + 0.95 * this.avgDrawTime);
	
	ctx.fillStyle = '#aaa';
	ctx.fillText(Math.round(this.avgDrawTime), 8, h - 8);
	
};