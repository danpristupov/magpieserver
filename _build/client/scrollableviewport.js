

function getAbsolutePosition(element) {
	var r = { x: element.offsetLeft, y: element.offsetTop };
	if (element.offsetParent) {
		var tmp = getAbsolutePosition(element.offsetParent);
		r.x += tmp.x;
		r.y += tmp.y;
	}
	return r;
};

/**
    * Retrieve the coordinates of the given event relative to the center
    * of the widget.
    *
    * @param event
    *   A mouse-related DOM event.
    * @param reference
    *   A DOM element whose position we want to transform the mouse coordinates to.
    * @return
    *    A hash containing keys 'x' and 'y'.
    */
function  getRelativeCoordinates(event, reference) {
	var x, y;
	event = event || window.event;
	var el = event.target || event.srcElement;
	if (!window.opera && typeof event.offsetX != 'undefined') {
		// Use offset coordinates and find common offsetParent
		var pos = { x: event.offsetX, y: event.offsetY };
		// Send the coordinates upwards through the offsetParent chain.
		var e = el;
		while (e) {
			e.mouseX = pos.x;
			e.mouseY = pos.y;
			pos.x += e.offsetLeft;
			pos.y += e.offsetTop;
			e = e.offsetParent;
		}
		// Look for the coordinates starting from the reference element.
		var e = reference;
		var offset = { x: 0, y: 0 }
		while (e) {
			if (typeof e.mouseX != 'undefined') {
				x = e.mouseX - offset.x;
				y = e.mouseY - offset.y;
				break;
			}
			offset.x += e.offsetLeft;
			offset.y += e.offsetTop;
			e = e.offsetParent;
		}
		// Reset stored coordinates
		e = el;
		while (e) {
			e.mouseX = undefined;
			e.mouseY = undefined;
			e = e.offsetParent;
		}
	}
	else {
		// Use absolute coordinates
		var pos = getAbsolutePosition(reference);
		x = event.pageX  - pos.x;
		y = event.pageY - pos.y;
	}
	// Subtract distance to middle
	return { x: x, y: y };
}




function wheelDelta(event) {
	//errMsgShow(event);
	var delta = 0;
	if (!event) /* For IE. */
		event = window.event;
	if (event.wheelDelta) { /* IE/Opera. */
		delta = event.wheelDelta/120;
	} else if (event.deltaX || event.deltaY || event.deltaZ) { /** Mozilla case. */
		/** In Mozilla, sign of delta is different than in IE.
			* Also, delta is multiple of 3.
			*/
		delta = -(event.deltaY + event.deltaX + event.deltaZ);
	}
	/** If delta is nonzero, handle it.
		* Basically, delta is now positive if wheel was scrolled up,
		* and negative, if wheel was scrolled down.
		*/

	/** Prevent default actions caused by mouse wheel.
		* That might be ugly, but we handle scrolls somehow
		* anyway, so don't bother here..
		*/
	if (event.preventDefault)
			event.preventDefault();
	event.returnValue = false;
	return delta;
}

function wheel(event){
	var delta = 0;
	if (!event) /* For IE. */
		event = window.event;
	if (event.wheelDelta) { /* IE/Opera. */
		delta = event.wheelDelta/120;
	} else if (event.detail) { /** Mozilla case. */
		/** In Mozilla, sign of delta is different than in IE.
			* Also, delta is multiple of 3.
			*/
		delta = -event.detail/3;
	}
	/** If delta is nonzero, handle it.
		* Basically, delta is now positive if wheel was scrolled up,
		* and negative, if wheel was scrolled down.
		*/
	if (delta)
		handleWheel(event, delta);
	/** Prevent default actions caused by mouse wheel.
		* That might be ugly, but we handle scrolls somehow
		* anyway, so don't bother here..
		*/
	if (event.preventDefault)
			event.preventDefault();
	event.returnValue = false;
}

function drawRoundRect(ctx, x, y, width, height, radius, fill, stroke) 
{
	ctx.beginPath();
	ctx.moveTo(x + radius, y);
	ctx.lineTo(x + width - radius, y);
	ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
	ctx.lineTo(x + width, y + height - radius);
	ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
	ctx.lineTo(x + radius, y + height);
	ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
	ctx.lineTo(x, y + radius);
	ctx.quadraticCurveTo(x, y, x + radius, y);
	ctx.closePath();
	if (fill) 
	{
		ctx.fill();
	}
	if (stroke) 
	{
		ctx.stroke();
	}
	
}

function ScrollableViewport(rootElement, container, canvas, minZoom, maxZoom)
{
	this.container = container;
	this.rootElement = rootElement;
	
	this.zoomLowPassAlpha = 0.4;
	this.lowPassAlpha = 1.0;
	this.canvas = canvas;
	
	this.minZoom = minZoom;
	this.maxZoom = maxZoom;
	this.zoom = minZoom;
	this.zoomFactor = 1.5;
	this.size = Math.min(1.0, 1.0 / Math.pow(this.zoomFactor, this.zoom));
	this.left = 0.0;
	this.right = 1.0;
	this.top = 0.0;
	this.bottom = 1.0;
	this.mouse = {
		isDown : false,
		panning : false,
		velocityMode : false,
		velocity: {x: 0.0, y: 0.0},
		relaxCoeff: 0.07,
		downPos : {x : 0, y : 0}, 
		movePos : {x : 0, y : 0},
		lastMovePos : {x : 0, y : 0},
		onScrollBar : false,
		onVertScrollbar : false,
		lastClickTime: 0
	};
	var self = this;
	this.filtered = {
		left : 0.5 - self.size / 4.0,
		right : 0.5 + self.size / 4.0,
		top : self.top,
		bottom : self.bottom
	};
	this.lastRedrawTime = (new Date()).getTime();
	this.maxRedrawInterval = 500;
	
	if (window.requestAnimationFrame)
	{
		this.animateFunc = function() {
				if (self.doLowPass != null) self.doLowPass();
				self.animRequestId = 
					window.requestAnimationFrame(self.animateFunc);
			};	
		this.animRequestId = window.requestAnimationFrame(this.animateFunc);
		;
	}
	else
	{
		setInterval(function () { 
			if (self.doLowPass != null) self.doLowPass(); }, 17);
	}

	this.canvas.onmousedown = function(event) { self.mouseDown(event) };
	this.canvas.onwheel = function(event) { self.onMouseWheel(event); };
	this.canvas.onmousewheel = this.canvas.onwheel;
	//this.canvas.onmoztouchdown = this.canvas.onmousedown;

	var oldMouseMoveHandler = this.rootElement.onmousemove;
	this.rootElement.onmousemove = function (event) {
		if (self.mouseMove) self.mouseMove(event);
		if (oldMouseMoveHandler != null) oldMouseMoveHandler(event);
	};
	var oldMouseUpHandler = this.rootElement.onmouseup;
	this.rootElement.onmouseup = function (event) {
		if (self.mouseUp) self.mouseUp(event);
		if (oldMouseUpHandler != null) oldMouseUpHandler(event);
	};
	var oldMouseLeaveHandler = this.rootElement.onmouseleave;
	this.rootElement.onmouseleave = function (event) {
		if (self.mouseLeave) self.mouseLeave(event);
		if (oldMouseLeaveHandler != null) oldMouseLeaveHandler(event);
	};
	//this.canvas.onmoztouchmove = this.canvas.onmousemove;
	//this.canvas.onmoztouchup = this.canvas.onmouseup;
	this.canvas.addEventListener("touchstart", function(e) {self.onTouchStart(e);}, false);
	this.canvas.addEventListener("touchend", function(e) {self.onTouchEnd(e);}, false);
	this.canvas.addEventListener("touchmove", function(e) {self.onTouchMove(e);}, false);
	this.lastPinchDistance = -1.0;
	
	this.scrollBarThickness = 12;
	this.scrollBarBgColor = '#f0f0f0';
	this.scrollBarFgColor = '#BDBDBD';
	this.scrollBarPadding = 1;
	this.scrollBarThumbMinSize = 16;
	this.scrollBarsSizes = [0, 0];
	this.scrollAreaMargins = {left: 0, top: 0, right: 0, bottom: 0};
	
	this.zoomPolicy = { allowVertical: true}
};

ScrollableViewport.prototype.animate = function()
{
	if (this.doLowPass != null) this.doLowPass();
	this.animRequestId = window.requestAnimationFrame(this.animate);
}

ScrollableViewport.prototype.onTouchStart = function(evt)
{
	evt.preventDefault();
	var touches = evt.targetTouches;
	if (evt.touches.length == 1)
	{
		this.mouse.isDown = true;
		this.mouse.downPos.x = touches[0].pageX;
		this.mouse.downPos.y = touches[0].pageY;
		this.mouse.movePos.x = this.mouse.downPos.x;
		this.mouse.movePos.y = this.mouse.downPos.y;
		this.mouse.lastMovePos.x = touches[0].pageX;
		this.mouse.lastMovePos.y = touches[0].pageY;
		this.mouse.button = 0;
	}
	this.lastPinchDistance = -1;
}

ScrollableViewport.prototype.onTouchEnd = function(evt)
{
	evt.preventDefault();
	this.mouse.isDown = false;
	if (evt.touches.length == 0 && this.lastPinchDistance == -1 && 
		Math.abs(this.mouse.downPos.x - this.mouse.movePos.x) < 2)
	{
		var ap = getAbsolutePosition(this.canvas);
        	this.click(this.mouse.downPos.x - ap.x, this.mouse.downPos.y - ap.y);
	}
	this.mouse.panning = false;
	this.mouse.velocityMode = false;
	this.lastPinchDistance = -1;
}

ScrollableViewport.prototype.onTouchMove = function(evt)
{
	evt.preventDefault();
	if (evt.touches.length == 1)
	{
		evt.clientX = evt.touches[0].clientX;
		evt.clientY = evt.touches[0].clientY;
		this.mouseMove(evt);
	}
	else if (evt.touches.length == 2)
	{
		var dist = Math.sqrt((evt.touches[0].clientX - evt.touches[1].pageX) * (evt.touches[0].pageX - evt.touches[1].pageX) + (evt.touches[0].pageY - evt.touches[1].pageY) * (evt.touches[0].pageY - evt.touches[1].pageY));
		var f = dist / this.lastPinchDistance;
		var pinchCenter = (evt.touches[0].pageX + evt.touches[1].pageX) / 2; 
		var pinchCenterY = (evt.touches[0].pageY + evt.touches[1].pageY) / 2; 
		var zoomDelta = Math.log(f) / Math.log(this.zoomFactor);
		if (this.lastPinchDistance > 0)
		{
			var ap = getAbsolutePosition(this.canvas);
			this.changeZoom(zoomDelta, pinchCenter - ap.x, 
							pinchCenterY - ap.y);
		}
		this.lastPinchDistance = dist;

	}
}
	
ScrollableViewport.prototype.doLowPass = function () {
	var backMoveVelocity = 0.125;
	var oldLeft = this.filtered.left, 
		oldRight = this.filtered.right,
		oldTop = this.filtered.top,
		oldBottom = this.filtered.bottom;
	var threshold = 0.0001 * (this.right - this.left);
	this.filtered.left = this.left * this.lowPassAlpha + 
					(1.0 - this.lowPassAlpha) * this.filtered.left;
	this.filtered.right = this.right * this.lowPassAlpha + 
					(1.0 - this.lowPassAlpha) * this.filtered.right;
	this.filtered.top = this.top * this.lowPassAlpha + 
					(1.0 - this.lowPassAlpha) * this.filtered.top;
	this.filtered.bottom = this.bottom * this.lowPassAlpha + 
					(1.0 - this.lowPassAlpha) * this.filtered.bottom;
	if (!this.mouse.panning)
	{
		if (this.left < 0 || this.right > 1)
		{
			this.mouse.velocity.x = (this.left < 0) ? -backMoveVelocity * this.left :
				-backMoveVelocity * (this.right - 1);
		}
		else
		{
			this.mouse.velocity.x -= this.mouse.relaxCoeff * this.mouse.velocity.x;
		}
		this.left += this.mouse.velocity.x;
		this.right += this.mouse.velocity.x;
		if (this.top < 0 || this.bottom > 1)
		{
			this.mouse.velocity.y = (this.top < 0) ? -backMoveVelocity * this.top :
				-backMoveVelocity * (this.bottom - 1);
		}
		else
		{
			this.mouse.velocity.y -= this.mouse.relaxCoeff * this.mouse.velocity.y;
		}
		this.left += this.mouse.velocity.x;
		this.right += this.mouse.velocity.x;
		this.top += this.mouse.velocity.y;
		this.bottom += this.mouse.velocity.y;
	}	

	if ((this.mouse.panning && this.mouse.velocityMode) || 
		Math.abs(this.filtered.left - oldLeft) > threshold || 
		Math.abs(this.filtered.right - oldRight) > threshold || 
		Math.abs(this.filtered.top - oldTop) > threshold || 
		Math.abs(this.filtered.bottom - oldBottom) > threshold || 
		((new Date()).getTime() - this.lastRedrawTime > this.maxRedrawInterval) ||
		this.repaintRequested)
	{
		if (this.mouse.velocityMode)
		{
			this.size = this.right - this.left;
			this.left += this.mouse.velocity.x;
			this.right += this.mouse.velocity.x;
			if (this.right > 1.0)
			{
				this.right = 1.0;
				this.left = this.right - this.size;
			}
			if (this.left < 0)
			{
				this.left = 0;
				this.right = this.size;
			}
			var ySize = this.bottom - this.top;
			this.top += this.mouse.velocity.y;
			this.bottom += this.mouse.velocity.y;
			if (this.bottom > 1.0)
			{
				this.bottom = 1.0;
				this.top = this.bottom - ySize;
			}
			if (this.top < 0)
			{
				this.top = 0;
				this.bottom = ySize;
			}
		}
		this.draw();
		this.repaintRequested = false;
		//var drawTime = (new Date()).getTime() - drawStartTime.getTime();
		//errMsgShow("Draw time: " + drawTime + "ms");
		this.lastRedrawTime = (new Date()).getTime();
	}
};
ScrollableViewport.prototype.mouseDown = function (event) {
	event.preventDefault();
	this.mouse.isDown = true;
	this.mouse.downPos.x = event.pageX;
	this.mouse.downPos.y = event.pageY;
	this.mouse.movePos.x = this.mouse.downPos.x;
	this.mouse.movePos.y = this.mouse.downPos.y;
	this.mouse.lastMovePos.x = event.pageX;
	this.mouse.lastMovePos.y = event.pageY;
	this.mouse.button = event.button;
	
	var rc = getRelativeCoordinates(event, this.canvas);
	this.mouse.onScrollBar = rc.y > 
		this.container.offsetHeight - this.scrollBarThickness;
	this.mouse.onVertScrollbar = rc.x > 
		this.container.offsetWidth - this.scrollBarThickness;
	this.mouse.onScrollBar = this.mouse.onScrollBar || this.mouse.onVertScrollbar;
	event.preventDefault();
};
ScrollableViewport.prototype.mouseUp = function (event) {
	var doubleClickTimeThresh = 400;
	this.mouse.isDown = false;
	if (this.mouse.downPos.x == this.mouse.movePos.x &&
		this.mouse.downPos.y == this.mouse.movePos.y)
	{
		var rc = getRelativeCoordinates(event, this.canvas);
		var time = new Date().getTime();
		var timeDiff = time - this.mouse.lastClickTime;
		if (timeDiff < doubleClickTimeThresh)
		{
			this.doubleClick(rc.x, rc.y);
		}
		else
		{
			this.click(rc.x, rc.y);
		}
		this.mouse.lastClickTime = time;
	}
	this.mouse.onScrollBar = false;
	this.mouse.panning = false;
	this.mouse.velocityMode = false;
	event.preventDefault();
};
ScrollableViewport.prototype.mouseLeave = function (event) {
	this.mouse.panning = false;
	this.mouse.isDown = false;
	this.mouse.onScrollBar = false;
};
ScrollableViewport.prototype.mouseMove = function (event) {
	this.mouse.lastMovePos.x = this.mouse.movePos.x;
	this.mouse.lastMovePos.y = this.mouse.movePos.y;
	this.mouse.movePos.x = event.clientX;
	this.mouse.movePos.y = event.clientY;
	
	this.mouse.panning = this.mouse.isDown && 
		(this.mouse.downPos.x != this.mouse.movePos.x ||
		 this.mouse.downPos.y != this.mouse.movePos.y);
	var w = this.container.offsetWidth;
	var h = this.container.offsetHeight;
	if (this.mouse.onScrollBar)
	{
		var delta = 0;
		if (this.mouse.onVertScrollbar)
		{
			if (this.scrollBarsSizes[1] != 0 && !isNaN(this.scrollBarsSizes[1]))
			{
				delta = (this.mouse.movePos.y - this.mouse.lastMovePos.y) / 
						this.scrollBarsSizes[1];
				this.top += delta;
				this.bottom += delta;
				var s = this.bottom - this.top;
				this.bottom = Math.min(1.0, this.bottom);
				this.top = Math.min(this.bottom - s, this.top);
				
				this.top = Math.max(0.0, this.top);
				this.bottom = this.top + s;
			}
		}
		else
		{
			if (this.scrollBarsSizes[0] != 0 && !isNaN(this.scrollBarsSizes[0]))
			{
				delta = (this.mouse.movePos.x - this.mouse.lastMovePos.x) / 
					this.scrollBarsSizes[0];
				this.left += delta;
				this.right += delta;
			}
		}
	}
	else if (this.mouse.panning)
	{
		this.lowPassAlpha = 1.0;
		this.size = this.right - this.left;
		
		var scrAreaWidth = w - this.scrollAreaMargins.left - 
							this.scrollAreaMargins.right;
		var scrAreaHeight = h - this.scrollAreaMargins.top - 
							this.scrollAreaMargins.bottom;
		
		var delta = { x: 0, y: 0 };
		if (this.mouse.button == 0)
		{
			delta.x = -(this.mouse.movePos.x - this.mouse.lastMovePos.x) / scrAreaWidth;
			delta.y = -(this.mouse.movePos.y - this.mouse.lastMovePos.y) / scrAreaHeight;
			this.mouse.velocityMode = false;
		}
		else
		{
			delta.x = 0.25 * (this.mouse.movePos.x - this.mouse.downPos.x) / scrAreaWidth;
			delta.y = 0.25 * (this.mouse.movePos.y - this.mouse.downPos.y) / scrAreaHeight;
			delta.x *= this.size;
			delta.y *= this.bottom - this.top;
			this.mouse.velocityMode = true;
			this.mouse.velocity.x = delta.x * 0.5 + 0.5 * this.mouse.velocity.x;
			this.mouse.velocity.y = delta.y * 0.5 + 0.5 * this.mouse.velocity.y;
			return;
		}
		delta.x *= this.size;
		delta.y *= this.bottom - this.top;
		this.left += delta.x;
		this.right += delta.x;
		this.top += delta.y;
		this.bottom += delta.y;
		this.mouse.velocity = delta;
	}
	event.preventDefault();
};

ScrollableViewport.prototype.getCenter = function () 
{
	this.size = this.right - this.left;
	return this.left + - this.size / 2;
}

ScrollableViewport.prototype.setCenter = function (center) {
	this.size = this.right - this.left;
	this.left = center - this.size / 2;
	this.right = center + this.size / 2;
	if (this.right > 1.0)
	{
		this.right = 1.0;
		this.left = this.right - this.size;
	}
	if (this.left < 0)
	{
		this.left = 0;
		this.right = this.size;
	}
};

ScrollableViewport.prototype.click = function (x, y) {};
ScrollableViewport.prototype.doubleClick = function (x, y) {};
ScrollableViewport.prototype.onMouseWheel = function (event) {
	var delta = wheelDelta(event);
	var rc = getRelativeCoordinates(event, this.canvas);
	this.changeZoom(delta, rc.x, rc.y);
};
ScrollableViewport.prototype.changeZoom = function(delta, x, y) {
	var tlcont = this.container;
	var w = tlcont.offsetWidth - 
			this.scrollAreaMargins.left - 
			this.scrollAreaMargins.right;
	var h = tlcont.offsetHeight - 
			this.scrollAreaMargins.top - 
			this.scrollAreaMargins.bottom;
	x -= this.scrollAreaMargins.left;
	y -= this.scrollAreaMargins.top;
	this.lowPassAlpha = this.zoomLowPassAlpha;
	this.size = (this.right - this.left);
	var vSize = (this.bottom - this.top);
	this.zoom += delta;
	this.zoom = Math.max(this.minZoom, Math.min(this.maxZoom, this.zoom));
	this.size = Math.min(1.0, 1.0 / Math.pow(this.zoomFactor, this.zoom));
	vSize = Math.min(1.0, 1.0 / Math.pow(this.zoomFactor, this.zoom));
	if (this.zoom == 0)
	{
		this.left = 0.0;
		this.right = 1.0;
		if (this.zoomPolicy.allowVertical)
		{
			this.top = 0.0;
			this.bottom = 1.0;
		}
	}
	else
	{
		var c = this.left + (x / w) * 
			(this.right - this.left);
		this.left = - (c - this.left) * this.size / 
			(this.right - this.left + 0.0) + c;
		this.right = this.left + this.size;
		if (this.left < 0)
		{
			this.left = 0;
			this.right = this.size;
		}
		if (this.right > 1.0)
		{
			this.right = 1.0;
			this.left = 1.0 - this.size;
		}
		if (this.zoomPolicy.allowVertical)
		{
			c = this.top + (y / h) * 
				(this.bottom - this.top);
			this.top = - (c - this.top) * vSize / 
				(this.bottom - this.top + 0.0) + c;
			this.bottom = this.top + vSize;
			if (this.top < 0)
			{
				this.top = 0;
				this.bottom = vSize;
			}
			if (this.bottom > 1.0)
			{
				this.bottom = 1.0;
				this.top = 1.0 - vSize;
			}
		}
	}

} 
ScrollableViewport.prototype.draw = function() {};

ScrollableViewport.prototype.requestViewportUpdate = function() {
	this.repaintRequested = true;
};

ScrollableViewport.prototype.drawRect = function(ctx, x, y, w, h) {
	ctx.beginPath();
	ctx.rect(x, y, w, h);
	ctx.closePath();
	ctx.fill();
}
ScrollableViewport.prototype.drawScrollBar = function(ctx, vertical, viewFrom, viewTo, 
												position, size) {
	var w = this.container.offsetWidth;
	var h = this.container.offsetHeight;
	
	var dimension = vertical ? w : h;
	
	var scrollBarPos = dimension - this.scrollBarThickness;
	var thumbThickness = this.scrollBarThickness - 2 * this.scrollBarPadding;
	var thumbPos = position + viewFrom * size + 0.5 * thumbThickness;
	var thumbSize = Math.max(this.scrollBarThumbMinSize, 
		(viewTo - viewFrom) * size) - thumbThickness;
	ctx.fillStyle = this.scrollBarBgColor;
	ctx.lineCap = "round";
	ctx.lineWidth = thumbThickness;
	if (vertical)
	{	
		this.drawRect(ctx, scrollBarPos, position, this.scrollBarThickness, size);
		ctx.strokeStyle = this.scrollBarFgColor;
		ctx.beginPath();
		ctx.moveTo(scrollBarPos + 0.5 * this.scrollBarThickness, 
				   thumbPos);
		ctx.lineTo(scrollBarPos + 0.5 * this.scrollBarThickness,
				   thumbPos + thumbSize);
		ctx.stroke();
		
	}
	else
	{
		this.drawRect(ctx, position, scrollBarPos, size, this.scrollBarThickness);
		
		ctx.strokeStyle = this.scrollBarFgColor;
		ctx.beginPath();
		ctx.moveTo(thumbPos, scrollBarPos + 0.5 * this.scrollBarThickness);
		ctx.lineTo(thumbPos + thumbSize, scrollBarPos + 0.5 * this.scrollBarThickness);
		ctx.stroke();
	}
	
	this.scrollBarsSizes[vertical ? 1 : 0] = size;
};
