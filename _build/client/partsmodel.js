
var PartsModel = PartsModel ||  {}

PartsModel.Event = function(sender) {
	this._sender = sender;
	this._listeners = [];
};

PartsModel.Event.prototype = {
	attach : function (listener) {
		if (this._listeners.indexOf(listener) >= 0)
		{
			return;
		}
		this._listeners.push(listener);
	},
	detach : function (listener) {
		var index = this._listeners.indexOf(listener);
		if (index >= 0)
		{
			delete this._listeners[index];
		}
	},
	notify : function (args) {
		var index;

		for (index = 0; index < this._listeners.length; index += 1) {
			this._listeners[index](this._sender, args);
		}
	}
};

PartsModel.ObjectList = function() {
	this.items = [];
	this.added = new PartsModel.Event(this);
	this.removed = new PartsModel.Event(this);
};

PartsModel.ObjectList.prototype.constructor = PartsModel.ObjectList;
PartsModel.ObjectList.prototype = {
	add : function (item) {
		this.items.push(item);
		this.added.notify({item: item});
	},
	removeAt : function (index) {
		var item = this.items[index];
		this.items.splice(index, 1);
		this.removed.notify({item: item, index: index});
	},
	removeLast : function (index) {
		this.removeAt(this.items.length - 1);
	},
	removeFirst : function (index) {
		this.removeAt(0);
	},
	remove : function (item) {
		for (var i in this.items)
		{
			if (this.items[i] == item)
			{
				this.removeAt(i);
				return;
			}
		}
	},
	clear : function () {
		for (var i in this.items)
		{
			this.removed.notify({item: this.items[i], index: i});
		}
		this.items = [];
	},
	count: function () { return this.items.length; },
	toPlainObject: function () {
		var array = [];
		for (var i in this.items)
		{
			array.push(this.items[i].toPlainObject());
		}
		return array;
	},
    clone: function () {
        var c = new PartsModel.ObjectList();
        for (var i in this.items)
        {
            var clone = this.items[i].clone();
            c.add(clone);
        }
        return c;
    }
};

PartsModel.Part = function(name) {
	this.name = name;
	this.productionSteps = new PartsModel.ObjectList();
	this.changed = new PartsModel.Event(this);
	var self = this;
	var notifierFunc = function (sender, args) {
		self.changed.notify({});
	};
	this.productionSteps.added.attach(notifierFunc);
	this.productionSteps.removed.attach(notifierFunc);
};
PartsModel.Part.prototype.constructor = PartsModel.Part;
PartsModel.Part.prototype = {
	setName : function (newName) {
		this.name = newName;
		this.changed.notify({});
	},
	getMaximumStepNumber : function () {
		if (this.productionSteps.count() == 0)
		{
			return 0;
		}
		var stepNum = this.productionSteps.items[0].step;
		for (var i in this.productionSteps.items)
		{
			stepNum = Math.max(this.productionSteps.items[i].step, stepNum);
		}
		return stepNum;
	},
	toPlainObject: function () {
		return {name: this.name, 
			productionSteps: this.productionSteps.toPlainObject()};
	},
    clone: function () {
        var c = new PartsModel.Part(this.name);
        c.productionSteps = this.productionSteps.clone();
        return c;
    }
};


PartsModel.Resource = function(name) {
	this.name = name;
	this.finite = false;
	this.enabled = true;
	this.groups = [];
	this.changed = new PartsModel.Event(this);
	var self = this;
	var notifierFunc = function (sender, args) {
		self.changed.notify({});
	};
};
PartsModel.Resource.prototype.constructor = PartsModel.Resource;
PartsModel.Resource.prototype = {
	setName : function (newName) {
		this.name = newName;
		this.changed.notify({});
	},
	setFinite : function (value) { 
		this.finite = value;
		this.changed.notify({});
	},
	setEnabled : function (value) {
		this.enabled = value;
		this.changed.notify({});
	},
	toPlainObject: function() {
		return {name: this.name, finite: this.finite, enabled: this.enabled};
	}
};

PartsModel.RequiredPart = function(part, quantity) {
	this.part = part,
	this.quantity = quantity;
	this.changed = new PartsModel.Event(this);
};
PartsModel.RequiredPart.prototype.constructor = PartsModel.RequiredPart;
PartsModel.RequiredPart.prototype = {
	setPart : function (part) {
		this.part = part;
		this.changed.notify({});
	},
	setQuantity : function (q) {
		this.quantity = q;
		this.changed.notify({});
	},
	toPlainObject: function() {
		return { part: {name: (this.part ? this.part.name : null)}, 
				 quantity: this.quantity };
	},
    clone: function () {
        var obj = new PartsModel.RequiredPart(this.part, this.quantity);
        return obj;
    }
};

PartsModel.DemandOrder = function (orderNum) {
	this.demandDate = new Date();
	this.number = orderNum;
	this.requiredParts = new PartsModel.ObjectList();
	this.changed = new PartsModel.Event(this);
	var self = this;
	var notifierFunc = function (sender, args) {
		self.changed.notify({});
	};
	this.requiredParts.added.attach(notifierFunc);
	this.requiredParts.removed.attach(notifierFunc);
};

PartsModel.DemandOrder.prototype.constructor = PartsModel.DemandOrder;
PartsModel.DemandOrder.prototype = {
	setDemandDate: function (date) {
		this.demandDate = date;
		this.changed.notify();
	},
	setNumber: function (num) {
		this.number = num;
		this.changed.notify();
	},
	toPlainObject: function () {
		var rqParts = this.requiredParts.toPlainObject();
		var lineNum = 1;
		for (var r in rqParts)
		{
			rqParts[r].line = lineNum;
			lineNum++;
		}
		
		return {demandDate: this.demandDate, number: this.number, 
			requiredParts: rqParts };
	}
};

PartsModel.ProductionStep = function (step, opName) {
	this.step = step;
	this.operationName = opName;
	this.processDuration = 10 * 60;
	this.processDurationType = 'PerItem';
	this.processDurationUnit = 'm';
	this.resourceGroup = '';
	this.requiredParts = new PartsModel.ObjectList();
	this.resources = new PartsModel.ObjectList();
	this.changed = new PartsModel.Event(this);
	var self = this;
	var notifierFunc = function (sender, args) {
		self.changed.notify({});
	};
	this.requiredParts.added.attach(notifierFunc);
	this.requiredParts.removed.attach(notifierFunc);
	this.resources.added.attach(notifierFunc);
	this.resources.removed.attach(notifierFunc);
};
PartsModel.ProductionStep.prototype.constructor = PartsModel.ProductionStep;
PartsModel.ProductionStep.prototype = {
	setStep : function (step) {
		this.step = step;
		this.changed.notify({});
	},
	setOperationName : function (name) {
		this.operationName = name;
		this.changed.notify({});
	},
	setProcessDuration : function (value) {
		this.processDuration = value;
		this.changed.notify({});
	},
	setProcessDurationType: function (value) {
		this.processDurationType = value;
		this.changed.notify({});
	},
	setProcessDurationUnit: function (value) {
		this.processDurationUnit = value;
		this.changed.notify({});
	},
	setResourceGroup: function (value) {
		this.resourceGroup = value;
		this.changed.notify({});
	},
	toPlainObject: function() {
		var resources = [];
		for (var i in this.resources.items)
		{
			var r = this.resources.items[i];
			resources.push({name: r.name});
		}
		
		return { operationName: this.operationName, 
			step: this.step,
			processDuration: this.processDuration,
			processDurationType: this.processDurationType,
			processDurationUnit: this.processDurationUnit,
			requiredParts: this.requiredParts.toPlainObject(),
			resources: resources
		};
	},
    clone: function () {
        var c = new PartsModel.ProductionStep(this.step, 
                                              this.operationName);
        c.processDuration = this.processDuration;
        c.processDurationType = this.processDurationType;
        c.processDurationUnit = this.processDurationUnit;
        c.resourceGroup = this.resourceGroup;
        c.requiredParts = this.requiredParts.clone();
        c.resources = new PartsModel.ObjectList();
        for (var i in this.resources.items)
        {
            c.resources.items.push(this.resources.items[i]);
        }
        return c;
    }
};

PartsModel.Model = function () {
	this.parts = new PartsModel.ObjectList();
	this.resources = new PartsModel.ObjectList();
	this.partRemoved = new PartsModel.Event(this);
    this.partCloned = new PartsModel.Event(this);
};

PartsModel.Model.prototype.constructor = PartsModel.Model;
PartsModel.Model.prototype = {
	toPlainObject: function () {
		var lps = this.getLinearizedParts();
		var parts = [];
		for (var p in lps)
		{
			var pr = lps[p];
			parts.push(pr.toPlainObject());
		}
		
		return {
			parts: parts, 
			resources: this.resources.toPlainObject()
		};
	},
	toJSON: function () {
		return JSON.stringify(this.toPlainObject(), null, 2);
	},
	getLinearizedParts: function () {
		var parts = [];
		var addPart = function (part) {
			if (part == null)
			{
				return;
			}
			if (parts.indexOf(part) >= 0)
			{
				return;
			}
			parts.push(part);
			
			for (var ps in part.productionSteps.items)
			{
				var pstep = part.productionSteps.items[ps];
				for (var rp in pstep.requiredParts.items)
				{
					var rqpart = pstep.requiredParts.items[rp];
					addPart(rqpart.part);
				}
			}	
		};
		
		for (var p in this.parts.items)
		{
			var part = this.parts.items[p];
			addPart(part);
		}
		return parts;
	},
    getReferencableParts: function (srcPart) {
        var parts = this.getLinearizedParts();
        var removeFoundParts = function (part) {
            var idx = parts.indexOf(part);
            if (idx >= 0)
            {
                parts.splice(idx, 1);
            }
            for (var ps in part.productionSteps.items)
            {
                var pstep = part.productionSteps.items[ps];
                for (var rp in pstep.requiredParts.items)
                {
                    var rqpart = pstep.requiredParts.items[rp];
                    removeFoundParts(rqpart.part);
                }
            }
        };
        removeFoundParts(srcPart);
        return parts;
    },
    getPartsReferencableBy: function (destPart) {
        var lps = this.getLinearizedParts();
        var searchForPart = function (part, partToLookFor) {
            for (var ps in part.productionSteps.items)
            {
                var pstep = part.productionSteps.items[ps];
                for (var rp in pstep.requiredParts.items)
                {
                    var rqpart = pstep.requiredParts.items[rp];
                    if (rqpart.part == partToLookFor)
                    {
                        return true;
                    }
                    else
                    {
                        return searchForPart(rqpart.part, partToLookFor);
                    }
                }
            }
        };
        var validParts = [];
        for (var i in lps)
        {
            var part = lps[i];
            if (destPart == part || validParts.indexOf(part) >= 0)
            {
                continue;
            }
            if (!searchForPart(part, destPart))
            {
                validParts.push(part);
            }
        }
        return validParts;
    },
	getProductionSteps: function () {
		var parts = this.getLinearizedParts();
		var psteps = [];
		for (var p in parts)
		{
			var part = parts[p];
			for (var ps in part.productionSteps.items)
			{
				psteps.push(part.productionSteps.items[ps]);
			}
		}
		return psteps;
	},
	addPartsCSV: function (lines) {
		this.parts.clear();
		var parts = [];
		
		var self = this;
		var addPart = function(pn) {
			if (!(pn in parts))
			{
				var newPart = new PartsModel.Part(pn);
				parts[pn] = {part: newPart, ops: []};
				self.parts.add(newPart);
			}
		};
		
		for (var l in lines)
		{
			var line = lines[l];
			var pnn = line.PartNumber;
			addPart(pnn);
			var part = parts[pnn];
			var opNum = line.OperationNumber; 
			var ps = null;
			if (!(opNum in part.ops))
			{
				ps = new PartsModel.ProductionStep(
					parseInt(line.StepNumber ? line.StepNumber : opNum), 
												"Operation " + opNum);
				part.ops[opNum] = ps;
				part.part.productionSteps.add(ps);
			}
			var rpn = line.RequiredPartNumber;
			var q = line.RequiredQuantity ? line.RequiredQuantity : 1;
			addPart(rpn);
			part.ops[opNum].requiredParts.add(new PartsModel.RequiredPart(parts[rpn].part, q));
		}
	},
	addProductionStepsCSV: function (lines) {
		var linearParts = this.getLinearizedParts();
		var findOrCreate = function (partName, opNum) {
			for (var p in linearParts)
			{
				var part = linearParts[p];
				if (part.name != partName)
				{
					continue;
				}
					
				for (var ps in part.productionSteps.items)
				{
					var pstep = part.productionSteps.items[ps];
					if (pstep.step == opNum)
					{
						return pstep;
					}
				}
				var pstep = new PartsModel.ProductionStep(opNum, "");
				part.productionSteps.add(pstep);
				return pstep;
			}
			return null;
		}
		for (var l in lines)
		{
			var line = lines[l];
			var pstep = findOrCreate(line.PartNumber, 
									 parseInt(line.OperationNumber));
			if (pstep == null)
			{
				continue;
			}
			var pdts = [];
			pdts['TimePerItem'] = 'PerItem';
			pdts['TimePerBatch'] = 'Constant';
			var pdt = (line.ProcessDurationType in pdts) ? 
						pdts[line.ProcessDurationType] : 'PerItem';
			var units = [];
			units['Minute'] = {n: 'm', m: 60};
			units['Second'] = {n: 's', m: 1};
			units['Hour'] = {n: 'h', m: 3600};
			var unit = (line.ProcessTimePeriod in units) ? 
						units[line.ProcessTimePeriod] : units['Minute'];
			
			pstep.setOperationName(line.OperationName);
			pstep.setProcessDuration(line.ProcessValue * unit.m);
			pstep.setProcessDurationType(pdt);
			pstep.setProcessDurationUnit(unit.n);
			pstep.setResourceGroup(line.ResourceGroup);
		}
	},
	addResourcesCSV: function (lines) {
		this.resources.clear();
		var self = this;
		var findOrCreate = function (name) {
			for (var r in self.resources.items)
			{
				var res = self.resources.items[r];
				if (res.name == name)
				{
					return res;
				}
			}
			var res = new PartsModel.Resource(name);
			self.resources.add(res);
			return res;
		};
		var psteps = this.getProductionSteps();
		for (var l in lines)
		{
			var line = lines[l];
			var resource = findOrCreate(line.Resource);
			if (resource.groups.indexOf(line.ResourceGroup) < 0)
			{
				resource.groups.push(line.ResourceGroup);
			}
			for (var ps in psteps)
			{
				var pstep = psteps[ps];
				if (pstep.resourceGroup == line.ResourceGroup)
				{
					pstep.resources.add(resource);
				}
			}
		}
	},
	addCSV: function (fileName, lines) {
		if (fileName.indexOf("ProductionSteps") >= 0)
		{
			if (this.parts.count() > 0)
			{
				this.addProductionStepsCSV(lines);
			}
		}
		else if (fileName.indexOf("ResourceGroupResources") >= 0)
		{
			this.addResourcesCSV(lines);
		}
		else
		{
			this.addPartsCSV(lines);
		}
	},
	findProductionStepParent: function (pstep) {
		var parts = this.getLinearizedParts();
		for (var p in parts)
		{
			var part = parts[p];
			for (var ps in part.productionSteps.items)
			{
				if (part.productionSteps.items[ps] == pstep)
				{
					return part;
				}
			}
		}
		return null;
	},
	removePart: function (partToRemove) {
		
		var searchParts = function (part) {
			for (var ps in part.productionSteps.items)
			{
				var pstep = part.productionSteps.items[ps];
				var rpsToRemove = [];
				for (var rp in pstep.requiredParts.items)
				{
					var rqpart = pstep.requiredParts.items[rp];
					if (rqpart.part == partToRemove)
					{
						rpsToRemove.push(rp);
					}
					else
					{
						searchParts(rqpart.part);
					}
				}
				for (var rp in rpsToRemove)
				{
					pstep.requiredParts.removeAt(rp);
				}
			}	
		};
		
		for (var p in this.parts.items)
		{
			var part = this.parts.items[p];
			searchParts(part);
		}
		var idx = this.parts.items.indexOf(partToRemove);
		if (idx >= 0)
		{
			this.parts.removeAt(idx);
		}
		for (var ps in partToRemove.productionSteps.items)
		{
			var pstep = partToRemove.productionSteps.items[ps];
			pstep.requiredParts.clear();
		}
		partToRemove.productionSteps.clear();
		this.partRemoved.notify(partToRemove);
	},
    clonePart: function (part) {
        var name = '';
        var num = 0;
        var lps = this.getLinearizedParts();
        var partExists = false;
        do
        {
            num++;
            name = part.name + ' ' + num;
            
            partExists = false;
            for (var i in lps)
            {
                if (lps[i].name == name)
                {
                    partExists = true;
                    break;
                }
            }
        }
        while (partExists);
        
        var clone = part.clone();
        
        clone.name = name;
        this.parts.add(clone);
        this.partCloned.notify({sourcePart: part, clonedPart: clone});
    }
    
};


