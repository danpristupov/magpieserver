﻿Ext.define('PanelWithBackButton', {
	extend: 'Ext.panel.Panel',

    goBackText: 'Go back',
    
	initComponent: function() {
		var handler = this.config.backBtnHandler || function () { alert("!!"); };
		this.tools = [{
			type: 'home',
			baseCls: 'tool-back',
			tooltip: this.goBackText,
			handler: handler
		}];
		this.callParent(arguments);
	},
	
	onRender: function() {
		this.callParent(arguments);

		this.header.addCls('backbutton-panel-header');
	}
});


Ext.define('ProductionStepModel', {
	extend: 'Ext.data.Model',
	fields: [ 'step', 'operationName' ]
});

Ext.define('RequiredPartModel', {
	extend: 'Ext.data.Model',
	fields: [ 'name', 'quantity' ]
});

Ext.define('ResourceModel', {
	extend: 'Ext.data.Model',
	fields: [ 'name', 'finite', 'enabled' ]
});

Ext.define('BOMManager', {
	extend: 'Ext.Panel',
	
	graphControl: null,
	sidebar: null,
    partToCopy: null,
    lastPasteTime: 0,
    
    removePstepDialogTitleText: 'Confirm deletion', 
    removePstepDialogMsgText: 'Do you really want to delete the operation?',
    defaultOperationText: 'Operation',
    operationsGridTitleText: 'Operations',
    stepColumnText: 'Step',
    nameColumnText: 'Name',
    editOperationText: 'Edit Operation',
    removePartDialogTitleText: 'Confirm deletion', 
    removePartDialogMsgText: 'Are you sure you want to remove the part?',
    partPanelTitleText: 'Product',
    removePartButtonText: 'Remove Part',
    notSetText: '(not set)',
    timeUnitsHoursText: 'hours',
    timeUnitsMinutesText: 'minutes',
    timeUnitsSecondsText: 'seconds',
    removeRqpartDialogTitleText: 'Confirm deletion', 
    removeRqpartDialogMsgText: 'Do you really want to delete selected required part?',
    perItemText: 'Duration Per Item',
    perTaskText: 'Task Duration',
    durationNumFieldLabelText: 'Duration',
    durationTypeComboLabelText: 'Duration Type',
    durationDialogTitleText: 'Operation Duration',
    durationContainerLabelText: 'Duration',
    okButtonText: 'OK',
    cancelButtonText: 'Cancel',
    resourcesGridTitleText: 'Resources',
    resourcesGridNameColumnText: 'Name',
    rqGridTitleText: 'Required Parts',
    rqGridNameColumnText: 'Name',
    rqGridQuantityColumnText: 'Quantity',
    psPanelPerItemText: 'item',
    psPanelPerTaskText: 'task',
    psPanelTitleText: 'Operation',
    psDurationPanelTitleText: 'Operation Duration',
    titleText: 'Bill Of Materials',
    expandAllButtonText: 'Expand All',
	collapseAllButtonText: 'Collapse All',
    copyPartButtonText: 'Copy Part',
    pastePartButtonText: 'Paste Part',
    
	addCSVData: function (data, fileName) {
		var csv = new CSV(data, { header: true });
		var lines = csv.parse();
		var psteps = (fileName.indexOf("ProductionSteps") >= 0);
		var resources = (fileName.indexOf("ResourceGroupResources") >= 0);
		var model = (psteps || resources) ? this.graphControl.model : new PartsModel.Model();
		
		model.addCSV(fileName, lines);
		if (!psteps && !resources)
		{
			this.graphControl.setModel(model);
		}
	},
	
	handleFileSelect: function (evt) {
		evt.stopPropagation();
		evt.preventDefault();
		
		var self = this;
		var files = evt.dataTransfer.files;

		var output = [];
		for (var i = 0, f; f = files[i]; i++) {
			var reader = new FileReader();

			reader.onload = (function(theFile) {
				return function(e) {
					self.addCSVData(e.target.result, theFile.name);
				};
			})(f);

			var contents = reader.readAsText(f, 'UTF-8');
		}
	},
	
	handleDragOver: function (evt) {
		evt.stopPropagation();
		evt.preventDefault();
		evt.dataTransfer.dropEffect = 'copy';
	},
	
	createSampleModel: function () {
		var model = new PartsModel.Model();
		var fryingPan = new PartsModel.Resource("Сковородка");
		var heater = new PartsModel.Resource("Печка");
		var blender = new PartsModel.Resource("Миксер");
		model.resources.add(fryingPan);
		model.resources.add(heater);
		model.resources.add(blender);
			
		var salt = new PartsModel.Part("Соль");
		var flour = new PartsModel.Part("Мука");
		var egg = new PartsModel.Part("Яйцо");
		var yeast = new PartsModel.Part("Дрожжи");
		var cabbage = new PartsModel.Part("Капуста");
		var onion = new PartsModel.Part("Лук");
		var oil = new PartsModel.Part("Подсолнечное масло");
		
		var dough = new PartsModel.Part("Тесто");
		var doughPs = new PartsModel.ProductionStep(1, "Замесить");
		doughPs.resources.add(blender);
		doughPs.requiredParts.add(new PartsModel.RequiredPart(salt, 1));
		doughPs.requiredParts.add(new PartsModel.RequiredPart(flour, 1));
		doughPs.requiredParts.add(new PartsModel.RequiredPart(egg, 1));
		doughPs.requiredParts.add(new PartsModel.RequiredPart(yeast, 1));
		dough.productionSteps.add(doughPs);
		var payload = new PartsModel.Part("Начинка");
		var payloadPs = new PartsModel.ProductionStep(1, "Обжарить");
		payloadPs.resources.add(fryingPan);
		payloadPs.requiredParts.add(new PartsModel.RequiredPart(cabbage, 1));
		payloadPs.requiredParts.add(new PartsModel.RequiredPart(onion, 1));
		payloadPs.requiredParts.add(new PartsModel.RequiredPart(oil, 1));
		payload.productionSteps.add(payloadPs);
		
		var pie = new PartsModel.Part("Пирожок с капустой");
		var bake = new PartsModel.ProductionStep(1, "Испечь");
		bake.resources.add(heater);
		bake.requiredParts.add(new PartsModel.RequiredPart(dough, 1));
		bake.requiredParts.add(new PartsModel.RequiredPart(payload, 1));
		var finalBake = new PartsModel.ProductionStep(2, "Сделать глазурь");
		finalBake.requiredParts.add(new PartsModel.RequiredPart(egg, 1));
		pie.productionSteps.add(bake);
		pie.productionSteps.add(finalBake);
		model.parts.add(pie);
		return model;
	},
	
	getExportableData: function () {
		return this.graphControl.model.toPlainObject()
	},

	expandAllButtonClicked: function () {
		this.graphControl.expandAllNodes(true);
	},
	
	collapseAllButtonClicked: function () {
		this.graphControl.expandAllNodes(false);
	},
    pastePart: function () {
        if (this.partToCopy == null)
        {
            return;
        }
        this.graphControl.model.clonePart(this.partToCopy);
    },
    copyPart: function () {
        var so = this.graphControl.selectedObject;
        if (!(so instanceof PartsModel.Part))
        {
            return;
        }
        this.partToCopy = so;
    },
	
	graphControlSelectionChanged: function () {
		var so = this.graphControl.selectedObject;
		this.sidebar.removeAll();
		var storeData = [];
        var self = this;
		
		var nameChanged = function (arg) {
			so.name = arg.value;
		};
		
		if (so instanceof PartsModel.Part)
		{
			var psStore = Ext.create('Ext.data.Store', {
				model: 'ProductionStepModel',
				data: storeData
			});
			var partData = {
				name: so.name
			};
			
			var loadPartOperations = function () {	
				storeData = [];
				for (var ps in so.productionSteps.items)
				{
					var pst = so.productionSteps.items[ps];
					storeData.push({key: ps, operationName: pst.operationName, 
						step: pst.step, obj: pst});
				}
				psStore.setData(storeData);
			};
			loadPartOperations();
			
			var opGrid = null;
			
			var removePstep = function () {
				if (opGrid.selModel.getCount() <= 0)
				{
					return;
				}
				Ext.Msg.show({
					title: this.removePstepDialogTitleText,
					message: this.removePstepDialogMsgText,
					buttons: Ext.Msg.YESNO,
					icon: Ext.Msg.QUESTION,
					fn: function(btn) {
						if (btn === 'yes') {
							so.productionSteps.removeAt(opGrid.selModel
													.getSelection()[0].data.key);
							loadPartOperations();
						}
					}
				});
				
			};
			var addPstep = function () {
				so.productionSteps.add(new PartsModel.ProductionStep(
					so.getMaximumStepNumber() + 1, self.defaultOperationText));
				loadPartOperations();
			};
			var opNameChanged = function (arg) {
				opGrid.selModel.getSelection()[0].data.obj.operationName = arg.value;
			};
			
			var movePstep = function (delta) {
				if (opGrid.selModel.getCount() <= 0)
				{
					return;
				}
				var index = opGrid.selModel.getSelection()[0].data.key;
				console.log(index, delta);
				if ((index == 0 && delta < 0) ||
					(index == so.productionSteps.count() - 1) && delta > 0)
				{
					return;
				}
				var nextIndex = (parseInt(index) + delta) + "";
				var a = so.productionSteps.items[index];
				var b = so.productionSteps.items[nextIndex];
				console.log(a, b);
				so.productionSteps.items[index] = b;
				so.productionSteps.items[nextIndex] = a;
				var step = b.step;
				b.setStep(a.step);
				a.setStep(step);
				loadPartOperations();
				opGrid.selModel.doSelect(opGrid.store.data.items[nextIndex]);
			};
			
			var movePstepDown = function (arg) {
				movePstep(1);
			};
			
			var movePstepUp = function (arg) {
				movePstep(-1);
			};
			
			var addPstepBtn = Ext.create('Ext.button.Button', {
				text: '+',
				handler: addPstep
			});
			
			var removePstepBtn = Ext.create('Ext.button.Button', {
				text: '-',
				handler: removePstep,
				disabled: true
			});
			
			var movePstepDownBtn = Ext.create('Ext.button.Button', {
				text: '\u25bc',
				handler: movePstepDown,
				disabled: true
			});
			
			var movePstepUpBtn = Ext.create('Ext.button.Button', {
				text: '\u25b2',
				handler: movePstepUp,
				disabled: true
			});
			
			var selectionChanged = function () { 
				var buttons = [ removePstepBtn, movePstepUpBtn, movePstepDownBtn ];
				for (var b in buttons)
				{
					buttons[b].setDisabled(opGrid.selModel.getCount() <= 0);
				}
				var index = opGrid.selModel.getSelection()[0].data.key;
				movePstepUpBtn.setDisabled(index == 0);
				movePstepDownBtn.setDisabled(index == so.productionSteps.count() - 1);
			};
			var self = this;
			opGrid = Ext.create('Ext.grid.Panel', {
				title: this.operationsGridTitleText,
				xtype: 'grid',
				enableCtxMenu: false,
				enableHdMenu: false,
				menuDisabled: true,
				store: psStore,
				columns: [
					{ text: self.stepColumnText, 
                        dataIndex: 'step', hideable: false, 
                        sortable:false, menuDisabled: true,
						align: 'right', width: 60 }, 
					{ 
                        text: self.nameColumnText, dataIndex: 'operationName', 
                        flex: 1, border: false,
                        hideable: false, sortable:false, menuDisabled: true
					},
					{
						xtype: 'actioncolumn', 
						width: 24,
						menuDisabled: true,
						items: [{
							icon: 'resources/extjs/images/tab-bar/default-plain-scroll-right.png',
							tooltip: self.editOperationText,
							handler: function(grid, rowIndex, colIndex, arg3, arg4, record) {
								self.graphControl.setSelectedObject(record.data.obj);
							}
						}]
					}
				],
				bbar: {
					items: [ addPstepBtn, removePstepBtn, '->', 
								movePstepDownBtn, movePstepUpBtn]
				},
				plugins: [{
					ptype: 'cellediting',
					clicksToEdit: 2
				}],
				autoScroll: true,
				height: 240
			});
			
			opGrid.selModel.on('select', selectionChanged);
			opGrid.on('itemdblclick', function (s, rec) { 
				self.graphControl.setSelectedObject(rec.data.obj);
			});
			
			var nameField = Ext.create('Ext.form.field.Text', {
				xtype: 'textfield',
				name: 'partNameEdit',
				fieldLabel: self.nameColumnText,
				bind: {
					value: '{name}'
				},
				selectOnFocus: true,
				margin: '8 8 8 4',
				labelWidth: 52,
				labelStyle: 'text-align:right',
				enableKeyEvents: true,
				listeners: {
					'change': nameChanged
				}
			});
			
			var deleteSelectedPart = function () {
				var so = self.graphControl.selectedObject;
				if (!(so instanceof PartsModel.Part))
				{
					return;
				}
				
				Ext.Msg.show({
					title: self.removePartDialogTitleText,
					message: self.removePartDialogMsgText,
					buttons: Ext.Msg.YESNO,
					icon: Ext.Msg.QUESTION,
					fn: function(btn) {
						if (btn === 'yes') {
							var model = self.graphControl.model;
							model.removePart(so);
						}
					}
				});
			};
			
			var partPanel = Ext.create('Ext.Panel', {
			    xtype: 'panel',
			    ui: 'basePanel',
			    frame: true, 
                layout: {
					type: 'vbox',
					pack: 'start',
					align: 'stretch'
				},
				bodyPadding: 0,
				style: {
				    margin: '0px -2px 0px -2px'
				},
				viewModel: {
					data: partData
				},
				
				title: self.partPanelTitleText,

				items: [nameField, opGrid, 
					{
						xtype: 'button',
						text: self.removePartButtonText,
						handler: deleteSelectedPart,
						margin: '8 8 4 4'
					}
				]
			});
			this.sidebar.add(partPanel);
			
			this.graphControl.nodeDoubleClicked.attach(function () {
				nameField.focus(true, 10);
			});
			
			var deleteKeyMap = new Ext.util.KeyMap(document, {
					key: [46], 
					fn: deleteSelectedPart
				}
			);
            var ctrlCMap = new Ext.util.KeyMap(document, {
					key: "c",
                    ctrl: true,
					fn: function () { self.copyPart(); }
				}
			);
            var ctrlVMap = new Ext.util.KeyMap(document, {
					key: "v",
                    ctrl: true,
					fn: function () { 
                        var ct = (new Date()).getTime();
                        var thr = 300;
                        if (ct - self.lastPasteTime < thr)
                        {
                            return;
                        }
                        self.pastePart(); 
                        self.lastPasteTime = ct;
                    }
				}
			);
			
		}
		else if (so instanceof PartsModel.ProductionStep)
		{
            var parentPart = this.graphControl.model.findProductionStepParent(so);
			var resourceData = this.graphControl.model.resources.toPlainObject();
			
			var resStore = Ext.create('Ext.data.Store', {
				model: 'ResourceModel',
				data: resourceData
			});
			
			var store = Ext.create('Ext.data.Store', {
				model: 'RequiredPartModel',
				data: storeData
			});
			
			var loadRequiredParts = function () {
				storeData = [];
				for (var ps in so.requiredParts.items)
				{
					var rqp = so.requiredParts.items[ps];
					var name = rqp.part ? rqp.part.name : self.notSetText;
					storeData.push({key: ps, name: name, 
							quantity: rqp.quantity, obj: rqp});
				}
				store.setData(storeData);
			};
			loadRequiredParts();
			
			var np = this.graphControl.model.getPartsReferencableBy(parentPart);
			var partsData = [];
			var partsByName = [];
			partsByName[self.notSetText] = null;
			partsData.push({name: self.notSetText});
			for (var p in np)
			{
				var part = np[p];
				partsData.push({name: part.name});
				partsByName[part.name] = part;
			}
			
			var partsStore = Ext.create('Ext.data.Store', {
				fields: ['name'],
				data: partsData
			});
            partsStore.sort('name');
			
			var timeUnits = [
				{t: 3600, sn: 'h', name: self.timeUnitsHoursText}, 
				{t: 60, sn: 'm', name: self.timeUnitsMinutesText},
				{t: 1, sn: 's', name: self.timeUnitsSecondsText}
			];
			
			var findTimeUnit = function (seconds) {
				for (var u in timeUnits)
				{
					if (so.processDurationUnit == timeUnits[u].sn)
					{
						return timeUnits[u];
					}
				}
				return timeUnits[0];
			};
			
			
			
			var data = {};
			
			var updateProdStepData = function () {
				data = {
					name: so.operationName,
					step: so.step,
					duration: so.processDuration,
					durationType: so.processDurationType
				};
				if (psPanel && psPanel.viewModel)
				{
					psPanel.viewModel.setData(data);
				}
			};
			updateProdStepData();
			
			var quantityChanged = function (arg) {
				rqGrid.selModel.getSelection()[0].data
					.obj.setQuantity(arg.value);
			};
			
			var reqPartChanged = function (arg) {
				rqGrid.selModel.getSelection()[0].data.obj
					.setPart(partsByName[arg.value]);
			};
			
			var rqGrid = null;
			
			var addRqpart = function (arg) {
				so.requiredParts.add(new PartsModel.RequiredPart(null, 1));
				loadRequiredParts();
			};
			
			var removeRqpart = function (arg) {
				if (rqGrid.selModel.getCount() <= 0)
				{
					return;
				}
				Ext.Msg.show({
					title: self.removeRqpartDialogTitleText,
					message: self.removeRqpartDialogMsgText,
					buttons: Ext.Msg.YESNO,
					icon: Ext.Msg.QUESTION,
					fn: function(btn) {
						if (btn === 'yes') {
							so.requiredParts.removeAt(rqGrid.selModel
														.getSelection()[0].data.key);
							loadRequiredParts();
						}
					}
				});
			};
			
			var removeRqpartBtn = Ext.create('Ext.button.Button', {
				text: '-',
				handler: removeRqpart,
				disabled: true
			});
			
			var rqPartSelectionChanged = function () { 
				removeRqpartBtn.setDisabled(rqGrid.selModel.getCount() <= 0);
			};
			
			var showDurationDialog = function () {
				var unitsStore = Ext.create('Ext.data.Store', {
					fields: ['name', 't', 'sn' ],
					data: [
						{ name: self.timeUnitsSecondsText, t: 1, sn: 's' }, 
						{ name: self.timeUnitsMinutesText, t: 60, sn: 'm' },
						{ name: self.timeUnitsHoursText, t: 3600, sn: 'h' }
					]
				});
				var typeStore =  Ext.create('Ext.data.Store', {
					fields: ['name', 'value'],
					data: [
						{ name: self.perItemText, value: 'PerItem' }, 
						{ name: self.perTaskText, value: 'Constant' }
					]
				});
				
				var durationNumField = Ext.create('Ext.form.field.Number', {
					xtype: 'numberfield',
					fieldLabel: self.durationNumFieldLabelText
				});
				var unitsCombo = Ext.create('Ext.form.field.ComboBox', {
					xtype: 'combobox',
					store: unitsStore,
					queryMode: 'local',
					displayField: 'name',
					valueField: 't',
					editable: false,
					width: 96
				});
				var typeCombo = Ext.create('Ext.form.field.ComboBox', {
					xtype: 'combobox',
					store: typeStore,
					fieldLabel: self.durationTypeComboLabelText,
					queryMode: 'local',
					displayField: 'name',
					valueField: 'value',
					editable: false
				});
				var saveAndClose = function () { 
					var unit = unitsCombo.value;
					so.setProcessDuration(durationNumField.value * unit);
					so.setProcessDurationType(typeCombo.value);
					so.setProcessDurationUnit(unitsCombo.getSelection().data.sn);
					dialog.close();
					updateProdStepData();
				};
				
				var closeDialog = function () {
					dialog.close();
				};
				
				var dialog = Ext.create("Ext.Window",{
					title: self.durationDialogTitleText,
					width: 400,
					height: 192,
					closable: true,
					modal: true,
					bodyPadding: 10,
					defaults: {
						labelStyle: 'text-align:right'
					},
					items: [
						{
							xtype: 'fieldcontainer',
							fieldLabel: self.durationContainerLabelText,
							combineErrors: true,
							msgTarget : 'side',
							layout: 'hbox',
							defaults: {
								flex: 1,
								hideLabel: true
							},
							items: [
								durationNumField, 
								unitsCombo
							]
						},
						typeCombo
					],
					buttons: [
						{
							text: self.okButtonText,
							handler: saveAndClose
						},
						{
							text: self.cancelButtonText,
							handler: closeDialog
						}
					]
				});
				var unit = findTimeUnit(so.processDuration).t;
				durationNumField.setValue(so.processDuration / unit);
				unitsCombo.setValue(unit);
				typeCombo.setValue(so.processDurationType);
				
				dialog.show();
			};
			
			var resGrid = Ext.create("Ext.grid.Panel", {
				title: self.resourcesGridTitleText,
				xtype: 'grid',
				store: resStore,
				style: {
				    margin: '-1px 0px 0px 0px'
				},
				columns: [
					{ 
						text: self.resourcesGridNameColumnText, 
						dataIndex: 'name', 
						flex: 1,
						menuDisabled: true,
						hideable: false
					}
				],
				selModel: {
					selType: 'checkboxmodel',
					checkOnly: true,
					showHeaderCheckbox: false
				},
				autoScroll: true,
				maxHeight: 240
			});
			
			var self = this;
			
			var goBack = function () {
				self.graphControl.setSelectedObject(parentPart);
			};
			
			var rqGrid = Ext.create("Ext.grid.Panel", {
				title: self.rqGridTitleText,
				xtype: 'grid',
				store: store,
				columns: [
					{ 
						text: self.rqGridNameColumnText, 
						dataIndex: 'name', 
						flex: 1,
						menuDisabled: true,
						hideable: false,
						editor: {
							xtype: 'combobox',
							store: partsStore,
							queryMode: 'local',
							displayField: 'name',
							valueField: 'name',
							editable: false,
							listeners: {
								'change': reqPartChanged
							}
						}
					}, 
					{ 
						text: self.rqGridQuantityColumnText, 
						dataIndex: 'quantity', 
						align: 'right',
						menuDisabled: true,
						width: 90,
						hideable: false,
						
						editor: {
							xtype: 'numberfield',
							componentCls: 'numberfield-right',
							selectOnFocus: true,
							hideTrigger: true,
							listeners: {
								'change': quantityChanged
							}
						}
					}
				],
				bbar: {
					items: [{
						text: '+',
						handler: addRqpart
					}, removeRqpartBtn ]
				},
				plugins: [{
					ptype: 'cellediting',
					clicksToEdit: 2
				}],
				autoScroll: true,
				maxHeight: 300
			});
			rqGrid.selModel.on('select', rqPartSelectionChanged);
			var psPanel = Ext.create('PanelWithBackButton', {
			    xtype: 'panel',
			    ui: 'basePanel',
			    frame: true,
				layout: {
					type: 'vbox',
					pack: 'start',
					align: 'stretch'
				},
				bodyPadding: 0,
				style: {
				    margin: '0px -2px 0px -2px'
				},
				viewModel: {
					data: data,
					formulas: {
						durationText: function (get) {
							var seconds = get('duration');
							var unit = findTimeUnit(seconds);
							var d = seconds / unit.t;
							var sf = (get('durationType') == 'PerItem') ? 
                                    self.psPanelPerItemText : self.psPanelPerTaskText;
							return d + " " + unit.name + " / " + sf;
						}
					}
				},
				title: self.psPanelTitleText,
				items: [
					{
						xtype: 'textfield',
						name: 'psNameEdit',
						fieldLabel: 'Name',
						bind: {
							value: '{name}'
						},
						selectOnFocus: true,
						margin: '8 8 8 4',
						labelWidth: 52,
						labelStyle: 'text-align:right',
						enableKeyEvents: true,
						listeners: {
							'change': function (arg) { so.operationName = arg.value }
						}
					}, 
					rqGrid,
					resGrid,
					{
					    xtype: 'panel',
					    ui: 'basePanelWithBorder',
					    frame: true,
						title: self.psDurationPanelTitleText,
						style: {
						    margin: '-1px 0px 0px 0px'
						},
						layout: {
							type: 'hbox'
						},
						items: [
							{
								xtype: 'textfield',
								margin: '8 4 8 4',
								selectOnFocus: true,
								flex: 1,
								readOnly: true,
								bind: {
									value: '{durationText}'
								}
							},
							{
								xtype: 'button',
								margin: '8 8 0 0',
								height: 24,
								text: '...',
								handler: showDurationDialog
							}
						]
					}
				],
				backBtnHandler: goBack
			});
			for (var r in so.resources.items)
			{
				var rs = so.resources.items[r];
				var index = this.graphControl.model.resources.items.indexOf(rs);
				resGrid.selModel.select(resStore.data.items[index], true, true);
			}
			var resGridSelectionChanged = function () {
				so.resources.clear();
				for (var i = 0; i < resGrid.selModel.getSelection().length; i++)
				{
					var index = resourceData.indexOf(
						resGrid.selModel.getSelection()[i].data);
					so.resources.add(self.graphControl
									.model.resources.items[index]);
				}
				console.log(JSON.stringify(so.resources.toPlainObject()));
			};
			resGrid.selModel.on("select", resGridSelectionChanged);
			resGrid.selModel.on("deselect", resGridSelectionChanged);
			
			this.sidebar.add(psPanel);
		}
		else if (so instanceof PartsModel.RequiredPart)
		{
			
		}
	},
	
	config: {
		title: 'Bill of Materials',
		xtype: 'panel',
		layout: {
			type: 'border'
		},
		style: {
            margin: '0px -1px -1px -1px'
		},
		region: 'center',
		items: [
			{
				region: 'center',
				xtype: 'panel',
				html: '<div id=graphContainer style="background:#fff;' + 
						'width:100%;height:100%;display:block"><canvas ' +
						'id="graphControl"></canvas></div>',
				style: {
				    margin: '-1px -1px -1px -1px'
				}
			},
			{
				region: 'east',
				name: 'sidebar',
				xtype: 'panel',
				ui: 'sidebarPanel',
				frame: true,
				html: '',
				width: 220,
				items: [],
				style: {
				    margin: '-1px 0px -1px -1px'
				}
			}
		],

		tbar: [
		{
			text: 'Expand All',
			name: 'expandAllButton'
		},
		{
			text: 'Collapse All',
			name: 'collapseAllButton'
		},
		{
			text: 'Copy Part',
			name: 'copyPartButton'
		},
		{
			text: 'Paste Part',
			name: 'pastePartButton'
		}]
	},

	constructor: function() {
		this.callParent(arguments);
		
		
		var self = this;
		
		this.on('afterrender', function () {
			if (this.graphControl != null)
			{
				return;
			}
			self.graphControl = new GraphView(document.body, 
										document.getElementById('graphContainer'), 
										document.getElementById('graphControl'));
			self.graphControl.layoutEngine = new DagreLayoutEngine();
			self.graphControl.setModel(self.createSampleModel());
			self.graphControl.layoutNodes();
			self.graphControl.selectionChanged.attach(
				function() { self.graphControlSelectionChanged() });
			
			self.sidebar = Ext.ComponentQuery.query('[name=sidebar]')[0];
			
			var handlers = [];
			handlers['expandAllButton'] = function () { self.expandAllButtonClicked(); };
			handlers['collapseAllButton'] = function () { self.collapseAllButtonClicked(); };
            handlers['copyPartButton'] = function () { self.copyPart(); };
            handlers['pastePartButton'] = function () { self.pastePart(); };
            var texts = [];
            texts['expandAllButton'] = self.expandAllButtonText;
			texts['collapseAllButton'] = self.collapseAllButtonText;
            texts['copyPartButton'] = self.copyPartButtonText;
            texts['pastePartButton'] = self.pastePartButtonText;
			
			for (var h in handlers)
			{
				var btn = Ext.ComponentQuery.query('[name=' + h + ']')[0];
				var handler = handlers[h];
				btn.setHandler(handler);
                btn.setText(texts[h]);
			}
			
		});
        this.setTitle(this.titleText);
    },
});


