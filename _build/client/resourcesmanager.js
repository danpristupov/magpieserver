Ext.define('ResourcesManager', {
	extend: 'Ext.Panel',
	config: {
		title: 'Resources',
		xtype: 'panel',
		bodyStyle:{"background-color":"#fff"},
		layout: {
			type: 'border'
		},
		style: {
		    margin: '-1px 0px -1px 0px'
		},
	},
	resGrid: null,
	resStore: null,
	resources: null,
    
    titleText: 'Resources',
    removeDialogTitleText: 'Confirm deletion',
    removeDialogMsgText: 'Do you really want to delete the selected resource?',
    finiteText: 'Finite',
    infiniteText: 'Infinite',
    nameColumnText: 'Name',
    typeColumnText: 'Type',
    enabledColumnText: 'Enabled',
    
	addCSVData: function (data, filename) {
		var csv = new CSV(data, { header: true });
		var lines = csv.parse();
		this.resources = new PartsModel.ObjectList();
		
		for (var l in lines)
		{
			var line = lines[l];
			var res = new PartsModel.Resource(line.Name);
			res.enabled = line.IsEnabled == 'TRUE';
			res.finite = line.IsFinite ? (line.IsFinite == 'TRUE') : true;
			this.resources.add(res);
		}
		this.updateGrid();
		
	},
	getExportableData: function () {
		if (!this.resources)
		{
			return [];
		}
		var objects = this.resources.toPlainObject();
		for (var i in objects)
		{
			objects[i].displayPosition = parseInt(i) + 1;
		}
		return objects;
	},
	updateGrid: function () {
		var storeData = [];
		for (var r in this.resources.items)
		{
			var res = this.resources.items[r];
			storeData.push({name: res.name, finite: (res.finite ? this.finiteText : this.infiniteText), 
							enabled: res.enabled, key: r, obj: res});
		}
		this.resStore.setData(storeData);
	},
	constructor: function() {
		this.callParent(arguments);
		
		var self = this;
		
		var addResource = function () {
			self.resources.add(new PartsModel.Resource(""));
			self.updateGrid();
		};
		
		var removeResource = function () {
			if (self.resGrid.selModel.getCount() <= 0)
			{
				return;
			}
			Ext.Msg.show({
				title: self.removeDialogTitleText,
				message: self.removeDialogMsgText,
				buttons: Ext.Msg.YESNO,
				icon: Ext.Msg.QUESTION,
				fn: function(btn) {
					if (btn === 'yes') {
						self.resources.removeAt(self.resGrid.selModel
												.getSelection()[0].data.key);
						self.updateGrid();
					}
				}
			});
		};
		
		var moveResource = function (delta) {
			if (self.resGrid.selModel.getCount() <= 0)
			{
				return;
			}
			var index = self.resGrid.selModel.getSelection()[0].data.key;
			console.log(index, delta);
			if ((index == 0 && delta < 0) ||
				(index == self.resources.count() - 1) && delta > 0)
			{
				return;
			}
			var nextIndex = (parseInt(index) + delta) + "";
			var a = self.resources.items[index];
			var b = self.resources.items[nextIndex];
			console.log(a, b);
			self.resources.items[index] = b;
			self.resources.items[nextIndex] = a;
			self.updateGrid();
			self.resGrid.selModel.doSelect(self.resGrid.store.data.items[nextIndex]);
		};
		
		var moveResourceDown = function () {
			moveResource(1);
		};
		
		var moveResourceUp = function () {
			moveResource(-1);
		};
		
		
		var addResourceBtn = Ext.create('Ext.button.Button', {
			text: '+',
			handler: addResource
		});
			
		var removeResourceBtn = Ext.create('Ext.button.Button', {
			text: '-',
			disabled: true,
			handler: removeResource
		});
			
		var moveResourceDownBtn = Ext.create('Ext.button.Button', {
			text: '\u25bc',
			disabled: true,
			handler: moveResourceDown
		});
			
		var moveResourceUpBtn = Ext.create('Ext.button.Button', {
			text: '\u25b2',
			disabled: true,
			handler: moveResourceUp
		});
		
		this.resStore = Ext.create('Ext.data.Store', {
			model: 'ResourceModel',
			data: []
		});
		
		var typesStore = Ext.create('Ext.data.Store', {
			fields: ['name', 'value'],
			data: [{name: self.finiteText, value: true}, 
                    {name: self.infiniteText, value: false}]
		});
		var typeChanged = function (e, value) {
			var res = self.resGrid.selModel.getSelection()[0].data.obj;
			res.setFinite(value == this.finiteText);
		};
		var nameChanged = function (e, value) {
			var res = self.resGrid.selModel.getSelection()[0].data.obj;
			res.setName(value);
		};
		var enabledChanged = function (column, recordIndex, checked) {
			var res = self.resources.items[recordIndex];
			res.setEnabled(checked);
		};
		
		this.resGrid = Ext.create('Ext.grid.Panel', {
			store: this.resStore,
			xtype: 'row-numberer',
			margin: '10 220 15 15',
			region: 'center',
			tbar: {
			    items: [
				addResourceBtn, removeResourceBtn, '->',
				moveResourceDownBtn, moveResourceUpBtn
			    ],
			    style: {
			        border: '0px'
			    }
			},
			columns: [
				{
					xtype: 'rownumberer'
				},
				{ 
					text: this.nameColumnText, 
					dataIndex: 'name', 
					flex: 1,
					editor: {
						xtype: 'textfield',
						listeners: {
							'change': nameChanged
						}
					}
				},
				{
					text: this.typeColumnText,
					dataIndex: 'finite',
					editor: {
						xtype: 'combobox',
						store: typesStore,
						queryMode: 'local',
						displayField: 'name',
						valueField: 'name',
						editable: false,
						listeners: {
							'change': typeChanged
						}
					}
				},
				{
					text: this.enabledColumnText,
					xtype: 'checkcolumn',
					dataIndex: 'enabled',
					editor: {
						xtype: 'checkbox',
						cls: 'x-grid-checkheader-editor'
					},
					listeners: {
						'checkchange': enabledChanged
					}
				}
			],
			autoScroll: true,
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 2
			}],
			autoScroll: true
		});
		
		var selectionChanged = function () { 
			var buttons = [ removeResourceBtn, moveResourceUpBtn, moveResourceDownBtn ];
			for (var b in buttons)
			{
				buttons[b].setDisabled(self.resGrid.selModel.getCount() <= 0);
			}
			var index = self.resGrid.selModel.getSelection()[0].data.key;
			moveResourceUpBtn.setDisabled(index == 0);
			moveResourceDownBtn.setDisabled(index == self.resources.count() - 1);
		};
		
		this.resGrid.selModel.on('select', selectionChanged);
		this.add(this.resGrid);
        
        this.setTitle(this.titleText);
	}
});