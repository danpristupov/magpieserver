Ext.define('locale.GlobalTranslations', {
    override: 'GlobalTranslations',
    billOfMaterialsText: 'Материалы',
    demandOrdersText: 'Заказы',
    resourcesText: 'Ресурсы',
    timelineText: 'Временная линия',
    filesGridNameColumnText: 'Имя',
    filesGridTypeColumnText: 'Тип',
    filesGridDeleteColumnText: 'Удалить',
    uploadWindowTitle: 'Загрузка файлов',
    uploadWindowDropZoneText: 'Перетащите другие файлы сюда',
    uploadWindowUploadBtnText: 'Загрузить',
    navigationWorkspaceText: 'Рабочее пространство',
    navigationConfigrationText: 'Конфигурация',
    downloadJsonBtnText: 'Загрузить JSON',
    dropZoneText: 'Перетащите файлы CSV сюда',
    logoutBtnText: 'Выход'
});

Ext.define('locale.DemandOrdersManager', {
    override: 'DemandOrdersManager',
    
    titleText: 'Заказы',
    editButtonDoneText: 'Завершить',
    editButtonEditText: 'Редактировать',
    notSetText: '(не задано)',
    rqpartRemoveDialogTitleText: 'Подтверждение удаления',
    rqpartRemoveDialogMsgText: 'Удалить выбранную строку заказа?',
    rqGridNameColumnText: 'Имя',
    rqGridQuantityColumnText: 'Количество',
    demandDateText: 'Дата',
});

Ext.define('locale.ResourcesManager', {
    override: 'ResourcesManager',
    
    titleText: 'Ресурсы',
    removeDialogTitleText: 'Подтверждение удаления',
    removeDialogMsgText: 'Вы действительно хотите удалить выбранный ресурс?',
    finiteText: 'Конечный',
    infiniteText: 'Бесконечный',
    nameColumnText: 'Имя',
    typeColumnText: 'Тип',
    enabledColumnText: 'Активен'
});

Ext.define('locale.PanelWithBackButton', {
    override: 'PanelWithBackButton',
    
    goBackText: 'Назад'
});

Ext.define('locale.BOMManager', {
    override: 'BOMManager',
    
    removePstepDialogTitleText: 'Подтверждение удаления', 
    removePstepDialogMsgText: 'Вы действительно хотите удалить выбранную операцию?',
    defaultOperationText: 'Операция',
    operationsGridTitleText: 'Операции',
    stepColumnText: 'Шаг',
    nameColumnText: 'Имя',
    editOperationText: 'Редактировать операцию',
    removePartDialogTitleText: 'Подтверждение удаления', 
    removePartDialogMsgText: 'Вы действительно хотите удалить выбранный продукт?',
    partPanelTitleText: 'Продукт',
    removePartButtonText: 'Удалить продукт',
    notSetText: '(не задано)',
    timeUnitsHoursText: 'часов',
    timeUnitsMinutesText: 'минут',
    timeUnitsSecondsText: 'секунд',
    removeRqpartDialogTitleText: 'Подтверждение удаления', 
    removeRqpartDialogMsgText: 'Вы действительно хотите удалить выбранный требуемый продукт?',
    perItemText: 'На единицу',
    perTaskText: 'Все задание',
    durationNumFieldLabelText: 'Продолжительность',
    durationTypeComboLabelText: 'Тип',
    durationDialogTitleText: 'Продолжительность операции',
    durationContainerLabelText: 'Продолжительность',
    okButtonText: 'OK',
    cancelButtonText: 'Отмена',
    resourcesGridTitleText: 'Ресурсы',
    resourcesGridNameColumnText: 'Имя',
    rqGridTitleText: 'Требуемые продукты',
    rqGridNameColumnText: 'Имя',
    rqGridQuantityColumnText: 'Количество',
    psPanelPerItemText: 'единицу',
    psPanelPerTaskText: 'задание',
    psPanelTitleText: 'Операция',
    psDurationPanelTitleText: 'Продолжительность',
    titleText: 'Материалы',
    expandAllButtonText: 'Развернуть все',
	collapseAllButtonText: 'Свернуть все',
    copyPartButtonText: 'Копировать продукт',
    pastePartButtonText: 'Вставить продукт'
});