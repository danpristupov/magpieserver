Ext.define('locale.GlobalTranslations', {
    override: 'GlobalTranslations',
    billOfMaterialsText: 'Bill Of Materials',
    demandOrdersText: 'Demand Orders',
    resourcesText: 'Resources',
    timelineText: 'Timeline',
    filesGridNameColumnText: 'Name',
    filesGridTypeColumnText: 'Type',
    filesGridDeleteColumnText: 'Delete',
    uploadWindowTitle: 'File Upload',
    uploadWindowDropZoneText: 'Drop another files here',
    uploadWindowUploadBtnText: 'Upload',
    navigationWorkspaceText: 'Workspace',
    navigationConfigrationText: 'Configuration',
    downloadJsonBtnText: 'Download JSON',
    dropZoneText: 'Drop CSV files here',
    logoutBtnText: 'Log out'
});

Ext.define('locale.DemandOrdersManager', {
    override: 'DemandOrdersManager',
    
    titleText: 'Demand Orders',
    editButtonDoneText: 'Done',
    editButtonEditText: 'Edit',
    notSetText: '(not set)',
    rqpartRemoveDialogTitleText: 'Confirm deletion',
    rqpartRemoveDialogMsgText: 'Do you really want to delete the selected line?',
    rqGridNameColumnText: 'Name',
    rqGridQuantityColumnText: 'Quantity',
    demandDateText: 'Demand Date',
    
});

Ext.define('locale.ResourcesManager', {
    override: 'ResourcesManager',
    
    removeDialogTitleText: 'Confirm deletion',
    removeDialogMsgText: 'Do you really want to delete the selected resource?',
    finiteText: 'Finite',
    infiniteText: 'Infinite',
    nameColumnText: 'Name',
    typeColumnText: 'Type',
    enabledColumnText: 'Enabled'
});


Ext.define('locale.PanelWithBackButton', {
    override: 'PanelWithBackButton',
    
    goBackText: 'Go back'
});

Ext.define('locale.BOMManager', {
    override: 'BOMManager',
    
    removePstepDialogTitleText: 'Confirm deletion', 
    removePstepDialogMsgText: 'Do you really want to delete the operation?',
    defaultOperationText: 'Operation',
    operationsGridTitleText: 'Operations',
    stepColumnText: 'Step',
    nameColumnText: 'Name',
    editOperationText: 'Edit Operation',
    removePartDialogTitleText: 'Confirm deletion', 
    removePartDialogMsgText: 'Are you sure you want to remove the part?',
    partPanelTitleText: 'Product',
    removePartButtonText: 'Remove Part',
    notSetText: '(not set)',
    timeUnitsHoursText: 'hours',
    timeUnitsMinutesText: 'minutes',
    timeUnitsSecondsText: 'seconds',
    removeRqpartDialogTitleText: 'Confirm deletion', 
    removeRqpartDialogMsgText: 'Do you really want to delete selected required part?',
    perItemText: 'Duration Per Item',
    perTaskText: 'Task Duration',
    durationNumFieldLabelText: 'Duration',
    durationTypeComboLabelText: 'Duration Type',
    durationDialogTitleText: 'Operation Duration',
    durationContainerLabelText: 'Duration',
    okButtonText: 'OK',
    cancelButtonText: 'Cancel',
    resourcesGridTitleText: 'Resources',
    resourcesGridNameColumnText: 'Name',
    rqGridTitleText: 'Required Parts',
    rqGridNameColumnText: 'Name',
    rqGridQuantityColumnText: 'Quantity',
    psPanelPerItemText: 'item',
    psPanelPerTaskText: 'task',
    psPanelTitleText: 'Operation',
    psDurationPanelTitleText: 'Operation Duration',
    titleText: 'Bill Of Materials',
    expandAllButtonText: 'Expand All',
	collapseAllButtonText: 'Collapse All',
    copyPartButtonText: 'Copy Part',
    pastePartButtonText: 'Paste Part'
});