Ext.define('PendingFileModel', {
    extend: 'Ext.data.Model',
    fields: [ 'name', 'type' ]
});

Ext.define('GlobalTranslations', {
    billOfMaterialsText: 'Bill Of Materials',
    demandOrdersText: 'Demand Orders',
    resourcesText: 'Resources',
    timelineText: 'Timeline',
    filesGridNameColumnText: 'Name',
    filesGridTypeColumnText: 'Type',
    filesGridDeleteColumnText: 'Delete',
    uploadWindowTitle: 'File Upload',
    uploadWindowDropZoneText: 'Drop another files here',
    uploadWindowUploadBtnText: 'Upload',
    navigationWorkspaceText: 'Workspace',
    navigationConfigrationText: 'Configuration',
    downloadJsonBtnText: 'Download JSON',
    dropZoneText: 'Drop CSV files here',
    logoutBtnText: 'Log out'
});

var gts = Ext.create('GlobalTranslations');
var openedTabs = [];
var tabPanel = null;
var pendingFiles = [];
var uploadWindow = null;
var sidebarCollapsed = false;
var filesStore = Ext.create('Ext.data.Store', {
    model: 'PendingFileModel',
    data: pendingFiles
});

var logOut = function () {
    window.location.href = '/logout';
};

var prepareRequestData = function () { 
    var reqObject = {};
    var bommanager = openedTabs[gts.billOfMaterialsText];
    if (bommanager)
    {
            reqObject = bommanager.getExportableData();
    }
    
    var doTab = openedTabs[gts.demandOrdersText];
    if (doTab)
    {
            reqObject.demandOrders = doTab.getExportableData();
    }
    var resourcesTab = openedTabs[gts.resourcesText];
    if (resourcesTab && resourcesTab.getExportableData().length > 0)
    {
            reqObject.resources = resourcesTab.getExportableData();
    }
    return reqObject;
};

var downloadJSON = function () {
    var download = function(filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + 
                encodeURIComponent(text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }
    download("model.json", JSON.stringify(prepareRequestData()));
};

var requestTimelineData = function () {
    
    var reqObject = prepareRequestData();
    reqObject.token = '2510F90C-D3E5-4381-B5CE-5FBEFE4CD8BA';
    
    console.log(JSON.stringify(reqObject, null, 2));
    
    var url = '/createtimeline';
    if (document.URL.indexOf('.html') >= 0)
    {
            url = 'http://magpieplx.cloudapp.net/api/values/CreateTimeLine2';
    }
    
    Ext.Ajax.useDefaultXhrHeader = false;
    Ext.Ajax.request({
        url: url, 
        method: 'POST',
        headers: { 'Content-Type': 'application/json; charset=utf-8', 
                                'Accept': 'application/json, text/json' },
        useDefaultXhrHeader: false,
        params : JSON.stringify(reqObject),
        success: function(conn, response, options, eOpts) {
            openTab(gts.timelineText);
            var tl = openedTabs[gts.timelineText];
            var respObject = JSON.parse(conn.responseText);

            tl.addCSVData(respObject.operations, "Operations.csv");
            tl.addCSVData(respObject.relationships, "Relationships.csv");
            tl.addCSVData(respObject.calendarPeriods, "CalendarPeriods.csv");
        },
        failure: function(conn, response, options, eOpts) {
            console.log(conn.responseText);
        }
});
};



var openTab = function (compName) {

    var classNames = [];
    classNames[gts.timelineText] = 'TimelineComponent';
    classNames[gts.demandOrdersText] = 'DemandOrdersManager';
    classNames[gts.billOfMaterialsText] = 'BOMManager';
    classNames[gts.resourcesText] = 'ResourcesManager';
    
    var tabs = Ext.ComponentQuery.query('#tabpanel > ');
    for (var i in tabs)
    {
        if (!tabs[i].title)
        {
                continue;
        }
        if (tabs[i].title == compName)
        {
                tabPanel.setActiveTab(tabs[i]);
                return tabs[i];
        }
    }
    if (!(compName in classNames))
    {
        return;
    }
    
    var newTab = Ext.create(classNames[compName], 
                                            { title: compName, closable: true });
    tabPanel.add(newTab);
    tabPanel.setActiveTab(newTab);
    openedTabs[compName] = newTab;
    return newTab;
};

var acceptPendingFiles = function () {
    var tabsByTypes = [];
    tabsByTypes['BillOfMaterials'] = gts.billOfMaterialsText;
    tabsByTypes['DemandOrders'] = gts.demandOrdersText;
    tabsByTypes['ProductionSteps'] = gts.billOfMaterialsText;
    tabsByTypes['ResourceRelationships'] = gts.billOfMaterialsText;
    tabsByTypes['TimelineOperations'] = gts.timelineText;
    tabsByTypes['TimelineLinks'] = gts.timelineText;
    tabsByTypes['TimelineCalendarPeriods'] = gts.timelineText;
    tabsByTypes['Resources'] = gts.resourcesText;
    
    
    for (var f in pendingFiles)
    {
        var file = pendingFiles[f];
        if (!(file.type in tabsByTypes))
        {
                continue;
        }
        var tab = openTab(tabsByTypes[file.type]);
        tab.addCSVData(file.data, file.name);
    }
    
    pendingFiles = [];
};

var showUploadWindow = function () {
    var saveAndClose = function () { 
        acceptPendingFiles();
        uploadWindow.close();
        uploadWindow = null;
    };
    
    var filesGrid = Ext.create('Ext.grid.Panel', {
        enableCtxMenu: false,
        enableHdMenu: false,
        menuDisabled: true,
        flex: 1,
        store: filesStore,
        columns: [
            { text: gts.filesGridNameColumnText, dataIndex: 'name', flex: 1, hideable: false, sortable:false }, 
            { text: gts.filesGridTypeColumnText, dataIndex: 'type', width: 150, hideable: false, sortable:false },
            {
                text: gts.filesGridDeleteColumnText,
                xtype: 'actioncolumn',
                items: [{
                    icon: 'resources/custom/file-upload-delete.png',
                    tooltip: gts.filesGridDeleteColumnText,
                    handler: function(grid, rowIndex, colIndex, arg3, arg4, record) {
                        pendingFiles.splice(rowIndex, 1);
                        filesStore.setData(pendingFiles);
                    }
                }]
            }
        ],
        bodyStyle: 'border-bottom-width: 0px'
    });
    
    uploadWindow = Ext.create("Ext.Window",{
        title: gts.uploadWindowTitle,
        width: 600,
        height: 380,
        closable: true,
        modal: true,
        bodyPadding: 10,
        defaultFocus: 'uploadButton',
        defaults: {
            labelStyle: 'text-align:right'
        },
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [
            filesGrid,
            {
                xtype: 'panel',
                height: 32,
                html: '<div id="wdropzone" style="display:block; ' + 
                    'text-align:center; font-family: Arial, sans; ' +
                    'font-size: 14pt; color: #888; width:100%; padding-top: 6px;' +
                    'height:100%">' + gts.uploadWindowDropZoneText + '</div>'
            }
        ],
        buttons: [{
            itemId: 'uploadButton',
            text: gts.uploadWindowUploadBtnText,
            handler: saveAndClose
        }]
    });
    uploadWindow.on('afterrender', function () {
        var dropzone = document.getElementById('wdropzone');
        dropzone.addEventListener('dragover', function (e) {  
                handleDragOver(e); }, false);
        dropzone.addEventListener('drop', function (e) {
                handleFileSelect(e); }, false);
    });
    uploadWindow.show();
};

var addCSVFile = function (data, name) {
    var types = [];
    types['BillOfMaterials.csv'] = 'BillOfMaterials';
    types['DemandOrders.csv'] = 'DemandOrders';
    types['ProductionSteps.csv'] = 'ProductionSteps';
    types['ResourceGroupResources.csv'] = 'ResourceRelationships';
    types['Operations.csv'] = 'TimelineOperations';
    types['Relationships.csv'] = 'TimelineLinks';
    types['CalendarPeriods.csv'] = 'TimelineCalendarPeriods';
    types['Resources.csv'] = 'Resources';
    
    var type = (name in types) ? types[name] : 'Unknown';
    
    pendingFiles.push({name: name, data: data, type: type});
    filesStore.setData(pendingFiles);
    if (uploadWindow == null)
    {
        showUploadWindow();
    }
};

var handleFileSelect = function (e) {
    e.stopPropagation();
    e.preventDefault();
                    
    var self = this;
    var files = e.dataTransfer.files;

    var output = [];
    for (var i = 0, f; f = files[i]; i++) {
        var reader = new FileReader();

        reader.onload = (function(theFile) {
            return function(e) {
                addCSVFile(e.target.result, theFile.name);
            };
        })(f);
        reader.readAsText(f, 'UTF-8');
    }
};


var handleDragOver = function (e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
};

var toggleMicroSidebar = function () {
    var tl = Ext.ComponentQuery.query('[name=treelist]')[0];
    tl.setMicro(!sidebarCollapsed);
    sidebarCollapsed = !sidebarCollapsed;
    var sidebar = ct = tl.ownerCt;
    sidebar.setWidth(sidebarCollapsed ? 48 : 220);
    var btn = Ext.ComponentQuery.query('[name=collapseSidebarButton]')[0];
    btn.setText(sidebarCollapsed ? '>>' : '<<');
    
};

Ext.application({
    name: 'BOMManager',
    launch: function() {
        
        var leftSidebar = Ext.create('Ext.Panel', {
            region: 'west',
            xtype: 'panel',
            name: 'leftSidebar',
            reference: 'treelistContainer',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            border: false,
            style: {
                margin: '-1px -2px -1px -1px'
            },
            viewModel: {
                stores: {
                    navItems: {
                        type: 'tree',
                        root: {
                            expanded: true,
                            children: [
                                {
                                    iconCls: 'fa fa-clock-o',
                                    text: gts.timelineText,
                                    leaf: true
                                },
                                {
                                    iconCls: 'fa fa-calendar-o',
                                    text: gts.demandOrdersText,
                                    leaf: true
                                },
                                {
                                    iconCls: 'fa fa-share-alt',
                                    text: gts.billOfMaterialsText,
                                    leaf: true
                                },
                                {
                                    iconCls: 'fa fa-tasks',
                                    text: gts.resourcesText,
                                    leaf: true
                                }
                            ]
                        }
                    }
                }
            },
            items: [{
                xtype: 'treelist',
                name: 'treelist',
                bind: '{navItems}',
                rootVisible: false,
                iconSize: 48,
                expanderFirst: false,
                listeners: {
                    'selectionchange' : function (list, item) { openTab(item.data.text); }
                },
                flex: 1
            },
            {
                xtype: 'button',
                text: '<<',
                name: 'collapseSidebarButton',
                handler: toggleMicroSidebar
            }]
        });

    tabPanel = Ext.create('Ext.TabPanel', {
        xtype: 'tabpanel',
        region: 'center',
        id: 'tabpanel',
            style: {
                margin: '-1px -1px -1px 1px'
            }
        });

        Ext.create('Ext.container.Viewport', {
            layout: {
        type: 'border'
            },
            items: [{
                xtype: 'panel',
                layout: {type: 'border'},
                region: 'center',
                items: [ leftSidebar, tabPanel ],
                tbar: [
                    {
                        text: '▶︎',
                        handler: requestTimelineData
                    },
                    {
                        text: gts.downloadJsonBtnText,
                        handler: downloadJSON
                    },
                    {
                        xtype: 'panel',
                        flex: 1,
                        height: 32,
                        html: '<div id="dropzone" style="display:block; ' + 
                                        'text-align:center; font-family: Arial, sans; ' +
                                        'font-size: 14pt; color: #888; width:100%; padding-top: 6px;' +
                                        'height:100%">' + gts.dropZoneText + '</div>'
                    },
                    {
                        xtype: 'panel',
                        html: session.user.name,
                        border: 0
                    },
                    {
                        text: gts.logoutBtnText,
                        handler: logOut
                    }
                ]
            }]
        });
    }
});
Ext.onReady(function () {
    var dropzone = document.getElementById('dropzone');
    dropzone.addEventListener('dragover', function (e) {  
            handleDragOver(e); }, false);
    dropzone.addEventListener('drop', function (e) {
            handleFileSelect(e); }, false);
});
