/**
 * Calculate a 32 bit FNV-1a hash
 * Found here: https://gist.github.com/vaiorabbit/5657561
 * Ref.: http://isthe.com/chongo/tech/comp/fnv/
 *
 * @param {string} str the input value
 * @param {boolean} [asString=false] set to true to return the hash value as 
 *     8-digit hex string instead of an integer
 * @param {integer} [seed] optionally pass the hash of the previous chunk
 * @returns {integer | string}
 */
function hashFnv32a(str, asString, seed) {
    /*jshint bitwise:false */
    var i, l,
        hval = (seed === undefined) ? 0x811c9dc5 : seed;

    for (i = 0, l = str.length; i < l; i++) {
        hval ^= str.charCodeAt(i);
        hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
    }
    if( asString ){
        // Convert to 8 digit hex string
        return ("0000000" + (hval >>> 0).toString(16)).substr(-8);
    }
    return hval >>> 0;
}

Date.prototype.getWeekNumber = function() {
    var d = new Date(+this);
    d.setHours(0, 0, 0);
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
};

Date.prototype.getUTCWeekNumber = function() {
	return this.getWeekNumber();
};
	
function Timeline(rootElement, container, canvas) {
	ScrollableViewport.call(this, rootElement, container, canvas, 0, 16);
	this.lineHeight = 0.2;
	
	this.timeScaleFormats = {
		year: {
			dateFormats: ["yyyy"],
			idFunc: function(time) { return new Date(time * 1000).getFullYear(); },
			timeFunc: function(id) { return new Date(Date.UTC(id, 0, 1, 0, 0, 0, 0)) }
		},
		month: {
			dateFormats: ["mmmm yyyy", "mmm yyyy", "mm"],
			idFunc: function(time) { var d = new Date(time * 1000); 
				return d.getMonth() + 12 * d.getFullYear(); },
			timeFunc: function(id) { return new Date(Date.UTC(id / 12, (id % 12), 1, 0, 0, 0, 0)) }
		},
		week: {
			dateFormats: ["Week w: dd/mm/yyyy", "w: dd/mm/yyyy", "dd/mm/yyyy"],
			idFunc: function(time) {
				var y = new Date(time * 1000).getFullYear();
				var firstMonday = new Date(y, 0, 1).getTime() / 1000 + 
					(8 - new Date(y, 0, 1).getDay()) * 86400;
				var daysFromFirstMon = (time - firstMonday) / 86400;
				var weekNum = daysFromFirstMon < 0 ? 0 : 
					(Math.floor(daysFromFirstMon / 7) + 1);
				
				return 53 * y + weekNum; 
			},
			timeFunc: function(id) {
				var y = id / 53;
				var weekNum = id % 53;
				if (weekNum <= 0)
				{
					return new Date(Date.UTC(y, 0, 1));
				}
				else
				{
					weekNum--;
					var firstMonday = new Date(Date.UTC(y, 0, 1)).getTime() / 1000 + 
						(8 - new Date(y, 0, 1).getDay()) * 86400;
					return new Date((firstMonday + weekNum * 7 * 86400) * 1000);
				}
			}
		},
		day: {
			dateFormats: ["dddd dd/mm/yyyy", "ddd dd/mm/yyyy", "ddd dd", "ddd"],
			idFunc: function(time) { return Math.floor(time / 86400); },
			timeFunc: function(id) { return new Date(id * 86400 * 1000); }
		},
		time12h: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 43200); },
			timeFunc: function(id) { return new Date(id * 43200 * 1000); }
		},
		time6h: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 21600); },
			timeFunc: function(id) { return new Date(id * 21600 * 1000); }
		},
		time3h: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 10800); },
			timeFunc: function(id) { return new Date(id * 10800 * 1000); }
		},
		time1h: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 3600); },
			timeFunc: function(id) { return new Date(id * 3600 * 1000); }
		},
		time30m: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 1800); },
			timeFunc: function(id) { return new Date(id * 1800 * 1000); }
		},
		time15m: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 900); },
			timeFunc: function(id) { return new Date(id * 900 * 1000); }
		},
		time10m: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 600); },
			timeFunc: function(id) { return new Date(id * 600 * 1000); }
		},
		time5m: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 300); },
			timeFunc: function(id) { return new Date(id * 300 * 1000); }
		},
		time1m: {
			dateFormats: ["hh:MM TT", "HH:MM"],
			idFunc: function(time) { return Math.floor(time / 60); },
			timeFunc: function(id) { return new Date(id * 60 * 1000); }
		}
	};
	
	this.timeScaleConfs = [
		{
			lengthCondition: function(l) { return l > 93 * 86400; }, 
			formats: [null, this.timeScaleFormats.year, this.timeScaleFormats.month]
		},
		{
			lengthCondition: function(l) { return l <= 93 * 86400; }, 
			formats: [
				this.timeScaleFormats.year, 
				this.timeScaleFormats.month,
				this.timeScaleFormats.week 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 40 * 86400; }, 
			formats: [
				this.timeScaleFormats.month, 
				this.timeScaleFormats.week,
				this.timeScaleFormats.day 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 14 * 86400; }, 
			formats: [
				this.timeScaleFormats.week, 
				this.timeScaleFormats.day,
				this.timeScaleFormats.time12h 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 4 * 86400; }, 
			formats: [
				this.timeScaleFormats.week, 
				this.timeScaleFormats.day,
				this.timeScaleFormats.time6h 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 40 * 3600; }, 
			formats: [
				this.timeScaleFormats.week, 
				this.timeScaleFormats.day,
				this.timeScaleFormats.time3h 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 14 * 3600; }, 
			formats: [
				this.timeScaleFormats.week, 
				this.timeScaleFormats.day,
				this.timeScaleFormats.time1h 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 10 * 3600; }, 
			formats: [
				this.timeScaleFormats.week, 
				this.timeScaleFormats.day,
				this.timeScaleFormats.time30m 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 3.5 * 3600; }, 
			formats: [
				this.timeScaleFormats.week, 
				this.timeScaleFormats.day,
				this.timeScaleFormats.time15m 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 2.5 * 3600; }, 
			formats: [
				this.timeScaleFormats.week, 
				this.timeScaleFormats.day,
				this.timeScaleFormats.time10m 
			]
		},
		{	
			lengthCondition: function(l) { return l <= 15 * 60; }, 
			formats: [
				this.timeScaleFormats.week, 
				this.timeScaleFormats.day,
				this.timeScaleFormats.time1m 
			]
		}
	];
	
	this.startTime = -1;
	this.endTime = 0;
	this.intervals = [];
	this.numIntervalLines = 0;
	
	this.visual = {
		backgroundColor: "#f6f6f6",
		lineHeight: 58,
		verticalSpacing: 10,
		invPadding: 4,
		scaleLabelsColor: '#292929',
		scaleLabelFont: '13px Arial',
		scaleBgColor: '#f6f6f6',
		scaleStripBgColor: '#fff',
		scaleStripAltColor: '#fff',
		scaleLines: [
			{thickness: 1.6, color: "#b9b9b9"},
			{thickness: 1, color: "#d1d1d1"},
			{thickness: 1, color: "#ececec"}
		],
		scaleLineHeight: 20,
		scaleLabelHeight: 14,
		invBgColor: '#50d030',
		invBorderColor: '#585858',
		invBorderWidth: 1,
		invLabelColor: ['#1E1E1E', '#fff'],
		tooltipBgColor: 'rgba(255, 255, 255, 0.9)',
		tooltipFont: '12px Arial',
		tooltipTextColor: '#444',
		tooltipHeadersFont: 'bold 12px Arial',
		labelsFont: '13px Arial',
		labelHeight: 15,
		secondaryLabelHeight: 15,
		secondaryLabelMargin: 4,
		secondaryLabelColor: ['#505050', '#ccc'],
		
		groupNameColumnWidth: 160,
		groupNameColumnBgColor: '#f6f6f6',
		groupNameColumnBorderLine: {thickness: 1, color: '#d6d6d6'},
		groupNameColumnCaptionColor: '#292929',
		dateFormat: "dd/mm/yyyy HH:MM:ss TT",
		minInvLabelWidth: 5,
		scrollBarTextColor: '#444',
		scrollBarFont: '11px Arial',
		scrollBarTextHeight: 10,
		grayedPeriodBgColor: '#f2f2f2',
		grayedPeriodBorderColor: '#ccc',
		grayedPeriodPadding: 4,
		setupInvBgColor: '#aaa',
		setupInvHeight: 12,
		teardownInvBgColor: '#bbb',
		teardownInvHeight: 12,
		selectedIntervalBorderColor: '#f00',
		selectedIntervalBorderWidth: 2,
		linkLineThickness: 1.5,
		linkLineColor: "#f00",
		invHoverAreaSize: 16,
		invHoverAreaFont: '14px Arial',
		invHoverAreaColor: '#000',
		invHoverAreaHoverColor: '#fff',
		tooltipMouseMargin: 10,
		tooltipBulletChars: ['☆', '★'],
		tooltipBulletColors: ['#000', '#FEBA02'],
		tooltipBulletFont: '14px Arial',
		tooltipBulletSize: 10,
		tooltipBulletMargin: 8,
        labelBgBrightnessThresh: 144
	};
	this.groups = [];
	this.labelsSourceFields = ["OperationName"];
	this.colorSourceField = "OperationName";
	this.highlightedTooltipFields = ["OperationName"];
	this.intervalRects = [];
	this.tooltipStatus = {
		invId: {gr: -1, inv: -1}, 
		timeoutId: 0, 
		x: 0, y: 0, w: 0, h: 0,
		timeoutHandle: 0,
		visible: false,
	};
	this.timeScaleRects = [];
	this.selectedInterval = null;
	this.links = [];
	this.avgDrawTime = 0.0;
	this.zoomPolicy.allowVertical = false;
	this.hoveredInterval = {gr: -1, inv: -1};
	this.linksDisplayMode = 'A';
}

Timeline.prototype = Object.create(ScrollableViewport.prototype);
Timeline.prototype.superclassMouseMove = Timeline.prototype.mouseMove;

Timeline.prototype.click = function (x, y) {
	var st = this.tooltipStatus;
	
	var trect = { 
		x: st.x - this.visual.tooltipMouseMargin,
		y: st.y - this.visual.tooltipMouseMargin,
		w: st.w + 2 * this.visual.tooltipMouseMargin,
		h: st.h + 2 * this.visual.tooltipMouseMargin
	};
	if (st.visible && x >= trect.x && y > trect.y && 
		x < trect.x + trect.w && y < trect.y + trect.h)
	{
		for (var br in st.bulletRects)
		{
			var brect = st.bulletRects[br];
			if (x >= brect.x && y > brect.y && 
				x < brect.x + brect.w && y < brect.y + brect.h)
			{
				var idx = this.labelsSourceFields.indexOf(brect.field);
				if (idx >= 0)
				{
					this.labelsSourceFields.splice(idx, 1);
				}
				else
				{
					this.labelsSourceFields.push(brect.field);
				}
				this.requestViewportUpdate();
				return;
			}
		}
		return;
	}
	
	for (var sr in this.timeScaleRects)
	{
		var r = this.timeScaleRects[sr];
		if (x >= r.x && x < r.x + r.w &&
			y >= r.y && y < r.y + r.h)
		{
			this.lowPassAlpha = this.zoomLowPassAlpha;
			this.left = Math.max(0, 
				(r.st - this.startTime) / (this.endTime - this.startTime));
			this.right = Math.min(1, 
				(r.et - this.startTime) / (this.endTime - this.startTime));
			return;
		}
	}
	for (var i in this.intervalRects)
	{
		var r = this.intervalRects[i];
		var gr = r[0].gr,
			inv = r[0].inv;
		if (this.groups[gr].intervals[inv].grayed)
		{
			continue;
		}
		for (rn in r)
		{
			if (x >= r[rn].x && x < r[rn].x + r[rn].w &&
				y >= r[rn].y && y < r[rn].y + r[rn].h)
			{
				this.selectedInterval = this.groups[gr].intervals[inv];
				return;
			}
		}
	}
	this.selectedInterval = null;
};

Timeline.prototype.mouseMove = function (event) {
	this.superclassMouseMove(event);
	
	var c = getRelativeCoordinates(event, this.canvas);
	var st = this.tooltipStatus;
	
	var trect = { 
		x: st.x - this.visual.tooltipMouseMargin,
		y: st.y - this.visual.tooltipMouseMargin,
		w: st.w + 2 * this.visual.tooltipMouseMargin,
		h: st.h + 2 * this.visual.tooltipMouseMargin
	};
	if (st.visible && c.x >= trect.x && c.y > trect.y && 
		  c.x < trect.x + trect.w && c.y < trect.y + trect.h)
	{
		return;
	}
	
	var tooltipTimeout = 500;
	var self = this;
	var timeoutHandler = function() {
		self.showTooltip();
	};
	
	var lastHoveredInterval = { gr: this.hoveredInterval.gr, 
		inv: this.hoveredInterval.inv };
	this.hoveredInterval = { gr: -1, inv: -1 };
	
	for (var i = 0; i < this.intervalRects.length; i++)
	{
		var r = this.intervalRects[i];
		var gr = r[0].gr,
			inv = r[0].inv;
		
		if (!this.groups[gr])
		{
			continue;
		}
		
		if (this.groups[gr].intervals[inv].grayed)
		{
			continue;
		}
		if (c.x >= r[0].x && c.x < r[0].x + r[0].w &&
			c.y >= r[0].y && c.y < r[0].y + r[0].h)
		{
			this.hoveredInterval = { gr: gr, inv: inv };
		}
		
		for (rn in r)
		{
			var rr = r[rn];
			if (!rr.hoverArea)
			{
				continue;
			}
			var ha = rr.hoverArea;
			if (c.x >= ha.x && c.x < ha.x + ha.w &&
				c.y >= ha.y && c.y < ha.y + ha.h)
			{
				if (gr != st.invId.gr ||
					inv != st.invId.inv)
				{
					this.hideTooltip();
					clearTimeout(st.timeoutHandle);
					st.invId.gr = gr;
					st.invId.inv = inv;
					st.x = c.x;
					st.y = c.y;
					st.timeoutHandle = 
						setTimeout(timeoutHandler, tooltipTimeout);
				}
				
				return;
			}
		}
	}
	
	
	if (-1 != st.invId.gr || -1 != st.invId.inv)
	{
		clearTimeout(st.timeoutHandle);
		st.timeoutHandle = 
					setTimeout(function () { self.hideTooltip(); }, tooltipTimeout);
	}
	if (lastHoveredInterval.inv != this.hoveredInterval.inv ||
		lastHoveredInterval.gr != this.hoveredInterval.gr)
	{
		this.requestViewportUpdate();
	}
};

Timeline.prototype.showTooltip = function() {
	this.tooltipStatus.visible = true;
	this.requestViewportUpdate();
};

Timeline.prototype.hideTooltip = function() {
	this.tooltipStatus.invId.gr = -1;
	this.tooltipStatus.invId.inv = -1;
	this.tooltipStatus.visible = false;
	this.requestViewportUpdate();
};

Timeline.prototype.addIntervals = function (intervals) {
	var intersectsWithAnyOf = function(line, inv, invs) {
		for (var i = 0; i < invs.length; i++)
		{
			if (invs[i].line != line)
			{
				continue;
			}
			var si = invs[i];
			var a = si.setupTime ? si.setupTime : si.startTime, 
				b = si.endTime,
				c = inv.setupTime ? inv.setupTime : inv.startTime, 
				d = inv.endTime;
			if (c >= a && c < b || d >= a && d < b ||
				a >= c && a <= d || b > c && b < d)
			{
				return true;
			}
		}
		return false;
	}
	
	for (var i = 0; i < intervals.length; i++)
	{
		var lineNum = 0;
		var inv = intervals[i];
		
		var groupId = -1;
		for (var g = 0; g < this.groups.length; g++)
		{
			if (this.groups[g].name == inv.Resource)
			{
				groupId = g;
				break;
			}
		}
		if (groupId == -1)
		{
			groupId = this.groups.length;
			this.groups.push({ name: inv.Resource, numLines: 1, numInvs: 0, numGrayed: 0, intervals: []});
		}
		
		
		inv.startTime = inv.StartTime.getTime() / 1000;
		inv.endTime = inv.EndTime.getTime() / 1000;
		
		var st = inv.startTime, nd = inv.endTime;
		
		if (inv.SetupStartTime && (inv.SetupStartTime instanceof Date) && 
			inv.SetupStartTime.getTime() != inv.StartTime.getTime())
		{
			st = inv.setupTime = inv.SetupStartTime.getTime() / 1000;
		}
		if (inv.TearDownStartTime && (inv.TearDownStartTime instanceof Date) && 
			inv.TearDownStartTime.getTime() != inv.EndTime.getTime())
		{
			nd = inv.teardownTime = inv.TearDownStartTime.getTime() / 1000;
		}
		
		if (inv.grayed)
		{
			var ng = this.groups[groupId].numGrayed + 1;
			this.groups[groupId].intervals[-ng] = inv;
			this.groups[groupId].numGrayed++;
		}
		else
		{
			while (intersectsWithAnyOf(lineNum, inv, 
						this.groups[groupId].intervals))
			{
				lineNum++;
			}
			inv.line = lineNum;
			this.groups[groupId].numLines = Math.max(lineNum + 1, 
					this.groups[groupId].numLines);
			this.groups[groupId].intervals[this.groups[groupId].numInvs] = inv;
			this.groups[groupId].numInvs++;
			
			this.startTime = (this.startTime == -1) ?  st : 
							Math.min(this.startTime, nd);
			this.endTime = (this.endTime == 0) ? nd : 
							Math.max(this.endTime, nd);
		}
	}
};

Timeline.prototype.addLinks = function (links) {
	var self = this;
	var invsByReference = [];
	for (var g in this.groups)
	{
		for (var i in this.groups[g].intervals)
		{
			if (i < 0) 
			{
				continue;
			}
			var ref = this.groups[g].intervals[i].Reference;
			invsByReference[ref] = {gr: g, inv: i};
		}
	}
	
	for (var l in links)
	{
		var newLink = {
			src: invsByReference[links[l].From],
			dst: invsByReference[links[l].To]
		};
		if (newLink.src && newLink.dst)
		{
			this.links.push(newLink);
		}
	}
}

Timeline.prototype.clearIntervals = function () {
	this.startTime = -1;
	this.endTime = 0;
	this.groups = [];
};

Timeline.prototype.calcIntervalColor = function (interval) {
	var hash = hashFnv32a(interval[this.colorSourceField].toString());

	var col = {
		r: 110 + ((hash & 0xFF) % 140),
		g: 110 + (((hash >> 8) & 0xFF) % 140),
		b: 110 + (((hash >> 16) & 0xFF) % 140)
	};
	return {
        fillStyle: "rgba(" + col.r + "," + col.g + "," + col.b + ", 1.0)", 
        intensity: (col.r + col.g + col.b) / 3.0
    };
}

Timeline.prototype.drawTimeScale = function(opts) {
	var i = 0;	
	var viewStartTime = opts.viewStartTime;
	var viewEndTime = opts.viewEndTime;
	var vpSizeInSeconds = viewEndTime - viewStartTime;
	var confIdx = 0;
	for (i = this.timeScaleConfs.length - 1; i >= 0; i--)
	{
		if (this.timeScaleConfs[i].lengthCondition(vpSizeInSeconds))
		{
			confIdx = i;
			break;
		}
	}
	var conf = this.timeScaleConfs[confIdx];
	
		
	opts.ctx.font = opts.font ? opts.font : this.visual.labelsFont;
	var textHeight = opts.textHeight ? opts.textHeight : this.visual.scaleLabelHeight;
	
	opts.ctx.save();
	opts.ctx.beginPath();
	opts.ctx.rect(opts.x, opts.y, opts.w, opts.h);
	opts.ctx.clip();
	
	var numScaleLines = opts.numScaleLines;
	var timeScaleHeight = this.visual.scaleLineHeight * numScaleLines;
	
	opts.ctx.fillStyle = this.visual.scaleBgColor;
	this.drawRect(opts.ctx, opts.x, 0, opts.w, timeScaleHeight);
	
	if (opts.startLine == 0)
	{
		this.timeScaleRects = [];
	}
	
	var guidelines = [];
	
	var line = 0;
	for (line = opts.startLine; 
		 line < opts.startLine + opts.numScaleLines; 
		 line++)
	{
		var uc = conf.formats[line];
		if (!uc) 
		{
			continue;
		}
		var id = uc.idFunc(viewStartTime);
		var endId = uc.idFunc(viewEndTime) + 2;
		
		var y = (line - opts.startLine) * this.visual.scaleLineHeight + opts.y;
		while (id < endId)
		{
			var date = uc.timeFunc(id);
			var invStartTime = date.getTime() / 1000;
			var invEndTime = uc.timeFunc(id + 1).getTime() / 1000;
			
			var invStartX = opts.x + opts.w * (invStartTime - viewStartTime) / vpSizeInSeconds;
			var invEndX = opts.x + opts.w * (invEndTime - viewStartTime) / vpSizeInSeconds;
			invStartX = Math.max(opts.x, invStartX);
			invEndX = Math.min(opts.x + opts.w, invEndX);
			
			opts.ctx.fillStyle = ((id % 2) == 0) ? this.visual.scaleStripAltColor : 
							this.visual.scaleStripBgColor;
			
			if (line == opts.numScaleLines - 1)
			{
				this.drawRect(opts.ctx, invStartX, timeScaleHeight, 
							  invEndX - invStartX, opts.h - timeScaleHeight);
			}
			if (opts.startLine == 0)
			{
				this.timeScaleRects.push({x: invStartX, y: y, 
					w: invEndX - invStartX, h: this.visual.scaleLineHeight, 
					st: invStartTime, et: invEndTime});
			}
			
			if (!(invStartX in guidelines) && invStartX != opts.x)
			{
				guidelines[invStartX] = line;
			}
			
			if (invStartX != opts.x)
			{
				opts.ctx.fillStyle = opts.linesColor ? opts.linesColor : 
									this.visual.groupNameColumnBorderLine.color;
				this.drawRect(opts.ctx, invStartX, y, 
								this.visual.groupNameColumnBorderLine.thickness, 
								this.visual.scaleLineHeight);
			}
			
			var labelWidth = 0;
			var label = "";
			for (var df = 0; df < uc.dateFormats.length; df++)
			{
				label = date.format(uc.dateFormats[df], true);
				labelWidth = opts.ctx.measureText(label).width;
				if (labelWidth < invEndX - invStartX)
				{
					break;
				}
			}
			
			if (labelWidth < invEndX - invStartX)
			{
				opts.ctx.fillStyle = opts.labelsColor ? opts.labelsColor : 
									this.visual.scaleLabelsColor;
				opts.ctx.font = this.visual.scaleLabelFont;
				opts.ctx.fillText(label, invStartX + 
					0.5 * (invEndX - invStartX - labelWidth), y + textHeight);
			}
			
			id++;
		}
		opts.ctx.fillStyle = this.visual.groupNameColumnBorderLine.color;
		this.drawRect(opts.ctx, opts.x, y + this.visual.scaleLineHeight, 
					  opts.w, this.visual.groupNameColumnBorderLine.thickness);
	}
	if (opts.guidelines !== false)
	{
		for (var glx in guidelines)
		{
			var gl = guidelines[glx];
			opts.ctx.fillStyle = this.visual.scaleLines[gl].color;
			this.drawRect(opts.ctx, glx, timeScaleHeight, 
							this.visual.scaleLines[gl].thickness, 
							opts.h - timeScaleHeight);
		}
	}
	opts.ctx.restore();
}

Timeline.prototype.drawTooltip = function (ctx) {
	var st = this.tooltipStatus;
	if (!st.visible || !this.groups[st.invId.gr] || 
		!this.groups[st.invId.gr].intervals[st.invId.inv] ||
		this.groups[st.invId.gr].intervals[st.invId.inv].grayed)
	{
		return;
	}
	var tw = 0;
	var th = 0;
	var padding = 8;
	var inv = this.groups[st.invId.gr].intervals[st.invId.inv];
	//console.log(inv);
	
	var headerLabels = [];
	var labels = [];
	var columnWidths = [0, 0];
	for (var k in inv) 
	{
		if (k == "line" || k == "startTime" || 
			k == "endTime" || k == "setupTime" ||
			k == "teardownTime")
		{
			continue;
		}
		var v = inv[k];
		if (v instanceof Date)
		{
			v = v.format(this.visual.dateFormat, true);
		}
		else if ((typeof v) != "string" && isNaN(v))
		{
			v = "";
		}
		ctx.font = this.visual.tooltipFont;
		var w = ctx.measureText(k).width;
		headerLabels.push({t: k, w: w});
		ctx.font = this.visual.tooltipHeadersFont;
		var vw = ctx.measureText(v).width;
		labels.push({t: v, w: vw});
		
		th += this.visual.scaleLabelHeight;
		tw = Math.max(w + padding + vw, tw);
		columnWidths[0] = Math.max(w, columnWidths[0]);
		columnWidths[1] = Math.max(vw, columnWidths[1]);
	}
	tw += 6 * padding + 2 * this.visual.tooltipBulletMargin + this.visual.tooltipBulletSize;
	th += 2 * padding;
	
	st.x = Math.min(this.canvas.offsetWidth - tw, st.x);
	st.y = Math.min(this.canvas.offsetHeight - th, st.y);
	st.w = tw;
	st.h = th;
	
	ctx.fillStyle = this.visual.tooltipBgColor;
	this.drawRect(ctx, st.x, st.y, tw, th);
	
	var textY = 0;
	st.bulletRects = [];
	
	for (var l in labels)
	{
		textY += this.visual.scaleLabelHeight;
		
		ctx.fillStyle = this.visual.tooltipTextColor;
		ctx.font = this.visual.tooltipHeadersFont;
		ctx.fillText(headerLabels[l].t, st.x + 
					padding + columnWidths[0] - headerLabels[l].w, 
					st.y + textY);
		
		var idx = (this.labelsSourceFields.indexOf(headerLabels[l].t) >= 0) ? 1 : 0;
		
		var bulletCoords = {x: st.x + padding + columnWidths[0] + 
							this.visual.tooltipBulletMargin,  
							y: st.y + textY - 0.5 * this.visual.scaleLabelHeight};
		ctx.font = this.visual.tooltipBulletFont;
		ctx.fillStyle = this.visual.tooltipBulletColors[idx];
		ctx.fillText(this.visual.tooltipBulletChars[idx], bulletCoords.x,
					bulletCoords.y + 0.5 * this.visual.scaleLabelHeight);
		
		var bulletRect = {
			x: bulletCoords.x - 0.5 * this.visual.tooltipBulletMargin, 
			y: bulletCoords.y - 0.5 * this.visual.tooltipBulletMargin, 
			w: this.visual.tooltipBulletSize + this.visual.tooltipBulletMargin, 
			h: this.visual.tooltipBulletSize + this.visual.tooltipBulletMargin, 
			field: headerLabels[l].t
		};
		st.bulletRects.push(bulletRect);
		
		ctx.fillStyle = this.visual.tooltipTextColor;
		ctx.font = this.visual.tooltipFont;
		ctx.fillText(labels[l].t, st.x + 
					padding + 2 * this.visual.tooltipBulletMargin + 
					this.visual.tooltipBulletSize + columnWidths[0], st.y + textY);
		
	}
};

Timeline.prototype.adjustScaleToDPI = function(ctx, w, h) {
	devicePixelRatio = window.devicePixelRatio || 1,
	backingStoreRatio = ctx.webkitBackingStorePixelRatio ||
						ctx.mozBackingStorePixelRatio ||
						ctx.msBackingStorePixelRatio ||
						ctx.oBackingStorePixelRatio ||
						ctx.backingStorePixelRatio || 1,

	ratio = devicePixelRatio / backingStoreRatio;

    if (devicePixelRatio !== backingStoreRatio) 
	{
		this.canvas.style.width = w + 'px';
		this.canvas.style.height = h + 'px';
		this.canvas.width = w * ratio;
		this.canvas.height = h * ratio;
        ctx.setTransform(1, 0, 0, 1, 0, 0);
		ctx.scale(ratio, ratio);
    }
};

Timeline.prototype.drawLinks = function(ctx) {
	if (!this.selectedInterval)
	{
		return;
	}
	var linksToDraw = [];
	var sic = null;
	for (var g in this.groups)
	{
		for (var i in this.groups[g].intervals)
		{
			if (this.groups[g].intervals[i] == this.selectedInterval)
			{
				sic = {gr: g, inv: i};
				break;
			}
		}
		if (sic)
		{
			break;
		}
	}
	var self = this;
	
	var findLinks = function (link, coords, parents, childs, inheritSearch) {
		for (var l in self.links)
		{
			var ll = self.links[l];
			if (ll == link || linksToDraw.indexOf(ll) >= 0)
			{
				continue;
			}
			if (childs && ll.src.gr == coords.gr && 
				ll.src.inv == coords.inv)
			{
				linksToDraw.push(ll);
				
				var nextParents = parents, nextChilds = childs;
				if (!inheritSearch)
				{
					nextParents = false;
					nextChilds = true;
				}
				
				findLinks(ll, ll.dst, nextParents, nextChilds, inheritSearch);
			}
			if (parents && ll.dst.gr == coords.gr && 
				ll.dst.inv == coords.inv)
			{
				linksToDraw.push(ll);
				
				var nextParents = parents, nextChilds = childs;
				if (!inheritSearch)
				{
					nextParents = true;
					nextChilds = false;
				}
				
				findLinks(ll, ll.src, nextParents, nextChilds, inheritSearch);
			}
		}
	};
	
	var ldm = this.linksDisplayMode;
	var addChilds = (ldm != 'P');
	var addParents = (ldm != 'N');
	var inherit = (ldm == 'T');
	
	findLinks(null, sic, addParents, addChilds, inherit);
	
	var minExtend = 80;
	for (var l in linksToDraw)
	{
		var link = linksToDraw[l];		
		
		var srcRect = this.calcIntervalRect(link.src.gr, link.src.inv);
		var dstRect = this.calcIntervalRect(link.dst.gr, link.dst.inv);
		
		if (!srcRect|| !dstRect)
		{
			continue;
		}

		var srcPnt = {x: srcRect.x + srcRect.w, 
					  y: srcRect.y + srcRect.h / 2};
		var dstPnt = {x: dstRect.x, 
					  y: dstRect.y + dstRect.h / 2};
		
		var dist = Math.max(minExtend, Math.abs(dstPnt.x - srcPnt.x));
		ctx.lineWidth = this.visual.linkLineThickness;
		ctx.strokeStyle = this.visual.linkLineColor;
		ctx.beginPath();
		ctx.moveTo(srcPnt.x, srcPnt.y);
		ctx.bezierCurveTo(srcPnt.x + 0.25 * dist, srcPnt.y, 
						  dstPnt.x - 0.25 * dist, dstPnt.y,
						  dstPnt.x, dstPnt.y);
		ctx.stroke();
	}
}

Timeline.prototype.calcIntervalRect = function (grId, invId) { 
	var inv = this.groups[grId].intervals[invId];
	var x = this.visual.groupNameColumnWidth;
	var w = this.container.offsetWidth - this.scrollBarThickness - x;
	var h = this.container.offsetHeight - this.scrollBarThickness - x;
	var numScaleLines = 3;
	var viewStartTime = this.startTime + this.filtered.left * 
						(this.endTime - this.startTime);
	var viewEndTime = this.startTime + this.filtered.right * 
						(this.endTime - this.startTime);
	var vpSizeInSeconds = viewEndTime - viewStartTime;
	
	var mapTime = function (t) {
		return x + w * (t - viewStartTime) / vpSizeInSeconds;
	};
	var timeScaleHeight = this.visual.scaleLineHeight * numScaleLines;
	
	var lineHeight = this.visual.lineHeight;
	if (this.labelsSourceFields.length > 2)
	{
		lineHeight += (this.labelsSourceFields.length - 2) * 
						this.visual.secondaryLabelHeight;
	}
	var totalHeight = 0;
	for (var gg = 0; gg < this.groups.length; gg++)
	{
		var gh = this.groups[gg].numLines * lineHeight;
		totalHeight += gh;
	}
	
	var invStartX = mapTime(inv.startTime);
	var invEndX = mapTime(inv.teardownTime ? inv.teardownTime : inv.endTime);
	
	var grY = timeScaleHeight - this.top * totalHeight;
	for (var gg = 0; gg < grId; gg++)
	{
		grY += this.groups[gg].numLines * lineHeight;
	}
	
	if (this.labelsSourceFields.length > 2)
	{
		lineHeight += (this.labelsSourceFields.length - 2) * 
						this.visual.secondaryLabelHeight;
	}
	
	var y = grY;
	
	if (!inv.grayed)
	{
		y += lineHeight * inv.line + this.visual.verticalSpacing;
	}
	
	invStartX = Math.max(x, invStartX);
	var groupHeight = this.groups[grId].numLines * lineHeight;
	var invHeight = lineHeight - 2 * this.visual.verticalSpacing;
	
	return {x: invStartX, y: y, w: invEndX - invStartX, 
					h: inv.grayed ? (groupHeight - 2 * this.visual.grayedPeriodPadding) : 
					invHeight, gr: grId, inv: invId};
};

Timeline.prototype.draw = function () {
	if (this.container.style.display == 'none' || 
		this.startTime < 0) 
	{
		return;
	}
	drawTime = new Date().getTime();
	var numScaleLines = 3;
	var timeScaleHeight = this.visual.scaleLineHeight * numScaleLines;
	var lineHeight = this.visual.lineHeight;
	if (this.labelsSourceFields.length > 2)
	{
		lineHeight += (this.labelsSourceFields.length - 2) * 
						this.visual.secondaryLabelHeight;
	}
	var invHeight = lineHeight - 2 * this.visual.verticalSpacing;
	
	var w = this.container.offsetWidth;
	var h = this.container.offsetHeight;	
	if (this.canvas.offsetWidth != w || this.canvas.offsetHeight != h)
	{
		this.canvas.width = w;
		this.canvas.height = h;
	}
	var ctx = this.canvas.getContext('2d');
	this.adjustScaleToDPI(ctx, w, h);
	ctx.clearRect(0, 0, w, h);

	w -= this.scrollBarThickness;
	h -= this.scrollBarThickness;
	var x = this.visual.groupNameColumnWidth;
	
	var viewStartTime = this.startTime + this.filtered.left * 
						(this.endTime - this.startTime);
	var viewEndTime = this.startTime + this.filtered.right * 
						(this.endTime - this.startTime);
	var vpSizeInSeconds = viewEndTime - viewStartTime;
	
	this.drawTimeScale({ctx: ctx, x: x, y: 0,
		w : w - this.visual.groupNameColumnWidth, h : h,
		viewStartTime: viewStartTime, viewEndTime: viewEndTime,
		numScaleLines: numScaleLines, startLine: 0, guidelines: true});
	
	// draw group names
	
	// fill group names column area with background
	ctx.fillStyle = this.visual.groupNameColumnBgColor;
	this.drawRect(ctx, 0, 0, this.visual.groupNameColumnWidth, h);
	
	// draw group names column borders
	ctx.fillStyle = this.visual.groupNameColumnBorderLine.color;
	this.drawRect(ctx, this.visual.groupNameColumnWidth, 0, 
				  this.visual.groupNameColumnBorderLine.thickness, h);
	this.drawRect(ctx, 0, 0, 
				  w, this.visual.groupNameColumnBorderLine.thickness);
	this.drawRect(ctx, 0, timeScaleHeight, 
				  w, this.visual.groupNameColumnBorderLine.thickness);
	
	ctx.font = this.visual.labelsFont;
	ctx.fillStyle = this.visual.groupNameColumnCaptionColor;
	ctx.fillText("Resources", this.visual.invPadding, 
				 	0.5 * (timeScaleHeight + this.visual.labelHeight));
	
	// clip the canvas to prevent drawing on time scale
	ctx.save();
	ctx.beginPath();
	ctx.rect(0, timeScaleHeight, w, h - timeScaleHeight);
	ctx.clip();
	
	var groupLine = 0;
	var totalHeight = 0;
	for (var grId = 0; grId < this.groups.length; grId++)
	{
		var gh = this.groups[grId].numLines * lineHeight;
		totalHeight += gh;
	}
	
	for (var grId = 0; grId < this.groups.length; grId++)
	{
		var y = timeScaleHeight + lineHeight * groupLine - this.top * totalHeight;
		var gh = this.groups[grId].numLines * lineHeight;
		if (grId != 0)
		{
			ctx.fillStyle = this.visual.scaleLines[2].color;
			this.drawRect(ctx, 0, y, w, this.visual.scaleLines[2].thickness);
		}
		ctx.fillStyle = this.visual.groupNameColumnCaptionColor;
		ctx.fillText(this.groups[grId].name, this.visual.invPadding, 
					 y + this.visual.labelHeight);
		groupLine += this.groups[grId].numLines;
	}
	ctx.restore();
	
	w -= this.visual.groupNameColumnWidth;
	
	ctx.save();
	ctx.beginPath();
	ctx.rect(x, timeScaleHeight, w, h - timeScaleHeight);
	ctx.clip();
	
	
	this.intervalRects = [];
	
	var mapTime = function (t) {
		return x + w * (t - viewStartTime) / vpSizeInSeconds;
	};
	
	var grY = timeScaleHeight - this.top * totalHeight;
	for (var grId = 0; grId < this.groups.length; grId++)
	{
		var groupHeight = this.groups[grId].numLines * lineHeight;
		for (var invId = Math.min(0, -this.groups[grId].numGrayed); 
			 invId < this.groups[grId].intervals.length; 
			 invId++)
		{
			var inv = this.groups[grId].intervals[invId];
            var invBoundsStart = inv.setupTime ? inv.setupTime : inv.startTime;
			if (inv.endTime < viewStartTime || invBoundsStart > viewEndTime)
			{
				continue;
			}
			if (inv.endTime < inv.startTime)
			{
				continue;
			}
			
			var invStartX = mapTime(inv.startTime);
			var invEndX = mapTime(inv.teardownTime ? inv.teardownTime : inv.endTime);
			var y = grY;
			if (!inv.grayed)
			{
				y += lineHeight * inv.line + this.visual.verticalSpacing;
				if (y + invHeight < timeScaleHeight)
				{
					continue;
				}
			}
			else
			{
				y += this.visual.grayedPeriodPadding;
			}
			
			invStartX = Math.max(x, invStartX);
			
			var invRect = {x: invStartX, y: y, w: invEndX - invStartX, 
							h: inv.grayed ? (groupHeight - 2 * this.visual.grayedPeriodPadding) : 
							invHeight, gr: grId, inv: invId}; 
			if (invRect.w > this.visual.minInvLabelWidth && !inv.grayed)
			{
				var hoverWidth = Math.min(this.visual.invHoverAreaSize, invRect.w);
				invRect.hoverArea = {
					x: Math.max(invRect.x, Math.min(invRect.x + invRect.w - 
							1.0 * this.visual.invHoverAreaSize, x + w - hoverWidth)),
					y: invRect.y,
					w: hoverWidth,
					h: this.visual.invHoverAreaSize
				};
			}
			//var invRect = this.calcIntervalRect(grId, invId);
			
			var currentRects = [invRect];
			
			var borderColor = (this.selectedInterval == inv) ? 
						this.visual.selectedIntervalBorderColor : this.visual.invBorderColor;
			var borderWidth = (this.selectedInterval == inv) ?  
						this.visual.selectedIntervalBorderWidth : this.visual.invBorderWidth;
			var xBorder = 0;
			if (inv.grayed || invRect.w >= this.visual.minInvLabelWidth)
			{
				ctx.fillStyle = inv.grayed ? this.visual.grayedPeriodBorderColor : 
								borderColor;
				this.drawRect(ctx, invRect.x, invRect.y, 
							  invRect.w + borderWidth, 
							  invRect.h);
				xBorder = borderWidth;
			}
			var invColor = !inv.grayed ? this.calcIntervalColor(inv) : null;
			ctx.fillStyle = inv.grayed ? this.visual.grayedPeriodBgColor : 
							invColor.fillStyle;
			this.drawRect(ctx, invRect.x + xBorder, invRect.y + xBorder,
					 invRect.w - 2 * xBorder + borderWidth, 
					 invRect.h - 2 * xBorder);
			
			
			
			xBorder = borderWidth;
			//setup
			if (inv.setupTime)
			{
				var setupStartX = mapTime(inv.setupTime);
				
				var rr = { x: setupStartX, 
						   y: invRect.y + invRect.h - this.visual.setupInvHeight,
						   w: invStartX - setupStartX, 
				           h: this.visual.setupInvHeight};
				
				if (rr.w > 0.5)
				{
					ctx.fillStyle = borderColor;
					this.drawRect(ctx, rr.x, rr.y, rr.w, rr.h);
					ctx.fillStyle = this.visual.setupInvBgColor;
					this.drawRect(ctx, rr.x + xBorder, rr.y + xBorder, 
								rr.w - xBorder, rr.h - 2 * xBorder);
				}
			}
			//teardown
			if (inv.teardownTime)
			{
				var endX = mapTime(inv.endTime);
				
				var rr = { x: invEndX, 
						   y: invRect.y,
						   w: endX - invEndX, 
				           h: this.visual.teardownInvHeight};
				if (rr.w > 0.5)
				{
					ctx.fillStyle = borderColor;
					this.drawRect(ctx, rr.x, rr.y, rr.w, rr.h);
					ctx.fillStyle = this.visual.teardownInvBgColor;
					this.drawRect(ctx, rr.x + xBorder, rr.y + xBorder, 
								rr.w - 2 * xBorder, rr.h - 2 * xBorder);
				}
			}
			
			if (!inv.grayed)
			{
				if (invRect.w > this.visual.minInvLabelWidth)
				{
					ctx.save();
					ctx.beginPath();
					ctx.rect(invRect.x + this.visual.invPadding, invRect.y, 
							invRect.w - 2 * this.visual.invPadding, invRect.h);
					ctx.clip();
					var iidx = (invColor.intensity < 
                                this.visual.labelBgBrightnessThresh) ? 1 : 0;
                    
					for (var ll in this.labelsSourceFields)
					{
						var label = inv[this.labelsSourceFields[ll]];
						if (label instanceof Date)
						{
							label = label.format(this.visual.dateFormat);
						}
						ctx.font = this.visual.labelsFont;
						ctx.fillStyle = (ll == 0) ? this.visual.invLabelColor[iidx] : 
										this.visual.secondaryLabelColor[iidx];
						ctx.fillText(label, invStartX + this.visual.invPadding, 
									y + this.visual.labelHeight + ll * 
										this.visual.secondaryLabelHeight);
					}
					
					if (invRect.hoverArea && this.hoveredInterval.gr == grId && 
						this.hoveredInterval.inv == invId)
					{
						var hl = (this.tooltipStatus.invId.gr == grId) && 
									(this.tooltipStatus.invId.inv == invId);
						ctx.fillStyle = hl ? this.visual.invHoverAreaHoverColor : 
										this.visual.invHoverAreaColor;
						ctx.font = this.visual.invHoverAreaFont;
						ctx.fillText("◉", invRect.hoverArea.x, 
									invRect.hoverArea.y + 0.75 * invRect.hoverArea.h);
					}
					
					ctx.restore();
					ctx.beginPath();
				}
				
			}
			this.intervalRects.push(currentRects);
		}
		grY += groupHeight;
	}
	this.drawLinks(ctx);
	
	var lineThumbSize = Math.min(1.0, (h - timeScaleHeight) / totalHeight);
	this.bottom = this.top + lineThumbSize;
	ctx.restore();
	this.drawScrollBar(ctx, true, this.top, this.bottom, 
					   timeScaleHeight, h - timeScaleHeight);
	this.drawScrollBar(ctx, false, this.filtered.left, this.filtered.right, 
					   this.visual.groupNameColumnWidth, w);
	this.drawTimeScale({ctx: ctx, x: this.visual.groupNameColumnWidth, 
		y: h,
		w : w, h : this.scrollBarThickness,
		viewStartTime: this.startTime, viewEndTime: this.endTime,
		numScaleLines: 1, startLine: 1, guidelines: false, 
		linesColor: this.visual.scrollBarTextColor,
		labelsColor: this.visual.scrollBarTextColor, 
		font: this.visual.scrollBarFont,
		textHeight: this.visual.scrollBarTextHeight});

	
	this.drawTooltip(ctx);
	
	this.scrollAreaMargins.left = this.visual.groupNameColumnWidth;
	this.scrollAreaMargins.right = this.scrollBarThickness;
	this.scrollAreaMargins.top = timeScaleHeight;
	this.scrollAreaMargins.bottom = this.scrollBarThickness;
	
	drawTime = new Date().getTime() - drawTime;
	
	this.avgDrawTime = (this.avgDrawTime == 0.0) ? drawTime : 
					(drawTime * 0.05 + 0.95 * this.avgDrawTime);
	
	ctx.fillStyle = '#aaa';
	ctx.fillText(Math.round(this.avgDrawTime), 8, h - 8);
};


Ext.define('TimelineField', {
	extend: 'Ext.data.Model',
	fields: [ 'name' ]
});

Ext.define('TimelineComponent', {
	extend: 'Ext.Panel',
	
	timelineControl: null,
	loadedParts: {calendarPeriods: false, operations: false},
	colorFieldCombo: null,
	fieldsStore: null,
	
	config: {
		title: 'Timeline',
		xtype: 'panel',
		layout: {
			type: 'border'
		},
        style: {
          margin: '0px -1px -1px -1px'
        },
		tbar: [],
		items: [
			{
				region: 'center',
				xtype: 'panel',
				html: '<div id=tlContainer style="background:#fff;' + 
						'width:100%;height:100%;display:block"><canvas ' +
						'id="tlControl"></canvas></div>',
				style: {
				    margin: '-2px 0px 0px 0px'
				},
			},
		]
	},
	
	handleFileSelect: function (evt) {
		evt.stopPropagation();
		evt.preventDefault();
		
		var self = this;
		var files = evt.dataTransfer.files;

		var output = [];
		for (var i = 0, f; f = files[i]; i++) {
			var reader = new FileReader();

			reader.onload = (function(theFile) {
				return function(e) {
					self.addCSVData(e.target.result, theFile.name);
				};
			})(f);
			reader.readAsText(f, 'UTF-8');
		}
		
	},
	
	addCSVData: function(data, filename) {
		var defaultCapField = "OperationName";
		var parseDate = function(text) {
			// "dd/mm/yyyy hh:mm"
			var p = text.split(' ');
			var d = p[0].split('/');
			var t = p[1].split(':');
			if (p[2] && p[2] == 'PM' && t[0] != 12)
			{
				t[0] = Math.round(t[0]) + 12;
			}
			if (p[2] && p[2] == 'AM' && t[0] == 12)
			{
				t[0] = 0;
			}
			return new Date(Date.UTC(d[2], d[1] - 1, d[0], t[0], t[1], 0, 0) - 
							(new Date).getTimezoneOffset());
		};
		var links = (filename.indexOf("Relationships") != -1);
		var calendarPeriods = (filename.indexOf("CalendarPeriods") != -1);
		
		
		if (this.loadedParts.calendarPeriods && 
			this.loadedParts.operations && !links)
		{
			this.timelineControl.clearIntervals();
			this.loadedParts.calendarPeriods = false;
			this.loadedParts.operations = true;
		}
		
		
		
		if (!links)
		{
			if (!calendarPeriods)
			{
				this.loadedParts.operations = true;
				this.timelineControl.clearIntervals();
			}
			else
			{
				this.loadedParts.calendarPeriods = true;
			}
			
			
			if (this.loadedParts.calendarPeriods && !this.loadedParts.operations)
			{
				this.loadedParts.calendarPeriods = false;
				return;
			}
		}
		
		var csv = new CSV(data, { header: true });
		var intervals = csv.parse();
		if (links)
		{
			this.timelineControl.addLinks(intervals);
		}
		else if (intervals)
		{
			var timeFields = ["StartTime", "EndTime", 
				"SetupStartTime", "TearDownStartTime"];
			for (var i = 0; i < intervals.length; i++)
			{
				for (var tf in timeFields)
				{
					var field = timeFields[tf];
					if (intervals[i][field] && 
						intervals[i][field].length && 
						intervals[i][field].length > 0)
					{
						intervals[i][field] = parseDate(intervals[i][field]);
					}
					else
					{
						delete intervals[i][field];
					}
				}
				
				if (!("OperationName" in intervals[i]))
				{
					intervals[i].grayed = true;
				}
			}
			if (!calendarPeriods)
			{
				var storeData = [];
				var defaultRecord = null;
				for (var k in intervals[0]) 
				{
					if (intervals[0].grayed) 
					{
						continue;
					}
					var record = { name: k };
					
					if (k == defaultCapField)
					{
						defaultRecord = record;
					}
					storeData.push(record);
				}
				this.fieldsStore.setData(storeData);
				this.colorFieldCombo.setValue(defaultCapField);
			}
			this.timelineControl.addIntervals(intervals);
		}
	},
	
	handleDragOver: function (evt) {
		evt.stopPropagation();
		evt.preventDefault();
		evt.dataTransfer.dropEffect = 'copy';
	},
	
	constructor: function() {
		this.callParent(arguments);
		
		var self = this;
		
		this.fieldsStore = Ext.create('Ext.data.Store', {
			model: 'TimelineField',
			data: []
		});
		
		
		this.colorFieldCombo = Ext.create('Ext.form.field.ComboBox', {
			fieldLabel: 'Interval color source',
			displayField: 'name',
			store: this.fieldsStore,
			queryMode: 'local',
			editable: false,
			labelWidth: 130
		});
		
		this.colorFieldCombo.on("select", function (arg, item) { 
			self.timelineControl.colorSourceField = item.data.name;
		});
		
		var dropZonePanel = Ext.create('Ext.Panel', {
			xtype: 'panel',
			flex: 1,
			height: 32,
			html: '<div id="tldropzone" style="display:block; ' + 
					'text-align:center; font-family: Arial, sans; ' +
					'font-size: 14pt; color: #888; width:100%; padding-top: 6px;' +
					'height:100%">Drop CSV files here</div>'
		});
		
		var linksDisplaySwitcher = Ext.create('Ext.button.Segmented', {
			fieldLabel: 'Links display mode',
			labelWidth: 130,
			items: [{
				xtype: 'segmentedbutton',
				items: [{
					xtype: 'button',
					text: 'P',
					value: 'P'
				}, {
					xtype: 'button',
					text: 'N',
					value: 'N'
				}, {
					xtype: 'button',
					text: 'A',
					value: 'A',
					pressed: true
				}, {
					xtype: 'button',
					text: 'T',
					value: 'T'
				}],
				listeners: {
					change: function (btn, newValue, oldValue, eOpts) {
						if (self.timelineControl != null) {
							self.timelineControl.linksDisplayMode = newValue[0];
						}
					}
				}
			}]
		});
		self.down('toolbar').add(this.colorFieldCombo);
		self.down('toolbar').add(linksDisplaySwitcher);
		self.down('toolbar').add(dropZonePanel);
		
		this.on('afterrender', function () {
			if (self.timelineControl != null)
			{
				return;
			}
			self.timelineControl = new Timeline(document.body, 
										document.getElementById('tlContainer'), 
										document.getElementById('tlControl'));
			var dropzone = document.getElementById('tldropzone');
			dropzone.addEventListener('dragover', function (e) {  
				self.handleDragOver(e); }, false);
			dropzone.addEventListener('drop', function (e) {
				self.handleFileSelect(e); }, false);
		});
	}
});


