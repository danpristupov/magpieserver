
Ext.define('DemandOrderModel', {
	extend: 'Ext.data.Model',
	fields: [ 'number', 'demandDate', 'preview' ]
});


Ext.define('DemandOrdersManager', {
	extend: 'Ext.Panel',
	
	orders: new PartsModel.ObjectList(),
	ordersStore: null,
	partsStore: null,
	partsByName: [],
	orderPanel: null,
	rqGrid: null,
	rqStore: null,
	listGrid: null,
	currentOrder: null,
	editing: false,
	editButton: null,
	dateField: null,
	changingRqparts: false,
    
    titleText: 'Demand Orders',
    editButtonDoneText: 'Done',
    editButtonEditText: 'Edit',
    notSetText: '(not set)',
    rqpartRemoveDialogTitleText: 'Confirm deletion',
    rqpartRemoveDialogMsgText: 'Do you really want to delete the selected line?',
    rqGridNameColumnText: 'Name',
    rqGridQuantityColumnText: 'Quantity',
    demandDateText: 'Demand Date',
    
    
	
	config: {
		title: 'Demand Orders',
		xtype: 'panel',
		region: 'center',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		height: '100%',
		style: {
		    margin: '-1px -1px -1px -1px'
		},
		items: []
	},
	
	toggleEditing: function () {
		this.editing = !this.editing;
		this.editButton.setText(this.editing ? this.editButtonDoneText : 
                                this.editButtonEditText);
		this.dateField.setReadOnly(!this.editing);
		if (this.editing)
		{
			this.rqGrid.down("toolbar").show();
		}
		else
		{
			this.rqGrid.down("toolbar").hide();
		}
	},
	
	handleDragOver: function (evt) {
		evt.stopPropagation();
		evt.preventDefault();
		evt.dataTransfer.dropEffect = 'copy';
	},
	
	handleFileSelect: function (evt) {
		evt.stopPropagation();
		evt.preventDefault();
		
		var self = this;
		var files = evt.dataTransfer.files;

		var output = [];
		for (var i = 0, f; f = files[i]; i++) {
			var reader = new FileReader();

			reader.onload = (function(theFile) {
				return function(e) {
					self.addCSVData(e.target.result, theFile.name);
				};
			})(f);
			reader.readAsText(f, 'UTF-8');
		}
		
	},
	
	addCSVData: function (data, fileName) {
		var csv = new CSV(data, { header: true });
		var lines = csv.parse();
		this.orders = new PartsModel.ObjectList();
		
		var self = this;
		var parts = [];
		parts[this.notSetText] = null;
		
		var parseDate = function(text) {
			// "dd/mm/yyyy [hh:mm]"
			var timeIncluded = (text.indexOf(' ') >= 0);
			var p = timeIncluded ? text.split(' ') : [text];
			var d = p[0].split('/');
			var t = [0, 0];
			if (p[1])
			{
				t = p[1].split(':');
				if (p[2] && p[2] == 'PM' && t[0] != 12)
				{
					t[0] = Math.round(t[0]) + 12;
				}
				if (p[2] && p[2] == 'AM' && t[0] == 12)
				{
					t[0] = 0;
				}
			}
			if (!timeIncluded)
			{
				var dd = d[0];
				d[0] = d[2];
				d[2] = dd;
			}
			return new Date(Date.UTC(d[2], d[1] - 1, d[0], t[0], t[1], 0, 0) - 
							(new Date).getTimezoneOffset());
		};
		
		var findOrCreateOrder = function (orderNum) {
			for (var i in self.orders.items)
			{
				if (self.orders.items[i].number == orderNum)
				{
					return self.orders.items[i];
				}
			}
			var order = new PartsModel.DemandOrder(orderNum);
			self.orders.add(order);
			return order;
		};
		var findOrCreatePart = function (name) {
			if (name in parts)
			{
				return parts[name];
			}
			var part = new PartsModel.Part(name);
			parts[name] = part;
			return part;
		};
		for (var l in lines)
		{
			var line = lines[l];
			var order = findOrCreateOrder(line.OrderNumber);
			order.setDemandDate(parseDate(line.DueDate));
			var part = findOrCreatePart(line.PartNumber);
			var rqp = new PartsModel.RequiredPart(part, line.Quantity);
			order.requiredParts.add(rqp);
		}
		var partsData = [];
		
		for (p in parts)
		{
			var part = parts[p];
			partsData.push({name: p, part: part});
		}
		this.partsStore.setData(partsData);
		this.partsByName = parts;
		this.updateGrids();
		this.orderPanel.hide();
	},
	
	getExportableData: function () {
		return this.orders.toPlainObject();
	},
	
	updateGrids: function() {
		var data = [];
		var orderToSelect = -1;
		for (var i in this.orders.items)
		{
			var order = this.orders.items[i];
			var preview = '';
			var maxRqparts = 3;
			var maxPreviewLength = 28;
			var j = 0;
			for (var rq in order.requiredParts.items)
			{
				var rqpart = order.requiredParts.items[rq];
				if (preview.length != 0)
				{
					preview += ', ';
				}
				var partName = rqpart.part ? rqpart.part.name : this.notSetText;
				preview += partName + ' : ' + parseFloat(rqpart.quantity);
				j++;
				if (preview.length > maxPreviewLength)
				{
					preview = preview.substr(0, maxPreviewLength);
					preview += '...';
					break;
				}
				if (j == maxRqparts)
				{
					preview += '...';
					break;
				}
			}
			if (order == this.currentOrder)
			{
				orderToSelect = i;
			}
			
			data.push({key: i, number: order.number, demandDate: order.demandDate, 
				preview: preview, obj: order});
		}
		this.ordersStore.setData(data);
		if (orderToSelect >= 0)
		{
			this.listGrid.selModel.doSelect(
				this.listGrid.store.data.items[orderToSelect]);
		}
		
	},
	
	showOrder: function (order) {
		opData = {number: order.number, demandDate: order.demandDate};
		this.orderPanel.viewModel.setData(opData);
		if (!this.changingRqparts)
		{
			storeData = [];
			for (var ps in order.requiredParts.items)
			{
				var rqp = order.requiredParts.items[ps];
				var name = rqp.part ? rqp.part.name : this.notSetText;
				storeData.push({key: ps, name: name, 
						quantity: rqp.quantity, obj: rqp});
			}
			var selection = null;
			if (this.rqGrid.selModel.getCount() > 0)
			{
				this.rqGrid.selModel.getSelection()[0];
			}
			this.rqStore.setData(storeData);
		}
		this.orderPanel.show();
		if (this.currentOrder != order)
		{
			this.currentOrder = order;
			this.editing = true;
			this.toggleEditing();
		}
		else if (selection != null)
		{
			this.rqGrid.selModel.doSelect(selection);
		}
	},
	
	showCurrentOrder: function () {
		this.showOrder(this.listGrid.selModel.getSelection()[0].data.obj);
	},
	
	constructor: function() {
		this.callParent(arguments);
		
		var self = this;
		
        this.setTitle(this.titleText);
		
		this.on('afterrender', function () {
			self.add(self.orderPanel);
		});
		
		this.ordersStore = Ext.create('Ext.data.Store', {
			model: 'DemandOrderModel',
			data: []
		});
		this.partsStore = Ext.create('Ext.data.Store', {
			fields: ['name'],
			data: []
		});
		
		this.rqStore = Ext.create('Ext.data.Store', {
			model: 'RequiredPartModel',
			data: []
		});
		
		var renderListRow = function(value, p, model) {
			return Ext.String.format(
				'<div style="display:table;width:100%;height:100%;max-width:100%;overflow:hidden;">' +
				'<div style="display:table-row">' + 
				'<div style="display:table-cell;font-weight:bold;">{0}</div>' +
				'<div style="display:table-cell">{1}</div></div></div>' + 
				'<div style="display:block;margin-top:4px;">{2}</div>',
				model.get('number'),
				model.get('demandDate').format('dd/mm/yyyy'),
				model.get('preview')
			);
		};
			
		this.listGrid = Ext.create("Ext.grid.Panel", {
			xtype: 'expander-lockable',
			store: this.ordersStore,
			width: 240,
			columnLines: false,
			hideHeaders: true,
			columns: [
				{ 
					dataIndex: 'number',
					flex: 1,
					renderer: renderListRow
				}
			],
			bbar: {
				items: [
					{
						text: '+'
					}, 
					{
						text: '-'
					}
				]
			},
			autoScroll: true
		});
		this.listGrid.selModel.on('select', function () { self.showCurrentOrder(); });
		
		var quantityChanged = function (s, q) { 
			self.changingRqparts = true;
			var rqpart = self.rqGrid.selModel.getSelection()[0].data.obj;
			rqpart.setQuantity(q);
			self.updateGrids();
			self.changingRqparts = false;
		};
		var reqPartChanged = function (s, partName) {
			var rqpart = self.rqGrid.selModel.getSelection()[0].data.obj;
			rqpart.setPart(self.partsByName[partName]);
			self.updateGrids();
		};
		var demandDateChanged = function (datefield, date, oldDate) {
			if (self.editing)
			{
				self.currentOrder.setDemandDate(date);
				self.updateGrids();
			}
			else if (oldDate.getTime() != date.getTime())
			{
				return false;
			}
		};
		
		var addRqpart = function () {
			if (!self.editing)
			{
				return;
			}
			self.currentOrder.requiredParts.add(new PartsModel.RequiredPart(null, 1));
			self.updateGrids();
		};
		
		var removeRqpart = function (arg) {
			if (self.rqGrid.selModel.getCount() <= 0 || !self.editing)
			{
				return;
			}
			Ext.Msg.show({
				title: self.rqpartRemoveDialogTitleText,
				message: self.rqpartRemoveDialogMsgText,
				buttons: Ext.Msg.YESNO,
				icon: Ext.Msg.QUESTION,
				fn: function(btn) {
					if (btn === 'yes') {
						self.currentOrder.requiredParts.removeAt(self.rqGrid.selModel
													.getSelection()[0].data.key);
						self.updateGrids();
					}
				}
			});
		};
		
		var removeRqpartBtn = Ext.create('Ext.button.Button', {
			text: '-',
			handler: removeRqpart,
			disabled: true
		});
		
		this.rqGrid = Ext.create('Ext.grid.Panel', {
			flex: 1,
			xtype: 'row-numberer',
			store: this.rqStore,
			columns: [
				{xtype: 'rownumberer'},
				{ 
					text: this.rqGridNameColumnText, 
					dataIndex: 'name', 
					flex: 1,
					editor: {
						xtype: 'combobox',
						store: this.partsStore,
						queryMode: 'local',
						displayField: 'name',
						valueField: 'name',
						editable: false,
						listeners: {
							'change': reqPartChanged
						}
					}
				}, 
				{ 
					text: this.rqGridQuantityColumnText, 
					dataIndex: 'quantity', 
					align: 'right', 
					width: 90,
					editor: {
						xtype: 'numberfield',
						componentCls: 'numberfield-right',
						selectOnFocus: true,
						hideTrigger: true,
						listeners: {
							'change': quantityChanged
						}
					}
				}
			],
			tbar: {
			    items: [{ text: '+', handler: addRqpart }, removeRqpartBtn],
			    style: {
			        border: '0px'
			    }
			},
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 2
			}],
			autoScroll: true
		});
		this.rqGrid.selModel.on('select', function () {
			removeRqpartBtn.setDisabled(self.rqGrid.selModel.getCount() <= 0);
		});
		
		this.editButton = Ext.create('Ext.button.Button', {
			xtype: 'button',
			text: this.editButtonEditText,
			handler: function () { self.toggleEditing(); }
		});
		
		this.dateField = Ext.create('Ext.form.field.Date', {
			xtype: 'datefield',
			bind: {
				value: '{demandDate}'
			},
			fieldLabel: this.demandDateText,
			value: '12/8/2015',
			margin: '8 8 8 4',
			width: 320,
			listeners: {
				'change': demandDateChanged
			}
		});
		
		this.orderPanel = Ext.create('Ext.panel.Panel', {
		    xtype: 'panel',
		    ui: 'basePanelWhiteBackground',
		    frame: true,
			flex: 1,
			border: false,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			bind: {
				title: '{number}'
			},
			tools: [
				this.editButton
			],
			headerPadding: '0 220 15 15',
			bodyPadding: '0 220 15 15',
			viewModel: {
				data: {number: '#111', demandDate: '12/8/2015'}
			},
			items: [
				this.dateField,
				this.rqGrid
			]
		});
		
		this.rqGrid.on('beforeedit', function(plugin, edit) {
			if (!self.editing)
			{
				edit.cancel = true;
			}
			return true;
		});
		
		this.add(this.listGrid);
		this.add(this.orderPanel);
		this.orderPanel.hide();
	}
});