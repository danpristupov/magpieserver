function DagreLayoutEngine() { };



DagreLayoutEngine.prototype.layout = function (nodes, links, rects) {
	if (nodes.length == 0 || links.length == 0)
	{
		return;
	}
	
	var nodeXMargin = 100;
	var nodeYMargin = 30;
	
	var g = new dagre.graphlib.Graph();

	g.setGraph({});
	g.setDefaultEdgeLabel(function() { return {}; });
	
	for (var n in nodes)
	{
		var node = nodes[n];

		node.width = rects[n].fullH + nodeYMargin;
		node.height = rects[n].fullW + nodeXMargin;
		
		g.setNode(n, node);
	}
	for (var l in links)
	{
		var link = links[l];
		g.setEdge(link.srcNode, link.dstNode);
	}
	
	dagre.layout(g);
	
	var maxX = 0;
	for (var n in nodes)
	{
		var node = nodes[n];
		maxX = Math.max(maxX, node.y);
	}
	
	for (var n in nodes)
	{
		var node = nodes[n];
		node.position.x = maxX - node.y;
		node.position.y = node.x;
		delete node.x;
		delete node.y;
		delete node.width;
		delete node.height;
	}
};